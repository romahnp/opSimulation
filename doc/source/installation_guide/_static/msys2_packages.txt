# for simulator
pacman -S mingw-w64-x86_64-boost                    # Tested with 1.72.0
pacman -S mingw-w64-x86_64-ccache                   # Tested with 4.7.4-1
pacman -S mingw-w64-x86_64-cmake                    # Tested with 3.27.3
pacman -S mingw-w64-x86_64-doxygen                  # Tested with 1.9.6-2
pacman -S mingw-w64-x86_64-gcc                      # Tested with 13.2.0
pacman -S mingw-w64-x86_64-gdb                      # Tested with 13.2.0
pacman -S mingw-w64-x86_64-graphviz                 # Tested with 2.44.1-12
pacman -S mingw-w64-x86_64-gtest                    # Tested with 1.14.0
pacman -S mingw-w64-x86_64-make                     # Tested with 4.4-2
pacman -S mingw-w64-x86_64-qt5-base                 # Tested with 5.15.3
pacman -S mingw-w64-x86_64-qt5-xmlpatterns          # Tested with 5.15.3

# for documentation
pacman -S mingw-w64-x86_64-python                   # Tested with 3.10.9-2
pacman -S mingw-w64-x86_64-python-pip               # Tested with 22.3.1-1
pacman -S mingw-w64-x86_64-python-lxml              # Tested with 4.9.2-1

# fonts and equation rendering in the documentation
pacman -S mingw-w64-x86_64-texlive-bin              # Tested with 2022.20220501-4
pacman -S mingw-w64-x86_64-texlive-core             # Tested with 2022.20220501-2
pacman -S mingw-w64-x86_64-texlive-font-utils       # Tested with 2022.20220501-1
pacman -S mingw-w64-x86_64-texlive-latex-extra      # Tested with 2022.20220501-1
pacman -S mingw-w64-x86_64-zziplib                  # Tested with 0.13.72-3

# documentation with only pacman
pacman -S libxslt-devel                             # Tested with 1.1.37-1
pacman -S mingw-w64-x86_64-python-sphinx            # Tested with 5.3.0-1
pacman -S mingw-w64-x86_64-python-sphinx-tabs       # Tested with 3.4.1-1
pacman -S mingw-w64-x86_64-python-sphinx_rtd_theme  # Tested with 1.1.1-1
pacman -S mingw-w64-x86_64-python-setuptools        # Tested with 66.1.0-1
pacman -S mingw-w64-x86_64-python-myst-parser       # Tested with 0.18.1-1

# for testing (optional)
pacman -S mingw-w64-x86_64-python-pytest            # Tested with 7.2.1-1
pacman -S mingw-w64-x86_64-python-pandas            # Tested with 1.5.3-1

# for developing purposes (optional)
pacman -S mingw-w64-x86_64-clang