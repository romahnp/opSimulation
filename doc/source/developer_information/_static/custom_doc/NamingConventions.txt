#pragma once

namespace openpass::component::algorithm
{

/* fooBar.h */                                           // File: lowerCamelCase
class FooBar                                             // Class: UpperCamelCase
{
private:
    static constexpr int MAGIC_NUMBER {-999};            // Constants: UPPER_CASE
    int myMember;                                        // Members: lowerCamelCase
    FooBar();                                            // Ctor: UpperCamelCase

public:
    void Bar();                                          // Methods: UpperCamelCase
    void BarBar(bool flag, int counter);                  // Arguments: lowerCamelCase
    void YaaBar(); /// Yaa = Yet Another Abbreviation    // Abbreviations: UpperCamelCase
};

}