/********************************************************************************
 * Copyright (c) 2021 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "common/globalDefinitions.h"
#include "fakeWorldObject.h"
#include "include/agentInterface.h"
#include "include/egoAgentInterface.h"

class FakeAgent : public FakeWorldObject, public AgentInterface
{
public:
  MOCK_CONST_METHOD0(GetAgentId, int());
  MOCK_METHOD0(GetEgoAgent, EgoAgentInterface &());
  MOCK_CONST_METHOD0(GetVehicleModelType, std::string());
  MOCK_CONST_METHOD0(GetVehicleModelParameters, std::shared_ptr<const mantle_api::EntityProperties>());
  MOCK_CONST_METHOD0(GetDriverProfileName, std::string());
  MOCK_CONST_METHOD0(GetScenarioName, std::string());
  MOCK_CONST_METHOD0(GetAgentCategory, AgentCategory());
  MOCK_CONST_METHOD0(GetAgentTypeName, std::string());
  MOCK_CONST_METHOD0(IsEgoAgent, bool());
  MOCK_CONST_METHOD0(GetVelocityX, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetVelocityY, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetWheelbase, units::length::meter_t());
  MOCK_CONST_METHOD0(GetGear, int());
  MOCK_CONST_METHOD0(GetDistanceCOGtoLeadingEdge, units::length::meter_t());
  MOCK_CONST_METHOD0(GetAccelerationX, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetAccelerationY, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD2(GetCollisionData, std::vector<void *>(int collisionPartnerId, int collisionDataId));
  MOCK_CONST_METHOD0(GetPostCrashVelocity, PostCrashVelocity());
  MOCK_METHOD1(SetPostCrashVelocity, void(PostCrashVelocity));
  MOCK_CONST_METHOD0(GetCollisionPartners, std::vector<CollisionPartner>());
  MOCK_METHOD1(SetPositionX, void(units::length::meter_t positionX));
  MOCK_METHOD1(SetPositionY, void(units::length::meter_t positionY));
  MOCK_METHOD1(SetVehicleModelParameter, void(std::shared_ptr<const mantle_api::EntityProperties> parameter));
  MOCK_METHOD1(SetVelocityX, void(units::velocity::meters_per_second_t velocityX));
  MOCK_METHOD1(SetVelocityY, void(units::velocity::meters_per_second_t velocityY));
  MOCK_METHOD1(SetVelocity, void(units::velocity::meters_per_second_t value));
  MOCK_METHOD1(SetAcceleration, void(units::acceleration::meters_per_second_squared_t value));
  MOCK_METHOD1(SetYaw, void(units::angle::radian_t value));
  MOCK_METHOD1(SetRoll, void(units::angle::radian_t value));
  MOCK_METHOD1(SetDistanceTraveled, void(units::length::meter_t distanceTraveled));
  MOCK_CONST_METHOD0(GetDistanceTraveled, units::length::meter_t());
  MOCK_METHOD1(SetGear, void(int gear));
  MOCK_METHOD1(SetEngineSpeed, void(units::angular_velocity::revolutions_per_minute_t engineSpeed));
  MOCK_METHOD1(SetEffAccelPedal, void(double percent));
  MOCK_METHOD1(SetEffBrakePedal, void(double percent));
  MOCK_METHOD1(SetSteeringWheelAngle, void(units::angle::radian_t steeringWheelAngle));
  MOCK_METHOD3(SetWheelsRotationRateAndOrientation,
               void(units::angle::radian_t,
                    units::velocity::meters_per_second_t,
                    units::acceleration::meters_per_second_squared_t));
  MOCK_METHOD3(SetWheelRotationRate, void(int, int, double));
  MOCK_METHOD2(GetWheelRotationRate, double(int, int));
  MOCK_METHOD1(SetMaxAcceleration, void(units::acceleration::meters_per_second_squared_t maxAcceleration));
  MOCK_METHOD1(SetMaxDeceleration, void(units::acceleration::meters_per_second_squared_t maxDeceleration));
  MOCK_METHOD1(SetAccelerationX, void(units::acceleration::meters_per_second_squared_t accelerationX));
  MOCK_METHOD1(SetAccelerationY, void(units::acceleration::meters_per_second_squared_t accelerationY));
  MOCK_METHOD1(UpdateCollision, void(CollisionPartner collisionPartner));
  MOCK_METHOD0(Unlocate, void());
  MOCK_METHOD0(Locate, bool());
  MOCK_METHOD0(Update, bool());
  MOCK_METHOD1(SetBrakeLight, void(bool brakeLightStatus));
  MOCK_CONST_METHOD0(GetBrakeLight, bool());
  MOCK_METHOD1(SetIndicatorState, void(IndicatorState indicatorState));
  MOCK_CONST_METHOD0(GetIndicatorState, IndicatorState());
  MOCK_METHOD1(SetHorn, void(bool hornSwitch));
  MOCK_CONST_METHOD0(GetHorn, bool());
  MOCK_METHOD1(SetHeadLight, void(bool headLightSwitch));
  MOCK_CONST_METHOD0(GetHeadLight, bool());
  MOCK_METHOD1(SetHighBeamLight, void(bool headLightSwitch));
  MOCK_CONST_METHOD0(GetHighBeamLight, bool());
  MOCK_CONST_METHOD0(GetLightState, LightState());
  MOCK_METHOD1(SetFlasher, void(bool flasherSwitch));
  MOCK_CONST_METHOD0(GetFlasher, bool());
  MOCK_METHOD4(InitAgentParameter,
               bool(int id,
                    int agentTypeId,
                    const AgentSpawnItem *agentSpawnItem,
                    const SpawnItemParameterInterface &spawnItemParameter));
  MOCK_METHOD1(InitParameter, void(const AgentBuildInstructions &agentBlueprint));
  MOCK_CONST_METHOD0(IsValid, bool());
  MOCK_CONST_METHOD0(GetAgentTypeId, int());
  MOCK_CONST_METHOD0(IsAgentInWorld, bool());
  MOCK_METHOD0(IsAgentAtEndOfRoad, bool());
  MOCK_METHOD1(SetPosition, void(Position pos));
  MOCK_METHOD1(GetDistanceToFrontAgent, units::length::meter_t(int laneId));
  MOCK_METHOD1(GetDistanceToRearAgent, units::length::meter_t(int laneId));
  MOCK_METHOD0(RemoveSpecialAgentMarker, void());
  MOCK_METHOD0(SetSpecialAgentMarker, void());
  MOCK_METHOD0(SetObstacleFlag, void());
  MOCK_METHOD0(GetDistanceToSpecialAgent, units::length::meter_t());
  MOCK_METHOD0(IsObstacle, bool());
  MOCK_CONST_METHOD0(IsLeavingWorld, bool());
  MOCK_CONST_METHOD0(IsCrossingLanes, bool());
  MOCK_METHOD0(GetDistanceFrontAgentToEgo, units::length::meter_t());
  MOCK_METHOD0(HasTwoLeftLanes, bool());
  MOCK_METHOD0(HasTwoRightLanes, bool());
  MOCK_METHOD1(EstimateLaneChangeState, LaneChangeState(double thresholdLooming));
  MOCK_CONST_METHOD0(IsBicycle, bool());
  MOCK_CONST_METHOD0(IsFirstCarInLane, bool());
  MOCK_CONST_METHOD0(GetTypeOfNearestMark, MarkType());
  MOCK_CONST_METHOD0(GetTypeOfNearestMarkString, std::string());
  MOCK_CONST_METHOD1(GetDistanceToNearestMark, units::length::meter_t(MarkType markType));
  MOCK_CONST_METHOD1(GetOrientationOfNearestMark, units::angle::radian_t(MarkType markType));
  MOCK_CONST_METHOD1(GetViewDirectionToNearestMark, double(MarkType markType));
  MOCK_CONST_METHOD1(GetAgentViewDirectionToNearestMark, AgentViewDirection(MarkType markType));
  MOCK_CONST_METHOD2(GetDistanceToNearestMarkInViewDirection,
                     units::length::meter_t(MarkType markType, AgentViewDirection agentViewDirection));
  MOCK_CONST_METHOD2(GetDistanceToNearestMarkInViewDirection,
                     units::length::meter_t(MarkType markType, double mainViewDirection));
  MOCK_CONST_METHOD2(GetOrientationOfNearestMarkInViewDirection,
                     units::angle::radian_t(MarkType markType, AgentViewDirection agentViewDirection));
  MOCK_CONST_METHOD2(GetOrientationOfNearestMarkInViewDirection,
                     units::angle::radian_t(MarkType markType, double mainViewDirection));
  MOCK_CONST_METHOD3(GetDistanceToNearestMarkInViewRange,
                     units::length::meter_t(MarkType markType,
                                            AgentViewDirection agentViewDirection,
                                            units::length::meter_t range));
  MOCK_CONST_METHOD3(GetDistanceToNearestMarkInViewRange,
                     units::length::meter_t(MarkType markType, double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD3(GetOrientationOfNearestMarkInViewRange,
                     units::angle::radian_t(MarkType markType,
                                            AgentViewDirection agentViewDirection,
                                            units::length::meter_t range));
  MOCK_CONST_METHOD3(GetOrientationOfNearestMarkInViewRange,
                     units::angle::radian_t(MarkType markType, double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD3(GetViewDirectionToNearestMarkInViewRange,
                     double(MarkType markType, AgentViewDirection agentViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD3(GetViewDirectionToNearestMarkInViewRange,
                     double(MarkType markType, double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetTypeOfNearestObject,
                     std::string(AgentViewDirection agentViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetTypeOfNearestObject, std::string(double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD3(GetDistanceToNearestObjectInViewRange,
                     units::length::meter_t(ObjectType objectType,
                                            AgentViewDirection agentViewDirection,
                                            units::length::meter_t range));
  MOCK_CONST_METHOD3(GetDistanceToNearestObjectInViewRange,
                     units::length::meter_t(ObjectType objectType,
                                            double mainViewDirection,
                                            units::length::meter_t range));
  MOCK_CONST_METHOD3(GetViewDirectionToNearestObjectInViewRange,
                     double(ObjectType objectType,
                            AgentViewDirection agentViewDirection,
                            units::length::meter_t range));
  MOCK_CONST_METHOD3(GetViewDirectionToNearestObjectInViewRange,
                     double(ObjectType objectType, double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetIdOfNearestAgent, int(AgentViewDirection agentViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetIdOfNearestAgent, int(double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetDistanceToNearestAgentInViewRange,
                     units::length::meter_t(AgentViewDirection agentViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetDistanceToNearestAgentInViewRange,
                     units::length::meter_t(double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetViewDirectionToNearestAgentInViewRange,
                     double(AgentViewDirection agentViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetViewDirectionToNearestAgentInViewRange,
                     double(double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD2(GetVisibilityToNearestAgentInViewRange,
                     double(double mainViewDirection, units::length::meter_t range));
  MOCK_CONST_METHOD0(GetYawRate, units::angular_velocity::radians_per_second_t());
  MOCK_METHOD1(SetYawRate, void(units::angular_velocity::radians_per_second_t yawRate));
  MOCK_CONST_METHOD0(GetCentripetalAcceleration, units::acceleration::meters_per_second_squared_t());
  MOCK_METHOD1(SetCentripetalAcceleration,
               void(units::acceleration::meters_per_second_squared_t centripetalAcceleration));
  MOCK_METHOD0(GetYawAcceleration, units::angular_acceleration::radians_per_second_squared_t());
  MOCK_METHOD1(SetYawAcceleration, void(units::angular_acceleration::radians_per_second_squared_t yawAcceleration));
  MOCK_CONST_METHOD0(GetTrajectoryTime, const std::vector<int> *());
  MOCK_CONST_METHOD0(GetTrajectoryXPos, const std::vector<units::length::meter_t> *());
  MOCK_CONST_METHOD0(GetTrajectoryYPos, const std::vector<units::length::meter_t> *());
  MOCK_CONST_METHOD0(GetTrajectoryVelocity, const std::vector<units::velocity::meters_per_second_t> *());
  MOCK_CONST_METHOD0(GetTrajectoryAngle, const std::vector<units::angle::radian_t> *());
  MOCK_METHOD1(SetAccelerationIntention, void(units::acceleration::meters_per_second_squared_t accelerationIntention));
  MOCK_CONST_METHOD0(GetAccelerationIntention, units::acceleration::meters_per_second_squared_t());
  MOCK_METHOD1(SetDecelerationIntention, void(units::acceleration::meters_per_second_squared_t decelerationIntention));
  MOCK_CONST_METHOD0(GetDecelerationIntention, units::acceleration::meters_per_second_squared_t());
  MOCK_METHOD1(SetAngleIntention, void(units::angle::radian_t angleIntention));
  MOCK_CONST_METHOD0(GetAngleIntention, units::angle::radian_t());
  MOCK_METHOD1(SetCollisionState, void(bool collisionState));
  MOCK_CONST_METHOD0(GetCollisionState, bool());
  MOCK_CONST_METHOD0(GetAccelerationAbsolute, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetTouchedRoads, const RoadIntervals &());
  MOCK_CONST_METHOD1(GetAbsolutePosition, Common::Vector2d<units::length::meter_t>(const ObjectPoint &objectPoint));
  MOCK_CONST_METHOD1(GetRoadPosition, const GlobalRoadPositions &(const ObjectPoint &point));
  MOCK_CONST_METHOD1(GetRoads, std::vector<std::string>(ObjectPoint point));
  MOCK_CONST_METHOD0(GetDistanceReferencePointToLeadingEdge, units::length::meter_t());
  MOCK_CONST_METHOD0(GetEngineSpeed, units::angular_velocity::revolutions_per_minute_t());
  MOCK_CONST_METHOD0(GetEffAccelPedal, double());
  MOCK_CONST_METHOD0(GetEffBrakePedal, double());
  MOCK_CONST_METHOD0(GetSteeringWheelAngle, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetMaxAcceleration, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetMaxDeceleration, units::acceleration::meters_per_second_squared_t());
  MOCK_CONST_METHOD0(GetSpeedGoalMin, units::velocity::meters_per_second_t());
  MOCK_CONST_METHOD0(GetSensorParameters, const openpass::sensors::Parameters &());
  MOCK_METHOD1(SetSensorParameters, void(openpass::sensors::Parameters sensorParameters));
  MOCK_CONST_METHOD3(GetDistanceToConnectorEntrance,
                     units::length::meter_t(std::string intersectingConnectorId,
                                            int intersectingLaneId,
                                            std::string ownConnectorId));
  MOCK_CONST_METHOD3(GetDistanceToConnectorDeparture,
                     units::length::meter_t(std::string intersectingConnectorId,
                                            int intersectingLaneId,
                                            std::string ownConnectorId));
  MOCK_CONST_METHOD0(GetDistanceToNextJunction, units::length::meter_t());
  MOCK_CONST_METHOD0(GetYawAcceleration, units::angular_acceleration::radians_per_second_squared_t());
  MOCK_CONST_METHOD0(GetTangentialAcceleration, units::acceleration::meters_per_second_squared_t());
  MOCK_METHOD1(SetTangentialAcceleration, void(units::acceleration::meters_per_second_squared_t));
  MOCK_METHOD1(SetVelocityVector, void(const mantle_api::Vec3<units::velocity::meters_per_second_t> &velocity));
  MOCK_METHOD1(SetAccelerationVector,
               void(const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> &acceleration));
};
