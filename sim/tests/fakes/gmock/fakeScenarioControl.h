/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once
#include "gmock/gmock.h"

#include "include/scenarioControlInterface.h"

class FakeScenarioControl : public ScenarioControlInterface
{
public:
  MOCK_METHOD1(GetStrategies,
               std::vector<std::shared_ptr<mantle_api::ControlStrategy>>(mantle_api::ControlStrategyType type));
  MOCK_METHOD1(GetStrategies,
               std::vector<std::shared_ptr<mantle_api::ControlStrategy>>(mantle_api::MovementDomain domain));
  MOCK_METHOD1(SetStrategies, void(std::vector<std::shared_ptr<mantle_api::ControlStrategy>> strategies));
  MOCK_CONST_METHOD0(HasNewLongitudinalStrategy, bool());
  MOCK_CONST_METHOD0(HasNewLateralStrategy, bool());
  MOCK_METHOD1(SetControlStrategyGoalReached, void(mantle_api::ControlStrategyType type));
  MOCK_CONST_METHOD1(HasControlStrategyGoalBeenReached, bool(mantle_api::ControlStrategyType type));
  MOCK_CONST_METHOD0(UseCustomController, bool());
  MOCK_METHOD1(SetCustomController, void(bool useCustomController));
  MOCK_METHOD0(GetCustomCommands, const std::vector<std::string>&());
  MOCK_METHOD1(AddCustomCommand, void(const std::string& command));
  MOCK_METHOD0(UpdateForNextTimestep, void());
  MOCK_METHOD0(GetCommands, const ScenarioCommands&());
};