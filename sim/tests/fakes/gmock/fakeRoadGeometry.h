/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "include/roadInterface/roadGeometryInterface.h"

class FakeRoadGeometry : public RoadGeometryInterface
{
public:
  MOCK_CONST_METHOD2(GetCoord,
                     Common::Vector2d<units::length::meter_t>(units::length::meter_t geometryOffset,
                                                              units::length::meter_t laneOffset));
  MOCK_CONST_METHOD1(GetDir, units::angle::radian_t(units::length::meter_t geometryOffset));
  MOCK_CONST_METHOD0(GetS, units::length::meter_t());
  MOCK_CONST_METHOD0(GetHdg, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetLength, units::length::meter_t());
};
