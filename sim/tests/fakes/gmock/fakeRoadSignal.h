/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include <list>
#include <optional>
#include <string>

#include "include/roadInterface/roadSignalInterface.h"

class FakeRoadSignal : public RoadSignalInterface
{
public:
  MOCK_CONST_METHOD0(GetId, std::string());
  MOCK_CONST_METHOD0(GetS, units::length::meter_t());
  MOCK_CONST_METHOD0(GetT, units::length::meter_t());
  MOCK_CONST_METHOD1(IsValidForLane, bool(int laneId));
  MOCK_CONST_METHOD0(GetCountry, std::string());
  MOCK_CONST_METHOD0(GetCountryRevision, std::string());
  MOCK_CONST_METHOD0(GetType, std::string());
  MOCK_CONST_METHOD0(GetSubType, std::string());
  MOCK_CONST_METHOD0(GetValue, std::optional<double>());
  MOCK_CONST_METHOD0(GetUnit, RoadSignalUnit());
  MOCK_CONST_METHOD0(GetText, std::string());
  MOCK_CONST_METHOD0(GetDependencies, std::vector<std::string>());
  MOCK_CONST_METHOD0(GetIsDynamic, bool());
  MOCK_CONST_METHOD0(GetWidth, units::length::meter_t());
  MOCK_CONST_METHOD0(GetHeight, units::length::meter_t());
  MOCK_CONST_METHOD0(GetZOffset, units::length::meter_t());
  MOCK_CONST_METHOD0(GetPitch, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetRoll, units::angle::radian_t());
  MOCK_CONST_METHOD0(GetOrientation, bool());
  MOCK_CONST_METHOD0(GetHOffset, units::angle::radian_t());
};
