/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "gmock/gmock.h"

#include <MantleAPI/Common/i_geometry_helper.h>
#include <MantleAPI/Test/test_utils.h>

#include "include/environmentInterface.h"

class FakeEnvironment : public core::EnvironmentInterface
{
public:
  MOCK_METHOD2(CreateMap, void(const std::string& map_file_path, const mantle_api::MapDetails& map_details));
  MOCK_METHOD2(AddEntityToController, void(mantle_api::IEntity& entity, mantle_api::UniqueId controller_id));
  MOCK_METHOD2(RemoveEntityFromController, void(mantle_api::UniqueId entity_id, mantle_api::UniqueId controller_id));
  MOCK_METHOD2(UpdateControlStrategies,
               void(mantle_api::UniqueId entity_id,
                    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies));
  MOCK_CONST_METHOD2(HasControlStrategyGoalBeenReached,
                     bool(mantle_api::UniqueId entity_id, mantle_api::ControlStrategyType type));
  MOCK_CONST_METHOD0(GetQueryService, const mantle_api::ILaneLocationQueryService&());
  MOCK_CONST_METHOD0(GetConverter, const mantle_api::ICoordConverter*());
  MOCK_CONST_METHOD0(GetGeometryHelper, const mantle_api::IGeometryHelper*());
  MOCK_METHOD1(SetDateTime, void(mantle_api::Time time));
  MOCK_METHOD0(GetDateTime, mantle_api::Time());
  MOCK_METHOD0(GetSimulationTime, mantle_api::Time());
  MOCK_METHOD1(SetWeather, void(mantle_api::Weather weather));
  MOCK_METHOD1(SetRoadCondition, void(std::vector<mantle_api::FrictionPatch> friction_patches));
  MOCK_METHOD2(SetTrafficSignalState,
               void(const std::string& traffic_signal_name, const std::string& traffic_signal_state));
  MOCK_METHOD3(ExecuteCustomCommand,
               void(const std::vector<std::string>& actors, const std::string& type, const std::string& command));
  MOCK_METHOD2(SetUserDefinedValue, void(const std::string& name, const std::string& value));
  MOCK_METHOD1(GetUserDefinedValue, std::optional<std::string>(const std::string& name));
  MOCK_METHOD1(SetDefaultRoutingBehavior, void(mantle_api::DefaultRoutingBehavior default_routing_behavior));
  MOCK_METHOD2(AssignRoute, void(mantle_api::UniqueId entity_id, mantle_api::RouteDefinition route_definition));
  MOCK_METHOD0(GetEntityRepository, core::EntityRepositoryInterface&());
  MOCK_CONST_METHOD0(GetEntityRepository, const core::EntityRepositoryInterface&());
  MOCK_METHOD0(GetControllerRepository, mantle_api::MockControllerRepository&());
  MOCK_CONST_METHOD0(GetControllerRepository, const mantle_api::MockControllerRepository&());
  MOCK_METHOD1(SetSimulationTime, void(mantle_api::Time simulationTime));
  MOCK_METHOD0(SyncWorld, void());
  MOCK_METHOD0(ResetControlStrategyStatus, void());
  MOCK_METHOD1(SetWorld, void(WorldInterface* world));
  MOCK_METHOD0(Reset, void());
  MOCK_METHOD1(InitTrafficSwarmService, void(const mantle_api::TrafficSwarmParameters& parameters));
  MOCK_METHOD0(GetTrafficSwarmService, mantle_api::ITrafficSwarmService&());
};
