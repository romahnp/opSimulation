/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include "include/worldInterface.h"

// see https://groups.google.com/g/googlemock/c/jVy6d2S_7Bo
inline void PrintTo(const RadioInterface &, std::ostream *){};

class FakeWorld : public WorldInterface
{
public:
  MOCK_METHOD0(CreateAgentAdapterForAgent, AgentInterface *());
  MOCK_METHOD1(CreateAgentAdapter, AgentInterface &(const AgentBuildInstructions &agentBlueprint));
  MOCK_CONST_METHOD1(GetAgent, AgentInterface *(int id));
  MOCK_METHOD1(GetAgentByName, AgentInterface *(const std::string &scenarioName));
  MOCK_METHOD0(GetEgoAgent, AgentInterface *());
  MOCK_METHOD0(CreateGlobalDrivingView, bool());
  MOCK_METHOD2(CreateScenery, bool(const SceneryInterface *scenery, const TurningRates &turningRates));
  MOCK_METHOD1(SetWeather, void(const mantle_api::Weather &weather));
  MOCK_METHOD2(SetTrafficSignalState,
               void(const std::string &traffic_signal_name, const std::string &traffic_signal_state));
  MOCK_METHOD1(CreateWorldScenario, bool(const std::string &scenarioFilename));
  MOCK_METHOD1(CreateWorldScenery, bool(const std::string &sceneryFilename));
  MOCK_METHOD0(Instantiate, bool());
  MOCK_CONST_METHOD1(GetLaneSections, LaneSections(const std::string &roadId));
  MOCK_METHOD6(IntersectsWithAgent,
               bool(units::length::meter_t x,
                    units::length::meter_t y,
                    units::angle::radian_t rotation,
                    units::length::meter_t length,
                    units::length::meter_t width,
                    units::length::meter_t center));
  MOCK_METHOD0(isInstantiated, bool());
  MOCK_METHOD3(IsSValidOnLane, bool(std::string roadId, int laneId, units::length::meter_t distance));
  MOCK_CONST_METHOD2(IsDirectionalRoadExisting, bool(const std::string &roadId, bool inOdDirection));
  MOCK_METHOD4(IsLaneTypeValid,
               bool(const std::string &roadId,
                    const int laneId,
                    const units::length::meter_t distanceOnLane,
                    const LaneTypes &validLaneTypes));
  MOCK_CONST_METHOD0(GetBicycle, const AgentInterface *());
  MOCK_CONST_METHOD1(GetLastCarInlane, const AgentInterface *(int laneNumber));
  MOCK_CONST_METHOD0(GetSpecialAgent, const AgentInterface *());
  MOCK_METHOD0(GetRemovedAgentsInPreviousTimestep, const std::vector<int>());
  MOCK_METHOD0(GetAgents, std::map<int, AgentInterface *>());
  MOCK_CONST_METHOD0(GetTrafficObjects, const std::vector<const TrafficObjectInterface *> &());
  MOCK_CONST_METHOD0(GetWorldObjects, const std::vector<const WorldObjectInterface *> &());
  MOCK_CONST_METHOD5(GetDistanceToEndOfLane,
                     RouteQueryResult<units::length::meter_t>(const RoadGraph &roadGraph,
                                                              RoadGraphVertex startNode,
                                                              int laneId,
                                                              units::length::meter_t initialSearchDistance,
                                                              units::length::meter_t maximumSearchLength));
  MOCK_CONST_METHOD6(GetDistanceToEndOfLane,
                     RouteQueryResult<units::length::meter_t>(const RoadGraph &roadGraph,
                                                              RoadGraphVertex startNode,
                                                              int laneId,
                                                              units::length::meter_t initialSearchDistance,
                                                              units::length::meter_t maximumSearchLength,
                                                              const LaneTypes &laneTypes));
  MOCK_CONST_METHOD0(GetFriction, double());
  MOCK_CONST_METHOD3(GetLaneCurvature,
                     units::curvature::inverse_meter_t(std::string roadId,
                                                       int laneId,
                                                       units::length::meter_t position));
  MOCK_CONST_METHOD5(
      GetLaneCurvature,
      RouteQueryResult<std::optional<units::curvature::inverse_meter_t>>(const RoadGraph &roadGraph,
                                                                         RoadGraphVertex startNode,
                                                                         int laneId,
                                                                         units::length::meter_t position,
                                                                         units::length::meter_t distance));
  MOCK_CONST_METHOD3(GetLaneDirection,
                     units::angle::radian_t(std::string roadId, int laneId, units::length::meter_t position));
  MOCK_CONST_METHOD5(GetLaneDirection,
                     RouteQueryResult<std::optional<units::angle::radian_t>>(const RoadGraph &roadGraph,
                                                                             RoadGraphVertex startNode,
                                                                             int laneId,
                                                                             units::length::meter_t position,
                                                                             units::length::meter_t distance));
  MOCK_CONST_METHOD3(GetLaneWidth,
                     units::length::meter_t(std::string roadId, int laneId, units::length::meter_t position));
  MOCK_CONST_METHOD5(GetLaneWidth,
                     RouteQueryResult<std::optional<units::length::meter_t>>(const RoadGraph &roadGraph,
                                                                             RoadGraphVertex startNode,
                                                                             int laneId,
                                                                             units::length::meter_t position,
                                                                             units::length::meter_t distance));
  MOCK_CONST_METHOD0(GetVisibilityDistance, units::length::meter_t());
  MOCK_CONST_METHOD2(GetLaneId, int(uint64_t streamId, units::length::meter_t endDistance));
  MOCK_CONST_METHOD4(LaneCoord2WorldCoord,
                     std::optional<Position>(units::length::meter_t distanceOnLane,
                                             units::length::meter_t offset,
                                             std::string roadId,
                                             int laneId));
  MOCK_CONST_METHOD2(RoadCoord2WorldCoord, std::optional<Position>(RoadPosition roadCoord, std::string roadID));
  MOCK_CONST_METHOD3(WorldCoord2LaneCoord,
                     GlobalRoadPositions(units::length::meter_t x,
                                         units::length::meter_t y,
                                         units::angle::radian_t heading));
  MOCK_CONST_METHOD1(GetRoadLength, units::length::meter_t(const std::string &roadId));
  MOCK_CONST_METHOD5(
      GetObstruction,
      RouteQueryResult<Obstruction>(const RoadGraph &roadGraph,
                                    RoadGraphVertex startNode,
                                    const GlobalRoadPosition &ownPosition,
                                    const std::map<ObjectPoint, Common::Vector2d<units::length::meter_t>> &points,
                                    const RoadIntervals &touchedRoads));
  MOCK_CONST_METHOD0(GetTimeOfDay, std::string());
  MOCK_CONST_METHOD0(GetTrafficRules, const TrafficRules &());
  MOCK_CONST_METHOD5(GetTrafficSignsInRange,
                     RouteQueryResult<std::vector<CommonTrafficSign::Entity>>(const RoadGraph &roadGraph,
                                                                              RoadGraphVertex startNode,
                                                                              int laneId,
                                                                              units::length::meter_t startDistance,
                                                                              units::length::meter_t searchRange));
  MOCK_CONST_METHOD5(GetRoadMarkingsInRange,
                     RouteQueryResult<std::vector<CommonTrafficSign::Entity>>(const RoadGraph &roadGraph,
                                                                              RoadGraphVertex startNode,
                                                                              int laneId,
                                                                              units::length::meter_t startDistance,
                                                                              units::length::meter_t searchRange));
  MOCK_CONST_METHOD5(GetTrafficLightsInRange,
                     RouteQueryResult<std::vector<CommonTrafficLight::Entity>>(const RoadGraph &roadGraph,
                                                                               RoadGraphVertex startNode,
                                                                               int laneId,
                                                                               units::length::meter_t startDistance,
                                                                               units::length::meter_t searchRange));
  MOCK_CONST_METHOD6(GetLaneMarkings,
                     RouteQueryResult<std::vector<LaneMarking::Entity>>(const RoadGraph &roadGraph,
                                                                        RoadGraphVertex startNode,
                                                                        int laneId,
                                                                        units::length::meter_t startDistance,
                                                                        units::length::meter_t range,
                                                                        Side side));
  MOCK_CONST_METHOD6(GetAgentsInRange,
                     RouteQueryResult<AgentInterfaces>(const RoadGraph &roadGraph,
                                                       RoadGraphVertex startNode,
                                                       int laneId,
                                                       units::length::meter_t startDistance,
                                                       units::length::meter_t backwardRange,
                                                       units::length::meter_t forwardRange));
  MOCK_CONST_METHOD6(GetObjectsInRange,
                     RouteQueryResult<std::vector<const WorldObjectInterface *>>(const RoadGraph &roadGraph,
                                                                                 RoadGraphVertex startNode,
                                                                                 int laneId,
                                                                                 units::length::meter_t startDistance,
                                                                                 units::length::meter_t backwardRange,
                                                                                 units::length::meter_t forwardRange));
  MOCK_CONST_METHOD2(GetAgentsInRangeOfJunctionConnection,
                     AgentInterfaces(std::string connectingRoadId, units::length::meter_t range));
  MOCK_CONST_METHOD3(GetDistanceToConnectorEntrance,
                     units::length::meter_t(/*const ObjectPosition position,*/ std::string intersectingConnectorId,
                                            int intersectingLaneId,
                                            std::string ownConnectorId));
  MOCK_CONST_METHOD3(GetDistanceToConnectorDeparture,
                     units::length::meter_t(/*const ObjectPosition position,*/ std::string intersectingConnectorId,
                                            int intersectingLaneId,
                                            std::string ownConnectorId));
  MOCK_CONST_METHOD0(GetGlobalDrivingView, void *());
  MOCK_CONST_METHOD0(GetGlobalObjects, void *());
  MOCK_METHOD0(Clear, void());
  MOCK_METHOD1(ExtractParameter, void(ParameterInterface *parameters));
  MOCK_METHOD1(QueueAgentRemove, void(const AgentInterface *agent));
  MOCK_METHOD1(QueueAgentUpdate, void(std::function<void()> func));
  MOCK_METHOD2(QueueAgentUpdate, void(std::function<void(double)> func, double val));
  MOCK_METHOD0(RemoveAgents, void());
  MOCK_METHOD0(Reset, void());
  MOCK_METHOD1(SetTimeOfDay, void(int timeOfDay));
  MOCK_METHOD1(SetWeekday, void(Weekday weekday));
  MOCK_METHOD1(SyncGlobalData, void(int timestamp));
  MOCK_METHOD1(PublishGlobalData, void(int timestamp));
  MOCK_CONST_METHOD0(GetOsiGroundTruth, void *());
  MOCK_METHOD0(GetWorldData, void *());
  MOCK_CONST_METHOD0(GetWeekday, Weekday());
  MOCK_CONST_METHOD2(GetConnectionsOnJunction,
                     std::vector<JunctionConnection>(std::string junctionId, std::string incomingRoadId));
  MOCK_CONST_METHOD1(GetIntersectingConnections, std::vector<IntersectingConnection>(std::string connectingRoadId));
  MOCK_CONST_METHOD1(GetPrioritiesOnJunction, std::vector<JunctionConnectorPriority>(std::string junctionId));
  MOCK_CONST_METHOD1(GetRoadSuccessor, RoadNetworkElement(std::string roadId));
  MOCK_CONST_METHOD1(GetRoadPredecessor, RoadNetworkElement(std::string roadId));
  MOCK_CONST_METHOD4(GetDistanceBetweenObjects,
                     RouteQueryResult<std::optional<units::length::meter_t>>(const RoadGraph &roadGraph,
                                                                             RoadGraphVertex startNode,
                                                                             units::length::meter_t ownPosition,
                                                                             const GlobalRoadPositions &target));
  MOCK_CONST_METHOD4(GetRelativeJunctions,
                     RouteQueryResult<RelativeWorldView::Roads>(const RoadGraph &roadGraph,
                                                                RoadGraphVertex startNode,
                                                                units::length::meter_t startDistance,
                                                                units::length::meter_t range));
  MOCK_CONST_METHOD4(GetRelativeRoads,
                     RouteQueryResult<RelativeWorldView::Roads>(const RoadGraph &roadGraph,
                                                                RoadGraphVertex startNode,
                                                                units::length::meter_t startDistance,
                                                                units::length::meter_t range));
  MOCK_CONST_METHOD6(GetRelativeLanes,
                     RouteQueryResult<RelativeWorldView::Lanes>(const RoadGraph &roadGraph,
                                                                RoadGraphVertex startNode,
                                                                int laneId,
                                                                units::length::meter_t distance,
                                                                units::length::meter_t range,
                                                                bool includeOncoming));
  MOCK_CONST_METHOD5(GetRelativeLaneId,
                     RouteQueryResult<std::optional<int>>(const RoadGraph &roadGraph,
                                                          RoadGraphVertex startNode,
                                                          int laneId,
                                                          units::length::meter_t distance,
                                                          GlobalRoadPositions targetPosition));
  MOCK_CONST_METHOD3(GetRoadGraph,
                     std::pair<RoadGraph, RoadGraphVertex>(const RouteElement &start,
                                                           int maxDepth,
                                                           bool inDrivingDirection));
  MOCK_CONST_METHOD1(GetEdgeWeights, std::map<RoadGraphEdge, double>(const RoadGraph &roadGraph));
  MOCK_CONST_METHOD1(GetRoadStream, std::unique_ptr<RoadStreamInterface>(const std::vector<RouteElement> &route));
  MOCK_CONST_METHOD4(ResolveRelativePoint,
                     RouteQueryResult<std::optional<GlobalRoadPosition>>(const RoadGraph &roadGraph,
                                                                         RoadGraphVertex startNode,
                                                                         ObjectPointRelative relativePoint,
                                                                         const WorldObjectInterface &object));
  MOCK_METHOD0(GetRadio, RadioInterface &());
  MOCK_CONST_METHOD3(GetUniqueLaneId, uint64_t(std::string roadId, int laneId, units::length::meter_t position));
};
