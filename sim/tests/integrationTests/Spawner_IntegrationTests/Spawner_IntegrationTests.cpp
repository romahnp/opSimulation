/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *               2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "SpawnerPreRunCommon.h"
#include "fakeAgent.h"
#include "fakeAgentBlueprintProvider.h"
#include "fakeAgentFactory.h"
#include "fakeCallback.h"
#include "fakeEntityRepository.h"
#include "fakeEnvironment.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeStochastics.h"
#include "fakeStream.h"
#include "fakeWorld.h"
#include "geometryHelper.h"

using ::testing::_;
using ::testing::AllOf;
using ::testing::ByMove;
using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::Ge;
using ::testing::Le;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::VariantWith;

using namespace units::literals;

constexpr char ROADID[] = "MyRoad";

class SpawnerPreRun_IntegrationTests : public testing::Test
{
public:
  SpawnerPreRun_IntegrationTests()
  {
    dependencies.parameters = &parameters;
    ON_CALL(parameters, GetParameterLists()).WillByDefault(ReturnRef(parameterLists));
    ON_CALL(parameters, GetParametersStochastic()).WillByDefault(ReturnRef(parametersStochastic));
    ON_CALL(parameters, GetParametersDouble()).WillByDefault(ReturnRef(parametersDouble));
    ON_CALL(*trafficGroup1, GetParametersDouble()).WillByDefault(ReturnRef(trafficGroup1Double));
    ON_CALL(*trafficGroup1, GetParametersStochastic()).WillByDefault(ReturnRef(trafficGroup1Stochastic));
    ON_CALL(*trafficGroup1, GetParametersBool()).WillByDefault(ReturnRef(trafficGroup1Bool));
    ON_CALL(*trafficGroup1, GetParametersDoubleVector()).WillByDefault(ReturnRef(trafficGroup1DoubleVector));
    ON_CALL(*trafficGroup1, GetParameterLists()).WillByDefault(ReturnRef(trafficGroup1Lists));
    ON_CALL(*trafficGroup2, GetParametersDouble()).WillByDefault(ReturnRef(trafficGroup2Double));
    ON_CALL(*trafficGroup2, GetParametersStochastic()).WillByDefault(ReturnRef(trafficGroup2Stochastic));
    ON_CALL(*trafficGroup2, GetParametersBool()).WillByDefault(ReturnRef(trafficGroup2Bool));
    ON_CALL(*trafficGroup2, GetParametersDoubleVector()).WillByDefault(ReturnRef(trafficGroup2DoubleVector));
    ON_CALL(*trafficGroup2, GetParameterLists()).WillByDefault(ReturnRef(trafficGroup2Lists));
    ON_CALL(*agentProfile1a, GetParametersDouble()).WillByDefault(ReturnRef(agentProfile1aDouble));
    ON_CALL(*agentProfile1b, GetParametersDouble()).WillByDefault(ReturnRef(agentProfile1bDouble));
    ON_CALL(*agentProfile2a, GetParametersDouble()).WillByDefault(ReturnRef(agentProfile2aDouble));
    ON_CALL(*agentProfile2b, GetParametersDouble()).WillByDefault(ReturnRef(agentProfile2bDouble));
    ON_CALL(*agentProfile1a, GetParametersString()).WillByDefault(ReturnRef(agentProfile1aString));
    ON_CALL(*agentProfile1b, GetParametersString()).WillByDefault(ReturnRef(agentProfile1bString));
    ON_CALL(*agentProfile2a, GetParametersString()).WillByDefault(ReturnRef(agentProfile2aString));
    ON_CALL(*agentProfile2b, GetParametersString()).WillByDefault(ReturnRef(agentProfile2bString));
    ON_CALL(stochastics, GetUniformDistributed(10, 20)).WillByDefault(Return(15));
    ON_CALL(stochastics, GetUniformDistributed(20, 30)).WillByDefault(Return(25));
    ON_CALL(stochastics, GetUniformDistributed(1, 3)).WillByDefault(Return(2));
    ON_CALL(stochastics, GetUniformDistributed(2, 4)).WillByDefault(Return(3));
    ON_CALL(stochastics, GetUniformDistributed(0, DoubleEq(6))).WillByDefault(Return(1));
    ON_CALL(stochastics, GetUniformDistributed(0, DoubleEq(41))).WillByDefault(Return(7));

    ON_CALL(entityRepository, CreateCommon(testing::A<const mantle_api::VehicleProperties&>()))
        .WillByDefault(
            [=](const mantle_api::VehicleProperties& properties) -> mantle_api::IVehicle&
            {
              auto agent = new FakeAgent;
              ON_CALL(*agent, GetLength()).WillByDefault(Return(5.0_m));
              ON_CALL(*agent, GetDistanceReferencePointToLeadingEdge()).WillByDefault(Return(3.0_m));
              auto vehicle = new mantle_api::MockVehicle;
              vehicles.push_back(vehicle);
              vehicle->SetProperties(std::make_unique<mantle_api::EntityProperties>(properties));
              ON_CALL(*vehicle, SetPosition(_))
                  .WillByDefault(
                      [=](const mantle_api::Vec3<units::length::meter_t>& position)
                      {
                        ON_CALL(*vehicle, GetPosition()).WillByDefault(Return(position));
                        ON_CALL(*agent, GetPositionX()).WillByDefault(Return(position.x - 0.5_m));
                        auto laneId = -1 + static_cast<int>(position.y / 3.0_m);
                        vehiclesOnLane[laneId].insert(vehiclesOnLane[laneId].begin(), vehicle);
                        agentsOnLane[laneId].insert(agentsOnLane[laneId].begin(), agent);
                      });
              ON_CALL(*vehicle, GetOrientation())
                  .WillByDefault(Return(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad}));
              ON_CALL(*vehicle, SetVelocity(_))
                  .WillByDefault(
                      [=](const mantle_api::Vec3<units::velocity::meters_per_second_t>& velocity)
                      {
                        ON_CALL(*vehicle, GetVelocity()).WillByDefault(Return(velocity));
                        ON_CALL(*agent, GetVelocity()).WillByDefault(Return(velocity.x));
                      });
              return *vehicle;
            });

    ON_CALL(controllerRepository, Create(testing::A<std::unique_ptr<mantle_api::IControllerConfig>>()))
        .WillByDefault(
            [&](std::unique_ptr<mantle_api::IControllerConfig> config) -> mantle_api::IController&
            {
              auto controller = new mantle_api::MockController;
              controllers.push_back(controller);

              const auto externalControllerConfig = dynamic_cast<mantle_api::ExternalControllerConfig*>(config.get());
              auto agentProfile = externalControllerConfig->parameters["AgentProfile"];

              ON_CALL(*controller, GetUniqueId()).WillByDefault(Return(nextId));
              agentProfiles.insert({nextId, agentProfile});
              nextId++;

              return *controller;
            });
    ON_CALL(*environment, AddEntityToController(testing::A<mantle_api::IEntity&>(), _))
        .WillByDefault(
            [&](mantle_api::IEntity& entity, std::uint64_t controller_id)
            {
              auto agentProfile = agentProfiles[controller_id];
              agentProfilesByEntity.insert({&entity, agentProfile});
            });

    ON_CALL(*environment, GetEntityRepository()).WillByDefault(ReturnRef(entityRepository));
    ON_CALL(*environment, GetControllerRepository()).WillByDefault(ReturnRef(controllerRepository));
    ON_CALL(*environment, GetGeometryHelper()).WillByDefault(Return(&geometryHelper));
    ON_CALL(entityRepository, SpawnReadyAgents()).WillByDefault(Return(true));

    RoadGraph roadGraph{};
    RoadGraphVertex vertex{};
    RouteElement routeElement{ROADID, true};
    add_vertex(routeElement, roadGraph);

    ON_CALL(world, IsDirectionalRoadExisting(ROADID, true)).WillByDefault(Return(true));
    ON_CALL(world, IsDirectionalRoadExisting(ROADID, false)).WillByDefault(Return(false));
    ON_CALL(world, IsLaneTypeValid(_, _, _, _)).WillByDefault(Return(true));
    ON_CALL(world, GetRoadGraph(_, _, _))
        .WillByDefault(Return(std::pair<RoadGraph, RoadGraphVertex>{roadGraph, vertex}));
    ON_CALL(world, LaneCoord2WorldCoord(_, _, _, _))
        .WillByDefault(
            [&](units::length::meter_t distanceOnLane, units::length::meter_t offset, std::string, int laneId) {
              return Position{distanceOnLane, offset + 3_m * laneId + 1.5_m, 0_rad, 0_i_m};
            });

    FakeRoadStream* roadStream = new FakeRoadStream;
    ON_CALL(*roadStream, GetStreamPosition(_))
        .WillByDefault(
            [](const GlobalRoadPosition& roadPosition) {
              return StreamPosition{roadPosition.roadPosition.s, 0_m};
            });
    FakeLaneStream* laneStream1 = new FakeLaneStream;
    ON_CALL(*laneStream1, GetLength()).WillByDefault(Return(10000_m));
    ON_CALL(*laneStream1, GetAgentsInRange(_, _))
        .WillByDefault(
            [&](StreamPosition start, StreamPosition end)
            {
              AgentInterfaces agentsInRange;
              for (const auto agent : agentsOnLane[-1])
              {
                auto agentS = agent->GetPositionX();
                if (start.s <= agentS && agentS <= end.s)
                {
                  agentsInRange.push_back(agent);
                }
              }
              return agentsInRange;
            });
    ON_CALL(*laneStream1, GetStreamPosition(_))
        .WillByDefault(
            [](const GlobalRoadPosition& roadPosition) {
              return StreamPosition{roadPosition.roadPosition.s, 0_m};
            });
    ON_CALL(*laneStream1, GetRoadPosition(_))
        .WillByDefault(
            [](const StreamPosition& streamPosition) {
              return GlobalRoadPosition{ROADID, -1, streamPosition.s, streamPosition.t, 0_rad};
            });
    std::vector<std::pair<units::length::meter_t, LaneType>> laneTypes{{0_m, LaneType::Driving}};
    ON_CALL(*laneStream1, GetLaneTypes()).WillByDefault(Return(laneTypes));
    ON_CALL(*laneStream1, GetStreamPosition(_, VariantWith<ObjectPointPredefined>(ObjectPointPredefined::RearCenter)))
        .WillByDefault(
            [](const WorldObjectInterface* object, const ObjectPoint&) {
              return StreamPosition{object->GetPositionX(), 0_m};
            });
    FakeLaneStream* laneStream2 = new FakeLaneStream;
    ON_CALL(*laneStream2, GetLength()).WillByDefault(Return(10000_m));
    ON_CALL(*laneStream2, GetAgentsInRange(_, _))
        .WillByDefault(
            [&](StreamPosition start, StreamPosition end)
            {
              AgentInterfaces agentsInRange;
              for (const auto agent : agentsOnLane[-2])
              {
                auto agentS = agent->GetPositionX();
                if (start.s <= agentS && agentS <= end.s)
                {
                  agentsInRange.push_back(agent);
                }
              }
              return agentsInRange;
            });
    ON_CALL(*laneStream2, GetStreamPosition(_))
        .WillByDefault(
            [](const GlobalRoadPosition& roadPosition) {
              return StreamPosition{roadPosition.roadPosition.s, 0_m};
            });
    ON_CALL(*laneStream2, GetRoadPosition(_))
        .WillByDefault(
            [](const StreamPosition& streamPosition) {
              return GlobalRoadPosition{ROADID, -2, streamPosition.s, streamPosition.t, 0_rad};
            });
    ON_CALL(*laneStream2, GetLaneTypes()).WillByDefault(Return(laneTypes));
    ON_CALL(*laneStream2, GetStreamPosition(_, VariantWith<ObjectPointPredefined>(ObjectPointPredefined::RearCenter)))
        .WillByDefault(
            [](const WorldObjectInterface* object, const ObjectPoint&) {
              return StreamPosition{object->GetPositionX(), 0_m};
            });
    FakeLaneStream* laneStream3 = new FakeLaneStream;
    ON_CALL(*laneStream3, GetLength()).WillByDefault(Return(10000_m));
    ON_CALL(*laneStream3, GetAgentsInRange(_, _))
        .WillByDefault(
            [&](StreamPosition start, StreamPosition end)
            {
              AgentInterfaces agentsInRange;
              for (const auto agent : agentsOnLane[-3])
              {
                auto agentS = agent->GetPositionX();
                if (start.s <= agentS && agentS <= end.s)
                {
                  agentsInRange.push_back(agent);
                }
              }
              return agentsInRange;
            });
    ON_CALL(*laneStream3, GetStreamPosition(_))
        .WillByDefault(
            [](const GlobalRoadPosition& roadPosition) {
              return StreamPosition{roadPosition.roadPosition.s, 0_m};
            });
    ON_CALL(*laneStream3, GetRoadPosition(_))
        .WillByDefault(
            [](const StreamPosition& streamPosition) {
              return GlobalRoadPosition{ROADID, -3, streamPosition.s, streamPosition.t, 0_rad};
            });
    ON_CALL(*laneStream3, GetLaneTypes()).WillByDefault(Return(laneTypes));
    ON_CALL(*laneStream3, GetStreamPosition(_, VariantWith<ObjectPointPredefined>(ObjectPointPredefined::RearCenter)))
        .WillByDefault(
            [](const WorldObjectInterface* object, const ObjectPoint&) {
              return StreamPosition{object->GetPositionX(), 0_m};
            });
    ON_CALL(*roadStream, GetLaneStream(_, -1))
        .WillByDefault(Return(ByMove(std::unique_ptr<LaneStreamInterface>(laneStream1))));
    ON_CALL(*roadStream, GetLaneStream(_, -2))
        .WillByDefault(Return(ByMove(std::unique_ptr<LaneStreamInterface>(laneStream2))));
    ON_CALL(*roadStream, GetLaneStream(_, -3))
        .WillByDefault(Return(ByMove(std::unique_ptr<LaneStreamInterface>(laneStream3))));
    ON_CALL(world, GetRoadStream(std::vector<RouteElement>{{ROADID, true}}))
        .WillByDefault(Return(ByMove(std::unique_ptr<RoadStreamInterface>(roadStream))));

    LaneSection fakeLaneSection1{1000.0_m, 1500.0_m, {-1, -2, -3}};
    LaneSections fakeLaneSections{fakeLaneSection1};
    ON_CALL(world, GetLaneSections(ROADID)).WillByDefault(Return(fakeLaneSections));

    auto vehicleModelParameters = std::make_shared<mantle_api::VehicleProperties>();
    vehicleModelParameters->type = mantle_api::EntityType::kVehicle;
    vehicleModelParameters->classification = mantle_api::VehicleClass::kMedium_car;
    vehicleModelParameters->bounding_box.dimension.length = 5.0_m;
    vehicleModelParameters->bounding_box.dimension.width = 1.0_m;
    vehicleModelParameters->bounding_box.geometric_center.x = 0.5_m;
    const std::string vehicleModelName = "TestVehicle";
    ON_CALL(agentBlueprintProvider, SampleVehicleModelName(_)).WillByDefault(Return(vehicleModelName));
    vehicleModels->insert({vehicleModelName, vehicleModelParameters});
  }

  ~SpawnerPreRun_IntegrationTests()
  {
    for (auto vehicle : vehicles)
    {
      delete vehicle;
    }
    for (auto controller : controllers)
    {
      delete controller;
    }
    for (auto [lane, agents] : agentsOnLane)
    {
      for (auto agent : agents)
      {
        delete agent;
      }
    }
  }

  SpawnerPreRunCommon CreateSpawner() { return SpawnerPreRunCommon{&dependencies, &callbacks}; }

  FakeAgentFactory agentFactory;
  FakeWorld world;
  FakeAgentBlueprintProvider agentBlueprintProvider;
  FakeStochastics stochastics;
  std::shared_ptr<FakeEnvironment> environment = std::make_shared<FakeEnvironment>();
  FakeEntityRepository entityRepository;
  mantle_api::MockControllerRepository controllerRepository;
  core::GeometryHelper geometryHelper;
  std::shared_ptr<Vehicles> vehicleModels = std::make_shared<Vehicles>();
  SpawnPointDependencies dependencies{
      &agentFactory, &world, &agentBlueprintProvider, &stochastics, environment, vehicleModels};
  FakeParameter parameters;

  std::shared_ptr<FakeParameter> spawnZone = std::make_shared<FakeParameter>();
  std::shared_ptr<FakeParameter> trafficGroup1 = std::make_shared<FakeParameter>();
  std::shared_ptr<FakeParameter> trafficGroup2 = std::make_shared<FakeParameter>();
  std::map<std::string, double> trafficGroup1Double{{"Weight", 2}};
  std::map<std::string, const openpass::parameter::StochasticDistribution> trafficGroup1Stochastic{
      {"Velocity", openpass::parameter::UniformDistribution{10, 20}},
      {"TGap", openpass::parameter::UniformDistribution{1, 3}}};
  std::map<std::string, const std::vector<double>> trafficGroup1DoubleVector{{"Homogeneity", {0.2, 0.5}}};
  std::map<std::string, bool> trafficGroup1Bool{{"RightLaneOnly", false}};
  std::shared_ptr<FakeParameter> agentProfile1a = std::make_shared<FakeParameter>();
  std::shared_ptr<FakeParameter> agentProfile1b = std::make_shared<FakeParameter>();
  std::map<std::string, ParameterInterface::ParameterLists> trafficGroup1Lists{
      {"AgentProfiles", {agentProfile1a, agentProfile1b}}};
  std::map<std::string, double> trafficGroup2Double{{"Weight", 5}};
  std::map<std::string, const openpass::parameter::StochasticDistribution> trafficGroup2Stochastic{
      {"Velocity", openpass::parameter::UniformDistribution{20, 30}},
      {"TGap", openpass::parameter::UniformDistribution{2, 4}}};
  std::map<std::string, const std::vector<double>> trafficGroup2DoubleVector{{"Homogeneity", {0.2, 0.5}}};
  std::map<std::string, bool> trafficGroup2Bool{{"RightLaneOnly", true}};
  std::shared_ptr<FakeParameter> agentProfile2a = std::make_shared<FakeParameter>();
  std::shared_ptr<FakeParameter> agentProfile2b = std::make_shared<FakeParameter>();
  std::map<std::string, ParameterInterface::ParameterLists> trafficGroup2Lists{
      {"AgentProfiles", {agentProfile2a, agentProfile2b}}};

  std::map<std::string, double> agentProfile1aDouble{{"Weight", 1}};
  std::map<std::string, double> agentProfile1bDouble{{"Weight", 2}};
  std::map<std::string, double> agentProfile2aDouble{{"Weight", 3}};
  std::map<std::string, double> agentProfile2bDouble{{"Weight", 4}};
  std::map<std::string, const std::string> agentProfile1aString{{"Name", "AgentProfile1a"}};
  std::map<std::string, const std::string> agentProfile1bString{{"Name", "AgentProfile1b"}};
  std::map<std::string, const std::string> agentProfile2aString{{"Name", "AgentProfile2a"}};
  std::map<std::string, const std::string> agentProfile2bString{{"Name", "AgentProfile2b"}};

  std::map<std::string, ParameterInterface::ParameterLists> parameterLists{
      {"SpawnZones", {spawnZone}}, {"TrafficGroups", {trafficGroup1, trafficGroup2}}};

  std::map<std::string, const openpass::parameter::StochasticDistribution> parametersStochastic;
  std::map<std::string, double> parametersDouble;

  FakeCallback callbacks;
  std::map<int, std::vector<FakeAgent*>> agentsOnLane;
  std::vector<mantle_api::MockVehicle*> vehicles;
  std::map<int, std::vector<mantle_api::MockVehicle*>> vehiclesOnLane;
  std::vector<mantle_api::MockController*> controllers;
  std::map<mantle_api::UniqueId, std::string> agentProfiles;
  std::map<mantle_api::IEntity*, std::string> agentProfilesByEntity;
  mantle_api::UniqueId nextId = 0;
};

const RelativeWorldView::Lane relativeLanePlus2{2, true, LaneType::Driving, std::nullopt, std::nullopt};
const RelativeWorldView::Lane relativeLanePlus1{1, true, LaneType::Driving, std::nullopt, std::nullopt};
const RelativeWorldView::Lane relativeLane0{0, true, LaneType::Driving, std::nullopt, std::nullopt};
const RelativeWorldView::Lane relativeLaneMinus1{-1, true, LaneType::Driving, std::nullopt, std::nullopt};
const RelativeWorldView::Lane relativeLaneMinus2{-2, true, LaneType::Driving, std::nullopt, std::nullopt};

void CheckTGap(std::vector<mantle_api::MockVehicle*> vehiclesOnLane,
               double expectedTGap,
               units::length::meter_t minS,
               units::length::meter_t maxS)
{
  for (size_t i = 0; i + 1 < vehiclesOnLane.size(); ++i)
  {
    auto frontAgent = vehiclesOnLane[i + 1];
    auto frontS = frontAgent->GetPosition().x;
    auto rearAgent = vehiclesOnLane[i];
    auto rearS = rearAgent->GetPosition().x;
    if (minS <= frontS && frontS <= maxS)
    {
      auto deltaS = frontS - rearS - rearAgent->GetProperties()->bounding_box.dimension.length;
      auto velocity = rearAgent->GetVelocity().Length();
      EXPECT_THAT(deltaS.value() / velocity.value(), DoubleEq(expectedTGap));
    }
  }
}

void CheckVelocity(std::vector<mantle_api::MockVehicle*> vehiclesOnLane,
                   double expectedVelocity,
                   units::length::meter_t minS,
                   units::length::meter_t maxS)
{
  for (size_t i = 0; i + 1 < vehiclesOnLane.size(); ++i)
  {
    auto frontAgent = vehiclesOnLane[i + 1];
    auto frontS = frontAgent->GetPosition().x;
    auto rearAgent = vehiclesOnLane[i];
    if (minS <= frontS && frontS <= maxS)
    {
      auto velocity = rearAgent->GetVelocity().Length();
      EXPECT_THAT(velocity.value(), DoubleEq(expectedVelocity));
    }
  }
}

void CheckAgentProfile(std::vector<mantle_api::MockVehicle*> vehiclesOnLane,
                       std::map<mantle_api::IEntity*, std::string> agentProfilesByEntity,
                       std::string expectedProfile,
                       units::length::meter_t minS,
                       units::length::meter_t maxS)
{
  for (size_t i = 0; i + 1 < vehiclesOnLane.size(); ++i)
  {
    auto frontAgent = vehiclesOnLane[i + 1];
    auto frontS = frontAgent->GetPosition().x;
    auto rearAgent = vehiclesOnLane[i];
    if (minS <= frontS && frontS <= maxS)
    {
      auto agentProfile = agentProfilesByEntity[rearAgent];
      EXPECT_THAT(agentProfile, Eq(expectedProfile));
    }
  }
}

TEST_F(SpawnerPreRun_IntegrationTests, ThreeContinuesLanes_SpawnWithCorrectTGapAndProfiles)
{
  std::map<std::string, const std::vector<std::string>> parametersStrings{{"Roads", {ROADID}}};
  ON_CALL(*spawnZone, GetParametersStringVector()).WillByDefault(ReturnRef(parametersStrings));
  std::map<std::string, const std::vector<int>> parametersIntVector{{"Lanes", {-1, -2, -3}}};
  ON_CALL(*spawnZone, GetParametersIntVector()).WillByDefault(ReturnRef(parametersIntVector));
  std::map<std::string, double> parametersDouble{{"SStart", 1000.0}, {"SEnd", 1500.0}};
  ON_CALL(*spawnZone, GetParametersDouble()).WillByDefault(ReturnRef(parametersDouble));

  ON_CALL(world, GetDistanceToEndOfLane(_, _, _, _, _, _))
      .WillByDefault(
          [](const RoadGraph&,
             RoadGraphVertex startNode,
             int,
             units::length::meter_t initialSearchDistance,
             units::length::meter_t,
             const LaneTypes&) {
            return RouteQueryResult<units::length::meter_t>{{startNode, 2000.0_m - initialSearchDistance}};
          });
  ON_CALL(world, IsSValidOnLane(ROADID, AllOf(Le(-1), Ge(-3)), AllOf(Ge(0_m), Le(2000_m)))).WillByDefault(Return(true));
  ON_CALL(world, GetLaneWidth(_, _, _)).WillByDefault(Return(3.0_m));
  ON_CALL(world, GetRoadLength(_)).WillByDefault(Return(10000._m));

  RouteQueryResult<std::vector<const WorldObjectInterface*>> noObjects{{0, {}}};
  ON_CALL(world, GetObjectsInRange(_, _, _, _, _, _)).WillByDefault(Return(noObjects));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1{
      {0, {{0_m, 0_m, {relativeLaneMinus2, relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, _, _, _)).WillByDefault(Return(relativeLanesMinus1));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, _, _, _)).WillByDefault(Return(relativeLanesMinus2));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus3{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1, relativeLanePlus2}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -3, _, _, _)).WillByDefault(Return(relativeLanesMinus3));

  auto spawner = CreateSpawner();

  spawner.Trigger(0);

  ASSERT_THAT(vehiclesOnLane[-3].empty(), false);
  CheckTGap(vehiclesOnLane[-3], 3, 1000_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-3], 25, 1000_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-3], agentProfilesByEntity, "AgentProfile2a", 1000_m, 1500_m);

  ASSERT_THAT(vehiclesOnLane[-2].empty(), false);
  CheckTGap(vehiclesOnLane[-2], 2, 1000_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-2], 27, 1000_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile1a", 1000_m, 1500_m);

  ASSERT_THAT(vehiclesOnLane[-1].empty(), false);
  CheckTGap(vehiclesOnLane[-1], 2, 1000_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 40.5, 1000_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-1], agentProfilesByEntity, "AgentProfile1a", 1000_m, 1500_m);
}

TEST_F(SpawnerPreRun_IntegrationTests, IncreasingLaneNumber_SpawnWithCorrectTGapAndProfiles)
{
  std::map<std::string, const std::vector<std::string>> parametersStrings{{"Roads", {ROADID}}};
  ON_CALL(*spawnZone, GetParametersStringVector()).WillByDefault(ReturnRef(parametersStrings));
  std::map<std::string, const std::vector<int>> parametersIntVector{{"Lanes", {-1, -2, -3}}};
  ON_CALL(*spawnZone, GetParametersIntVector()).WillByDefault(ReturnRef(parametersIntVector));
  std::map<std::string, double> parametersDouble{{"SStart", 1000.0}, {"SEnd", 1500.0}};
  ON_CALL(*spawnZone, GetParametersDouble()).WillByDefault(ReturnRef(parametersDouble));

  ON_CALL(world, GetDistanceToEndOfLane(_, _, _, _, _, _))
      .WillByDefault(
          [](const RoadGraph&,
             RoadGraphVertex startNode,
             int,
             units::length::meter_t initialSearchDistance,
             units::length::meter_t,
             const LaneTypes&) {
            return RouteQueryResult<units::length::meter_t>{{startNode, 2000.0_m - initialSearchDistance}};
          });
  ON_CALL(world, IsSValidOnLane(ROADID, AllOf(Le(-1), Ge(-2)), AllOf(Ge(0_m), Le(2000_m)))).WillByDefault(Return(true));
  ON_CALL(world, IsSValidOnLane(ROADID, -3, AllOf(Ge(1400_m), Le(2000_m)))).WillByDefault(Return(true));
  ON_CALL(world, GetLaneWidth(_, _, _)).WillByDefault(Return(3.0_m));
  ON_CALL(world, GetRoadLength(_)).WillByDefault(Return(10000._m));

  RouteQueryResult<std::vector<const WorldObjectInterface*>> noObjects{{0, {}}};
  ON_CALL(world, GetObjectsInRange(_, _, _, _, _, _)).WillByDefault(Return(noObjects));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1First{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, Le(1400_m), _, _)).WillByDefault(Return(relativeLanesMinus1First));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1Second{
      {0, {{0_m, 0_m, {relativeLaneMinus2, relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, Ge(1400_m), _, _)).WillByDefault(Return(relativeLanesMinus1Second));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2First{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, Le(1400_m), _, _)).WillByDefault(Return(relativeLanesMinus2First));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2Second{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, Ge(1400_m), _, _)).WillByDefault(Return(relativeLanesMinus2Second));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus3{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1, relativeLanePlus2}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -3, _, _, _)).WillByDefault(Return(relativeLanesMinus3));

  auto spawner = CreateSpawner();

  spawner.Trigger(0);

  ASSERT_THAT(vehiclesOnLane[-3].empty(), false);
  CheckTGap(vehiclesOnLane[-3], 3, 1400_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-3], 25, 1400_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-3], agentProfilesByEntity, "AgentProfile2a", 1400_m, 1500_m);

  ASSERT_THAT(vehiclesOnLane[-2].empty(), false);
  CheckTGap(vehiclesOnLane[-2], 2, 1400_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-2], 27, 1400_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile1a", 1400_m, 1500_m);
  CheckTGap(vehiclesOnLane[-2], 3, 1000_m, 1400_m);
  CheckVelocity(vehiclesOnLane[-2], 25, 1000_m, 1400_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile2a", 1000_m, 1400_m);

  ASSERT_THAT(vehiclesOnLane[-1].empty(), false);
  CheckTGap(vehiclesOnLane[-1], 2, 1000_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 40.5, 1400_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 27, 1000_m, 1400_m);
  CheckAgentProfile(vehiclesOnLane[-1], agentProfilesByEntity, "AgentProfile1a", 1000_m, 1500_m);
}

TEST_F(SpawnerPreRun_IntegrationTests, DecreasingLaneNumber_SpawnWithCorrectTGapAndProfiles)
{
  std::map<std::string, const std::vector<std::string>> parametersStrings{{"Roads", {ROADID}}};
  ON_CALL(*spawnZone, GetParametersStringVector()).WillByDefault(ReturnRef(parametersStrings));
  std::map<std::string, const std::vector<int>> parametersIntVector{{"Lanes", {-1, -2, -3}}};
  ON_CALL(*spawnZone, GetParametersIntVector()).WillByDefault(ReturnRef(parametersIntVector));
  std::map<std::string, double> parametersDouble{{"SStart", 1000.0}, {"SEnd", 1500.0}};
  ON_CALL(*spawnZone, GetParametersDouble()).WillByDefault(ReturnRef(parametersDouble));

  ON_CALL(world, GetDistanceToEndOfLane(_, _, -3, _, _, _))
      .WillByDefault(
          [](const RoadGraph&,
             RoadGraphVertex startNode,
             int,
             units::length::meter_t initialSearchDistance,
             units::length::meter_t,
             const LaneTypes&) {
            return RouteQueryResult<units::length::meter_t>{{startNode, 1200.0_m - initialSearchDistance}};
          });
  ON_CALL(world, GetDistanceToEndOfLane(_, _, AllOf(Le(-1), Ge(-2)), _, _, _))
      .WillByDefault(
          [](const RoadGraph&,
             RoadGraphVertex startNode,
             int,
             units::length::meter_t initialSearchDistance,
             units::length::meter_t,
             const LaneTypes&) {
            return RouteQueryResult<units::length::meter_t>{{startNode, 2000.0_m - initialSearchDistance}};
          });
  ON_CALL(world, IsSValidOnLane(ROADID, AllOf(Le(-1), Ge(-2)), AllOf(Ge(0_m), Le(2000_m)))).WillByDefault(Return(true));
  ON_CALL(world, IsSValidOnLane(ROADID, -3, AllOf(Ge(0_m), Le(1200_m)))).WillByDefault(Return(true));
  ON_CALL(world, GetLaneWidth(_, _, _)).WillByDefault(Return(3.0_m));
  ON_CALL(world, GetRoadLength(_)).WillByDefault(Return(10000._m));

  RouteQueryResult<std::vector<const WorldObjectInterface*>> noObjects{{0, {}}};
  ON_CALL(world, GetObjectsInRange(_, _, _, _, _, _)).WillByDefault(Return(noObjects));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1First{
      {0, {{0_m, 0_m, {relativeLaneMinus2, relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, Le(1200_m), _, _)).WillByDefault(Return(relativeLanesMinus1First));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1Second{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, Ge(1200_m), _, _)).WillByDefault(Return(relativeLanesMinus1Second));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2First{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, Le(1200_m), _, _)).WillByDefault(Return(relativeLanesMinus2First));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2Second{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, Ge(1200_m), _, _)).WillByDefault(Return(relativeLanesMinus2Second));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus3{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1, relativeLanePlus2}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -3, _, _, _)).WillByDefault(Return(relativeLanesMinus3));

  auto spawner = CreateSpawner();

  spawner.Trigger(0);

  ASSERT_THAT(vehiclesOnLane[-3].empty(), false);
  CheckTGap(vehiclesOnLane[-3], 3, 1000_m, 1200_m);
  CheckVelocity(vehiclesOnLane[-3], 25, 1000_m, 1200_m);
  CheckAgentProfile(vehiclesOnLane[-3], agentProfilesByEntity, "AgentProfile2a", 1000_m, 1200_m);

  ASSERT_THAT(vehiclesOnLane[-2].empty(), false);
  CheckTGap(vehiclesOnLane[-2], 3, 1200_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-2], 25, 1200_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile2a", 1200_m, 1500_m);
  CheckTGap(vehiclesOnLane[-2], 2, 1000_m, 1200_m);
  CheckVelocity(vehiclesOnLane[-2], 27, 1000_m, 1200_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile1a", 1000_m, 1200_m);

  ASSERT_THAT(vehiclesOnLane[-1].empty(), false);
  CheckTGap(vehiclesOnLane[-1], 2, 1000_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 27, 1200_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 40.5, 1000_m, 1200_m);
  CheckAgentProfile(vehiclesOnLane[-1], agentProfilesByEntity, "AgentProfile1a", 1000_m, 1500_m);
}

TEST_F(SpawnerPreRun_IntegrationTests, RightLaneStartsAndEndsWithinRange_SpawnWithCorrectTGapAndProfiles)
{
  std::map<std::string, const std::vector<std::string>> parametersStrings{{"Roads", {ROADID}}};
  ON_CALL(*spawnZone, GetParametersStringVector()).WillByDefault(ReturnRef(parametersStrings));
  std::map<std::string, const std::vector<int>> parametersIntVector{{"Lanes", {-1, -2, -3}}};
  ON_CALL(*spawnZone, GetParametersIntVector()).WillByDefault(ReturnRef(parametersIntVector));
  std::map<std::string, double> parametersDouble{{"SStart", 1000.0}, {"SEnd", 1500.0}};
  ON_CALL(*spawnZone, GetParametersDouble()).WillByDefault(ReturnRef(parametersDouble));

  ON_CALL(world, GetDistanceToEndOfLane(_, _, -3, _, _, _))
      .WillByDefault(
          [](const RoadGraph&,
             RoadGraphVertex startNode,
             int,
             units::length::meter_t initialSearchDistance,
             units::length::meter_t,
             const LaneTypes&) {
            return RouteQueryResult<units::length::meter_t>{{startNode, 1400.0_m - initialSearchDistance}};
          });
  ON_CALL(world, GetDistanceToEndOfLane(_, _, AllOf(Le(-1), Ge(-2)), _, _, _))
      .WillByDefault(
          [](const RoadGraph&,
             RoadGraphVertex startNode,
             int,
             units::length::meter_t initialSearchDistance,
             units::length::meter_t,
             const LaneTypes&) {
            return RouteQueryResult<units::length::meter_t>{{startNode, 2000.0_m - initialSearchDistance}};
          });
  ON_CALL(world, IsSValidOnLane(ROADID, AllOf(Le(-1), Ge(-2)), AllOf(Ge(0_m), Le(2000_m)))).WillByDefault(Return(true));
  ON_CALL(world, IsSValidOnLane(ROADID, -3, AllOf(Ge(1200_m), Le(1400_m)))).WillByDefault(Return(true));
  ON_CALL(world, GetLaneWidth(_, _, _)).WillByDefault(Return(3.0_m));
  ON_CALL(world, GetRoadLength(_)).WillByDefault(Return(10000._m));

  RouteQueryResult<std::vector<const WorldObjectInterface*>> noObjects{{0, {}}};
  ON_CALL(world, GetObjectsInRange(_, _, _, _, _, _)).WillByDefault(Return(noObjects));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1First{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, Ge(1400_m), _, _)).WillByDefault(Return(relativeLanesMinus1First));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1Second{
      {0, {{0_m, 0_m, {relativeLaneMinus2, relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, AllOf(Ge(1200_m), Le(1400_m)), _, _))
      .WillByDefault(Return(relativeLanesMinus1Second));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus1Third{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -1, Le(1200_m), _, _)).WillByDefault(Return(relativeLanesMinus1Third));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2First{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, Ge(1400_m), _, _)).WillByDefault(Return(relativeLanesMinus2First));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2Second{
      {0, {{0_m, 0_m, {relativeLaneMinus1, relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, AllOf(Ge(1200_m), Le(1400_m)), _, _))
      .WillByDefault(Return(relativeLanesMinus2Second));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus2Third{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -2, Le(1200_m), _, _)).WillByDefault(Return(relativeLanesMinus2Third));
  RouteQueryResult<RelativeWorldView::Lanes> relativeLanesMinus3{
      {0, {{0_m, 0_m, {relativeLane0, relativeLanePlus1, relativeLanePlus2}}}}};
  ON_CALL(world, GetRelativeLanes(_, _, -3, _, _, _)).WillByDefault(Return(relativeLanesMinus3));

  auto spawner = CreateSpawner();

  spawner.Trigger(0);

  ASSERT_THAT(vehiclesOnLane[-3].empty(), false);
  CheckTGap(vehiclesOnLane[-3], 3, 1200_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-3], 25, 1200_m, 1400_m);
  CheckAgentProfile(vehiclesOnLane[-3], agentProfilesByEntity, "AgentProfile2a", 1200_m, 1400_m);

  ASSERT_THAT(vehiclesOnLane[-2].empty(), false);
  CheckTGap(vehiclesOnLane[-2], 3, 1400_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-2], 25, 1400_m, 1500_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile2a", 1400_m, 1500_m);
  CheckTGap(vehiclesOnLane[-2], 2, 1200_m, 1400_m);
  CheckVelocity(vehiclesOnLane[-2], 27, 1200_m, 1400_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile1a", 1200_m, 1400_m);
  CheckTGap(vehiclesOnLane[-2], 3, 1000_m, 1200_m);
  CheckVelocity(vehiclesOnLane[-2], 25, 1000_m, 1200_m);
  CheckAgentProfile(vehiclesOnLane[-2], agentProfilesByEntity, "AgentProfile2a", 1000_m, 1200_m);

  ASSERT_THAT(vehiclesOnLane[-1].empty(), false);
  CheckTGap(vehiclesOnLane[-1], 2, 1000_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 27, 1400_m, 1500_m);
  CheckVelocity(vehiclesOnLane[-1], 40.5, 1200_m, 1400_m);
  CheckVelocity(vehiclesOnLane[-1], 27, 1000_m, 1200_m);
  CheckAgentProfile(vehiclesOnLane[-1], agentProfilesByEntity, "AgentProfile1a", 1000_m, 1500_m);
}
