/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/*
 * Author: Daniel Becker
 * Based on: PMSF FMU Framework for FMI 2.0 Co-Simulation FMUs
 *
 * (C) 2019 Institute for Automotive Engineering, RWTH Aachen Univ.
 * (C) 2020 BMW AG
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "OSMPDummyTCTU.h"

/*
 * Debug Breaks
 *
 * If you define DEBUG_BREAKS the FMU will automatically break
 * into an attached Debugger on all major computation functions.
 * Note that the FMU is likely to break all environments if no
 * Debugger is actually attached when the breaks are triggered.
 */
#if defined(DEBUG_BREAKS) && !defined(NDEBUG)
#if defined(__has_builtin) && !defined(__ibmxl__)
#if __has_builtin(__builtin_debugtrap)
#define DEBUGBREAK() __builtin_debugtrap()
#elif __has_builtin(__debugbreak)
#define DEBUGBREAK() __debugbreak()
#endif
#endif
#if !defined(DEBUGBREAK)
#if defined(_MSC_VER) || defined(__INTEL_COMPILER)
#include <intrin.h>
#define DEBUGBREAK() __debugbreak()
#else
#include <signal.h>
#if defined(SIGTRAP)
#define DEBUGBREAK() raise(SIGTRAP)
#else
#define DEBUGBREAK() raise(SIGABRT)
#endif
#endif
#endif
#else
#define DEBUGBREAK()
#endif

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <math.h>
#include <mutex>
#include <string>

//#include "Environment/EnvironmentBuilder.hh"
//#include "Path/GraphBuilder.hh"
//#include "Path/Graph.hh"
//#include "Environment/Environment.hh"
#include <memory>

/// Mutex for controlling access to the variables below (for changing them in a
/// mutable manner)
std::mutex sEnvironmentMutex;
/// Pointer to an course environment shared by multiple pedestrians
// TODO: This should be made const
//static std::shared_ptr<Env::Environment> sEnvironment(nullptr);
/// Pointer to a strategy level graph shared by multiple pedestrians
//static Path::GraphBuilder::StrategyGraphPtr sStrategyGraph(nullptr);
/**
 * The serialized form of the last ground truth init received. The idea here:
 * multiple instances of this FMU ideally should share various data structures,
 * which are based only on the ground truth received during initialization.
 * To do this, the ground truth is cached. If an instance is initialized with
 * the same ground truth received previously, it can reuse the last Environment
 * and the last StrategyGraph.
 */
static std::vector<char> sCachedGroundTruthInit;

#ifdef PRIVATE_LOG_PATH
ofstream COSMPTrafficAgent::private_log_file;
#endif

/*
 * ProtocolBuffer Accessors
 */

static void* decode_integer_to_pointer(fmi2Integer hi, fmi2Integer lo)
{
#if PTRDIFF_MAX == INT64_MAX
  union addrconv
  {
    struct
    {
      int lo;
      int hi;
    } base;
    unsigned long long address;
  } myaddr;
  myaddr.base.lo = lo;
  myaddr.base.hi = hi;
  return reinterpret_cast<void*>(myaddr.address);
#elif PTRDIFF_MAX == INT32_MAX
  return reinterpret_cast<void*>(lo);
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}

static void encode_pointer_to_integer(const void* ptr, fmi2Integer& hi, fmi2Integer& lo)
{
#if PTRDIFF_MAX == INT64_MAX
  union addrconv
  {
    struct
    {
      int lo;
      int hi;
    } base;
    unsigned long long address;
  } myaddr;
  myaddr.address = reinterpret_cast<unsigned long long>(ptr);
  hi = myaddr.base.hi;
  lo = myaddr.base.lo;
#elif PTRDIFF_MAX == INT32_MAX
  hi = 0;
  lo = reinterpret_cast<int>(ptr);
#else
#error "Cannot determine 32bit or 64bit environment!"
#endif
}

void COSMPTrafficAgent::refresh_fmi_sensor_view_config_request()
{
  osi3::SensorViewConfiguration config;
  if (get_fmi_sensor_view_config(config))
    set_fmi_sensor_view_config_request(config);
  else
  {
    config.Clear();
    config.mutable_version()->CopyFrom(
        osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version));
    // We want a full view of the environment, in all directions
    config.set_field_of_view_horizontal(2 * M_PI);
    config.set_field_of_view_vertical(2 * M_PI);
    // 200m view. This primarily needs to be far enough to react to vehicles early enough
    config.set_range(200);
    // update rate: 50Hz (20ms). Must be identical to stepSize in the
    // modelDescription.xml.
    // Note: the model doesn't care that much about the exact cycle time.
    config.mutable_update_cycle_time()->set_seconds(0);
    config.mutable_update_cycle_time()->set_nanos(20000000);
    config.mutable_update_cycle_offset()->Clear();
    // The model doesn't need any road information from the sensor view,
    // since it already has this information from the GroundTruthInit
    // message. This can greatly improve performance.
    //config.set_omit_static_information(true);
    set_fmi_sensor_view_config_request(config);
  }
}

bool COSMPTrafficAgent::get_fmi_sensor_view_config(osi3::SensorViewConfiguration& data)
{
  if (integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX] <= 0) return false;

  void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX],
                                           integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX]);
  normal_log("OSMP",
             "Got %08X %08X, reading %d bytes from %p to get SensorViewConfiguration ...",
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASEHI_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_BASELO_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX],
             buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize SensorViewConfiguration");
    return false;
  }
  return true;
}

bool COSMPTrafficAgent::get_fmi_sensor_view_in(osi3::SensorView& data)
{
  if (integer_vars[FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX] <= 0) return false;

  void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASEHI_IDX],
                                           integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASELO_IDX]);
  normal_log("OSMP",
             "Got %08X %08X, reading %d bytes from %p to get SensorView ...",
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASEHI_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_BASELO_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX],
             buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_SENSORVIEW_IN_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize SensorView");
    return false;
  }
  return true;
}

bool COSMPTrafficAgent::get_fmi_traffic_command_in(osi3::TrafficCommand& data)
{
  if (integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX] <= 0) return false;

  void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASEHI_IDX],
                                           integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASELO_IDX]);
  normal_log("OSMP",
             "Got %08X %08X, reading %d bytes from %p to get TrafficCommand ...",
             integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASEHI_IDX],
             integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_BASELO_IDX],
             integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX],
             buffer);
  if (!data.ParseFromArray(buffer, integer_vars[FMI_INTEGER_TRAFFICCOMMAND_IN_SIZE_IDX]))
  {
    normal_log("OSMP", "Could not deserialize TrafficCommand");
    return false;
  }
  return true;
}

/**
 * Serializes to the given buffer.
 * @return true if successful.
 */
template <typename T>
static bool serializeToVector(std::vector<char>& buffer, const T& data)
{
#if GOOGLE_PROTOBUF_VERSION >= 3004000
  buffer.resize(data.ByteSizeLong());
#else
  buffer.resize(static_cast<int>(data.ByteSize()));
#endif
  return data.SerializeToArray(buffer.data(), static_cast<int>(buffer.size()));
}

void COSMPTrafficAgent::set_fmi_traffic_update_out(const osi3::TrafficUpdate& data)
{
  serializeToVector(currentOutputBuffer, data);
  encode_pointer_to_integer(currentOutputBuffer.data(),
                            integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX],
                            integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX]);
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX] = (fmi2Integer)currentOutputBuffer.size();
  normal_log("OSMP",
             "Providing %08X %08X, writing %d bytes from %p to set TrafficUpdate ...",
             integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX],
             integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX],
             integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX],
             currentOutputBuffer.data());
  using std::swap;
  swap(currentOutputBuffer, lastOutputBuffer);
}

void COSMPTrafficAgent::set_fmi_sensor_view_config_request(const osi3::SensorViewConfiguration& data)
{
  serializeToVector(currentConfigRequestBuffer, data);
  encode_pointer_to_integer(currentConfigRequestBuffer.data(),
                            integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX],
                            integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX]);
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX] = (fmi2Integer)currentConfigRequestBuffer.size();
  normal_log("OSMP",
             "Providing %08X %08X, writing %d bytes from %p to set SensorViewConfiguration ...",
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX],
             integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX],
             currentConfigRequestBuffer.data());
  using std::swap;
  swap(currentConfigRequestBuffer, lastConfigRequestBuffer);
}

void COSMPTrafficAgent::reset_fmi_traffic_update_out()
{
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_SIZE_IDX] = 0;
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASEHI_IDX] = 0;
  integer_vars[FMI_INTEGER_TRAFFICUPDATE_OUT_BASELO_IDX] = 0;
}

void COSMPTrafficAgent::reset_fmi_sensor_view_config_request()
{
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX] = 0;
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX] = 0;
  integer_vars[FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX] = 0;
}

/*
 * Actual Core Content
 */

fmi2Status COSMPTrafficAgent::doInit()
{
  DEBUGBREAK();

  /* Booleans */
  for (size_t i = 0; i < FMI_BOOLEAN_VARS; i++) boolean_vars[i] = fmi2False;

  /* Integers */
  for (size_t i = 0; i < FMI_INTEGER_VARS; i++) integer_vars[i] = 0;

  /* Reals */
  for (size_t i = 0; i < FMI_REAL_VARS; i++) real_vars[i] = 0.0;

  /* Strings */
  for (size_t i = 0; i < FMI_STRING_VARS; i++) string_vars[i] = "";

  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::doStart(fmi2Boolean toleranceDefined,
                                      fmi2Real tolerance,
                                      fmi2Real startTime,
                                      fmi2Boolean stopTimeDefined,
                                      fmi2Real stopTime)
{
  DEBUGBREAK();
  (void)toleranceDefined;
  (void)tolerance;
  (void)startTime;
  (void)stopTimeDefined;
  (void)stopTime;

  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::doEnterInitializationMode()
{
  DEBUGBREAK();

  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::doExitInitializationMode()
{
  DEBUGBREAK();

  if (integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_SIZE_IDX] <= 0)
  {
    normal_log("OSMP", "Did not receive a GroundTruthInit message");
    return fmi2Error;
  }

  {
    const size_t dataSize = static_cast<size_t>(integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_SIZE_IDX]);
    void* buffer = decode_integer_to_pointer(integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_BASEHI_IDX],
                                             integer_vars[FMI_INTEGER_GROUNDTRUTHINIT_IN_BASELO_IDX]);

    std::lock_guard<std::mutex> guard{sEnvironmentMutex};
    if (dataSize == sCachedGroundTruthInit.size()
        && memcmp(buffer, sCachedGroundTruthInit.data(), sCachedGroundTruthInit.size()) == 0)
    {
      normal_log("OSI", "Received identical configuration than previous version - everything OK");
    }
    else
    {
      if (sCachedGroundTruthInit.empty())
        normal_log("OSI", "Received new ground truth initialization");
      else
        normal_log("OSI",
                   "Received different ground truth initialization - if you didn't load a different map, this is "
                   "probably a bug in the environment");

      sCachedGroundTruthInit.resize(dataSize);
      memcpy(sCachedGroundTruthInit.data(), buffer, dataSize);

      osi3::GroundTruth groundTruthInit;
      bool ok = groundTruthInit.ParseFromArray(buffer, static_cast<int>(dataSize));
      if (!ok)
      {
        normal_log("OSI", "Could not deserialize GroundTruthInit message");
        return fmi2Error;
      }
    }
  }

  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::doCalc(fmi2Real currentCommunicationPoint,
                                     fmi2Real communicationStepSize,
                                     fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
{
  DEBUGBREAK();
  (void)noSetFMUStatePriorToCurrentPointfmi2Component;

  osi3::SensorView currentViewIn;
  osi3::TrafficCommand currentCommandIn;
  osi3::TrafficUpdate currentOut;
  double time = currentCommunicationPoint;
  normal_log("OSI",
             "Calculating Trajectory Agent at %g for %g (step size %g)",
             currentCommunicationPoint,
             time,
             communicationStepSize);

  // Get sensor view (ignored in MS1)
  if (!get_fmi_sensor_view_in(currentViewIn))
  {
    normal_log("OSI", "No valid sensor view input, therefore providing no valid output.");
    reset_fmi_traffic_update_out();
    set_fmi_valid(false);
    return fmi2OK;
  }

  if (!get_fmi_traffic_command_in(currentCommandIn))
  {
    normal_log("OSI", "No valid command input, therefore providing no valid output.");
    reset_fmi_traffic_update_out();
    set_fmi_valid(false);
    return fmi2OK;
  }

  /* Clear Output */
  currentOut.Clear();
  currentOut.mutable_version()->CopyFrom(
      osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version));
  /* Adjust Timestamp */
  currentOut.mutable_timestamp()->set_seconds((long long int)floor(time));
  currentOut.mutable_timestamp()->set_nanos((int)((time - floor(time)) * 1000000000.0));

  /* Determine Our Vehicle ID and copy external state */
  osi3::Identifier ego_id = currentViewIn.global_ground_truth().host_vehicle_id();
  normal_log("OSI", "Looking for EgoVehicle with ID: %llu", static_cast<unsigned long long>(ego_id.value()));
  std::for_each(currentViewIn.global_ground_truth().moving_object().begin(),
                currentViewIn.global_ground_truth().moving_object().end(),
                [this, ego_id, &currentOut](const osi3::MovingObject& obj)
                {
                  normal_log("OSI",
                             "MovingObject with ID %llu is EgoVehicle: %d",
                             static_cast<unsigned long long>(obj.id().value()),
                             obj.id().value() == ego_id.value());
                  if (obj.id().value() == ego_id.value())
                  {
                    normal_log(
                        "OSI", "Found EgoVehicle with ID: %llu", static_cast<unsigned long long>(obj.id().value()));
                    currentOut.add_update()->CopyFrom(obj);
                  }
                });

  auto internalState = currentOut.add_internal_state();
  internalState->mutable_vehicle_steering()->mutable_vehicle_steering_wheel()->set_angle(0.5);
  internalState->mutable_vehicle_powertrain()->set_pedal_position_acceleration(0.2);
  internalState->mutable_vehicle_powertrain()->set_gear_transmission(4);
  internalState->mutable_vehicle_brake_system()->set_pedal_position_brake(0.3);

  if (currentCommandIn.action_size() > 0)
  {
    if (currentCommandIn.action(0).has_follow_trajectory_action())
    {
      /* Forward dummy values */
      auto trajectoryPoint
          = currentCommandIn.action(0).follow_trajectory_action().trajectory_point(trajectory_point_index);
      auto baseMoving = currentOut.mutable_update(0)->mutable_base();

      auto position = baseMoving->mutable_position();
      auto bb_center_to_rear = currentOut.update(0).vehicle_attributes().bbcenter_to_rear();
      position->set_x(trajectoryPoint.position().x()
                      - bb_center_to_rear.x() * std::cos(trajectoryPoint.orientation().yaw()));
      position->set_y(trajectoryPoint.position().y()
                      - bb_center_to_rear.x() * std::sin(trajectoryPoint.orientation().yaw()));

      auto orientation = baseMoving->mutable_orientation();
      orientation->set_yaw(trajectoryPoint.orientation().yaw());

      auto velocity = baseMoving->mutable_velocity();
      velocity->set_x(10.0 * trajectoryPoint.position().x());
      velocity->set_y(10.0 * trajectoryPoint.position().y());

      auto acceleration = baseMoving->mutable_acceleration();
      acceleration->set_x(5.0 * trajectoryPoint.position().x());
      acceleration->set_y(5.0 * trajectoryPoint.position().y());

      if ((trajectory_point_index + 1) < currentCommandIn.action(0).follow_trajectory_action().trajectory_point_size())
      {
        trajectory_point_index++;
      }
    }
    else if (currentCommandIn.action(0).has_acquire_global_position_action())
    {
      auto globalPosition = currentCommandIn.action(0).acquire_global_position_action().position();
      auto globalOrientation = currentCommandIn.action(0).acquire_global_position_action().orientation();

      auto baseMoving = currentOut.mutable_update(0)->mutable_base();

      auto position = baseMoving->mutable_position();
      auto bb_center_to_rear = currentOut.update(0).vehicle_attributes().bbcenter_to_rear();
      position->set_x(globalPosition.x() - bb_center_to_rear.x() * std::cos(globalOrientation.yaw()));
      position->set_y(globalPosition.y() - bb_center_to_rear.x() * std::sin(globalOrientation.yaw()));

      auto orientation = baseMoving->mutable_orientation();
      orientation->set_yaw(globalOrientation.yaw());

      auto velocity = baseMoving->mutable_velocity();
      velocity->set_x(0.0);
      velocity->set_y(0.0);

      auto acceleration = baseMoving->mutable_acceleration();
      acceleration->set_x(0.0);
      acceleration->set_y(0.0);
    }
    else if (currentCommandIn.action(0).has_speed_action())
    {
      auto speed = currentCommandIn.action(0).speed_action().absolute_target_speed();

      auto baseMoving = currentOut.mutable_update(0)->mutable_base();
      auto velocity = baseMoving->mutable_velocity();
      velocity->set_x(speed);
      velocity->set_y(0.0);
    }
    else if (currentCommandIn.action(0).has_custom_action())
    {
      auto customCommand = currentCommandIn.action(0).custom_action().command();
      auto baseMoving = currentOut.mutable_update(0)->mutable_base();

      auto position = baseMoving->mutable_position();
      auto velocity = baseMoving->mutable_velocity();
      auto acceleration = baseMoving->mutable_acceleration();
      auto orientation = baseMoving->orientation();

      auto bb_center_to_rear = currentOut.update(0).vehicle_attributes().bbcenter_to_rear();

      (customCommand.find("PositionX:") == 0)
          ? position->set_x(std::stod(customCommand.substr(11)) - bb_center_to_rear.x() * std::cos(orientation.yaw()))
          : position->set_x(0.0);
      (customCommand.find("PositionY:") == 0)
          ? position->set_y(std::stod(customCommand.substr(11)) - bb_center_to_rear.y() * std::sin(orientation.yaw()))
          : position->set_y(0.0);

      (customCommand.find("VelocityX:") == 0) ? velocity->set_x(std::stod(customCommand.substr(11)))
                                              : velocity->set_x(0.0);
      (customCommand.find("VelocityY:") == 0) ? velocity->set_y(std::stod(customCommand.substr(11)))
                                              : velocity->set_y(0.0);

      (customCommand.find("AccelerationX:") == 0) ? acceleration->set_x(std::stod(customCommand.substr(14)))
                                                  : acceleration->set_x(0.0);
      (customCommand.find("AccelerationY:") == 0) ? acceleration->set_y(std::stod(customCommand.substr(14)))
                                                  : acceleration->set_y(0.0);
    }
  }

  /* Serialize */
  set_fmi_traffic_update_out(currentOut);
  set_fmi_valid(true);

  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::doTerm()
{
  DEBUGBREAK();

  return fmi2OK;
}

void COSMPTrafficAgent::doFree()
{
  DEBUGBREAK();
}

/*
 * Generic C++ Wrapper Code
 */

COSMPTrafficAgent::COSMPTrafficAgent(fmi2String theinstanceName,
                                     fmi2Type thefmuType,
                                     fmi2String thefmuGUID,
                                     fmi2String thefmuResourceLocation,
                                     const fmi2CallbackFunctions* thefunctions,
                                     fmi2Boolean thevisible,
                                     fmi2Boolean theloggingOn)
    : instanceName(theinstanceName),
      fmuType(thefmuType),
      fmuGUID(thefmuGUID),
      fmuResourceLocation(thefmuResourceLocation),
      visible(!!thevisible),
      loggingOn(!!theloggingOn),
      functions(*thefunctions),
      simulationStarted(false)
{
  loggingCategories.clear();
  loggingCategories.insert("FMI");
  loggingCategories.insert("OSMP");
  loggingCategories.insert("OSI");
  loggingCategories.insert("Logic");
}

COSMPTrafficAgent::~COSMPTrafficAgent() {}

fmi2Status COSMPTrafficAgent::SetDebugLogging(fmi2Boolean theloggingOn,
                                              size_t nCategories,
                                              const fmi2String categories[])
{
  fmi_verbose_log("fmi2SetDebugLogging(%s)", theloggingOn ? "true" : "false");
  loggingOn = theloggingOn ? true : false;
  if (categories && (nCategories > 0))
  {
    loggingCategories.clear();
    for (size_t i = 0; i < nCategories; i++)
    {
      if (strcmp(categories[i], "FMI") == 0)
        loggingCategories.insert("FMI");
      else if (strcmp(categories[i], "OSMP") == 0)
        loggingCategories.insert("OSMP");
      else if (strcmp(categories[i], "OSI") == 0)
        loggingCategories.insert("OSI");
      else if (strcmp(categories[i], "Logic") == 0)
        loggingCategories.insert("Logic");
    }
  }
  else
  {
    loggingCategories.clear();
    loggingCategories.insert("FMI");
    loggingCategories.insert("OSMP");
    loggingCategories.insert("OSI");
    loggingCategories.insert("Logic");
  }
  return fmi2OK;
}

fmi2Component COSMPTrafficAgent::Instantiate(fmi2String instanceName,
                                             fmi2Type fmuType,
                                             fmi2String fmuGUID,
                                             fmi2String fmuResourceLocation,
                                             const fmi2CallbackFunctions* functions,
                                             fmi2Boolean visible,
                                             fmi2Boolean loggingOn)
{
  COSMPTrafficAgent* myc
      = new COSMPTrafficAgent(instanceName, fmuType, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);

  if (myc == NULL)
  {
    fmi_verbose_log_global("fmi2Instantiate(\"%s\",%d,\"%s\",\"%s\",\"%s\",%d,%d) = NULL (alloc failure)",
                           instanceName,
                           fmuType,
                           fmuGUID,
                           (fmuResourceLocation != NULL) ? fmuResourceLocation : "<NULL>",
                           "FUNCTIONS",
                           visible,
                           loggingOn);
    return NULL;
  }

  if (myc->doInit() != fmi2OK)
  {
    fmi_verbose_log_global("fmi2Instantiate(\"%s\",%d,\"%s\",\"%s\",\"%s\",%d,%d) = NULL (doInit failure)",
                           instanceName,
                           fmuType,
                           fmuGUID,
                           (fmuResourceLocation != NULL) ? fmuResourceLocation : "<NULL>",
                           "FUNCTIONS",
                           visible,
                           loggingOn);
    delete myc;
    return NULL;
  }
  else
  {
    fmi_verbose_log_global("fmi2Instantiate(\"%s\",%d,\"%s\",\"%s\",\"%s\",%d,%d) = %p",
                           instanceName,
                           fmuType,
                           fmuGUID,
                           (fmuResourceLocation != NULL) ? fmuResourceLocation : "<NULL>",
                           "FUNCTIONS",
                           visible,
                           loggingOn,
                           myc);
    return (fmi2Component)myc;
  }
}

fmi2Status COSMPTrafficAgent::SetupExperiment(fmi2Boolean toleranceDefined,
                                              fmi2Real tolerance,
                                              fmi2Real startTime,
                                              fmi2Boolean stopTimeDefined,
                                              fmi2Real stopTime)
{
  fmi_verbose_log(
      "fmi2SetupExperiment(%d,%g,%g,%d,%g)", toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
  return doStart(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
}

fmi2Status COSMPTrafficAgent::EnterInitializationMode()
{
  fmi_verbose_log("fmi2EnterInitializationMode()");
  return doEnterInitializationMode();
}

fmi2Status COSMPTrafficAgent::ExitInitializationMode()
{
  fmi_verbose_log("fmi2ExitInitializationMode()");
  simulationStarted = true;
  return doExitInitializationMode();
}

fmi2Status COSMPTrafficAgent::DoStep(fmi2Real currentCommunicationPoint,
                                     fmi2Real communicationStepSize,
                                     fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
{
  fmi_verbose_log("fmi2DoStep(%g,%g,%d)",
                  currentCommunicationPoint,
                  communicationStepSize,
                  noSetFMUStatePriorToCurrentPointfmi2Component);
  return doCalc(currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
}

fmi2Status COSMPTrafficAgent::Terminate()
{
  fmi_verbose_log("fmi2Terminate()");
  return doTerm();
}

fmi2Status COSMPTrafficAgent::Reset()
{
  fmi_verbose_log("fmi2Reset()");

  doFree();
  simulationStarted = false;
  return doInit();
}

void COSMPTrafficAgent::FreeInstance()
{
  fmi_verbose_log("fmi2FreeInstance()");
  doFree();
}

fmi2Status COSMPTrafficAgent::GetReal(const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
{
  fmi_verbose_log("fmi2GetReal(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_REAL_VARS)
      value[i] = real_vars[vr[i]];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::GetInteger(const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
{
  fmi_verbose_log("fmi2GetInteger(...)");
  bool needRefresh = !simulationStarted;
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_INTEGER_VARS)
    {
      if (needRefresh
          && (vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASEHI_IDX
              || vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_BASELO_IDX
              || vr[i] == FMI_INTEGER_SENSORVIEW_IN_CONFIG_REQUEST_SIZE_IDX))
      {
        refresh_fmi_sensor_view_config_request();
        needRefresh = false;
      }
      value[i] = integer_vars[vr[i]];
    }
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::GetBoolean(const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
{
  fmi_verbose_log("fmi2GetBoolean(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_BOOLEAN_VARS)
      value[i] = boolean_vars[vr[i]];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::GetString(const fmi2ValueReference vr[], size_t nvr, fmi2String value[])
{
  fmi_verbose_log("fmi2GetString(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_STRING_VARS)
      value[i] = string_vars[vr[i]].c_str();
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::SetReal(const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
{
  fmi_verbose_log("fmi2SetReal(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_REAL_VARS)
      real_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::SetInteger(const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[])
{
  fmi_verbose_log("fmi2SetInteger(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    //log("OSMP", "SetInteger: integer_vars[%d]=%d", vr[i], value[i]);
    if (vr[i] < FMI_INTEGER_VARS)
      integer_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::SetBoolean(const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[])
{
  fmi_verbose_log("fmi2SetBoolean(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_BOOLEAN_VARS)
      boolean_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

fmi2Status COSMPTrafficAgent::SetString(const fmi2ValueReference vr[], size_t nvr, const fmi2String value[])
{
  fmi_verbose_log("fmi2SetString(...)");
  for (size_t i = 0; i < nvr; i++)
  {
    if (vr[i] < FMI_STRING_VARS)
      string_vars[vr[i]] = value[i];
    else
      return fmi2Error;
  }
  return fmi2OK;
}

/*
 * FMI 2.0 Co-Simulation Interface API
 */

extern "C"
{
  FMI2_Export const char* fmi2GetTypesPlatform()
  {
    return fmi2TypesPlatform;
  }

  FMI2_Export const char* fmi2GetVersion()
  {
    return fmi2Version;
  }

  FMI2_Export fmi2Status fmi2SetDebugLogging(fmi2Component c,
                                             fmi2Boolean loggingOn,
                                             size_t nCategories,
                                             const fmi2String categories[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->SetDebugLogging(loggingOn, nCategories, categories);
  }

  /*
   * Functions for Co-Simulation
   */
  FMI2_Export fmi2Component fmi2Instantiate(fmi2String instanceName,
                                            fmi2Type fmuType,
                                            fmi2String fmuGUID,
                                            fmi2String fmuResourceLocation,
                                            const fmi2CallbackFunctions* functions,
                                            fmi2Boolean visible,
                                            fmi2Boolean loggingOn)
  {
    return COSMPTrafficAgent::Instantiate(
        instanceName, fmuType, fmuGUID, fmuResourceLocation, functions, visible, loggingOn);
  }

  FMI2_Export fmi2Status fmi2SetupExperiment(fmi2Component c,
                                             fmi2Boolean toleranceDefined,
                                             fmi2Real tolerance,
                                             fmi2Real startTime,
                                             fmi2Boolean stopTimeDefined,
                                             fmi2Real stopTime)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->SetupExperiment(toleranceDefined, tolerance, startTime, stopTimeDefined, stopTime);
  }

  FMI2_Export fmi2Status fmi2EnterInitializationMode(fmi2Component c)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->EnterInitializationMode();
  }

  FMI2_Export fmi2Status fmi2ExitInitializationMode(fmi2Component c)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->ExitInitializationMode();
  }

  FMI2_Export fmi2Status fmi2DoStep(fmi2Component c,
                                    fmi2Real currentCommunicationPoint,
                                    fmi2Real communicationStepSize,
                                    fmi2Boolean noSetFMUStatePriorToCurrentPointfmi2Component)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->DoStep(currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPointfmi2Component);
  }

  FMI2_Export fmi2Status fmi2Terminate(fmi2Component c)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->Terminate();
  }

  FMI2_Export fmi2Status fmi2Reset(fmi2Component c)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->Reset();
  }

  FMI2_Export void fmi2FreeInstance(fmi2Component c)
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    myc->FreeInstance();
    delete myc;
  }

  /*
   * Data Exchange Functions
   */
  FMI2_Export fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->GetReal(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->GetInteger(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->GetBoolean(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2GetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->GetString(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->SetReal(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetInteger(fmi2Component c,
                                        const fmi2ValueReference vr[],
                                        size_t nvr,
                                        const fmi2Integer value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->SetInteger(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetBoolean(fmi2Component c,
                                        const fmi2ValueReference vr[],
                                        size_t nvr,
                                        const fmi2Boolean value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->SetBoolean(vr, nvr, value);
  }

  FMI2_Export fmi2Status fmi2SetString(fmi2Component c,
                                       const fmi2ValueReference vr[],
                                       size_t nvr,
                                       const fmi2String value[])
  {
    COSMPTrafficAgent* myc = (COSMPTrafficAgent*)c;
    return myc->SetString(vr, nvr, value);
  }

  /*
   * Unsupported Features (FMUState, Derivatives, Async DoStep, Status Enquiries)
   */
  FMI2_Export fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
  {
    (void)c;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
  {
    (void)c;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
  {
    (void)c;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t* size)
  {
    (void)c;
    (void)FMUstate;
    (void)size;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SerializeFMUstate(fmi2Component c,
                                               fmi2FMUstate FMUstate,
                                               fmi2Byte serializedState[],
                                               size_t size)
  {
    (void)c;
    (void)FMUstate;
    (void)serializedState;
    (void)size;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2DeSerializeFMUstate(fmi2Component c,
                                                 const fmi2Byte serializedState[],
                                                 size_t size,
                                                 fmi2FMUstate* FMUstate)
  {
    (void)c;
    (void)serializedState;
    (void)size;
    (void)FMUstate;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2GetDirectionalDerivative(fmi2Component c,
                                                      const fmi2ValueReference vUnknown_ref[],
                                                      size_t nUnknown,
                                                      const fmi2ValueReference vKnown_ref[],
                                                      size_t nKnown,
                                                      const fmi2Real dvKnown[],
                                                      fmi2Real dvUnknown[])
  {
    (void)c;
    (void)vUnknown_ref;
    (void)nUnknown;
    (void)vKnown_ref;
    (void)nKnown;
    (void)dvKnown;
    (void)dvUnknown;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2SetRealInputDerivatives(
      fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer order[], const fmi2Real value[])
  {
    (void)c;
    (void)vr;
    (void)nvr;
    (void)order;
    (void)value;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2GetRealOutputDerivatives(
      fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer order[], fmi2Real value[])
  {
    (void)c;
    (void)vr;
    (void)nvr;
    (void)order;
    (void)value;
    return fmi2Error;
  }

  FMI2_Export fmi2Status fmi2CancelStep(fmi2Component c)
  {
    (void)c;
    return fmi2OK;
  }

  FMI2_Export fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }

  FMI2_Export fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String* value)
  {
    (void)c;
    (void)s;
    (void)value;
    return fmi2Discard;
  }
}
