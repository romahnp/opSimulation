/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "FmuCalculations.h"
#include "FmuHandler.h"
#include "SignalInterface/SignalTranslator.h"

using namespace units::literals;

using namespace units::literals;

using ::testing::Eq;

TEST(OsmpFmuUnitTests, GetTrafficCommandFromOpenScenarioTrajectory_FollowTrajectoryAction)
{
  mantle_api::Pose fakePose1{{0.1_m, -0.2_m, 0_m}, {0.3_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{-1.1_m, 1.2_m, 0_m}, {1.3_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{2.1_m, -2.2_m, 0_m}, {-2.3_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1, 0_s};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2, 5.1_s};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3, 15.2_s};

  mantle_api::PolyLine polyLine{fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3};

  mantle_api::Trajectory trajectory;
  trajectory.type = polyLine;

  osi3::TrafficCommand trafficCommand;
  FmuHelper::AddTrafficCommandActionFromOpenScenarioTrajectory(trafficCommand.add_action(), trajectory);

  const auto& trajectoryAction = trafficCommand.action(0).follow_trajectory_action();
  ASSERT_THAT(trajectoryAction.trajectory_point_size(), Eq(3));

  const auto& firstPoint = trajectoryAction.trajectory_point(0);
  ASSERT_THAT(firstPoint.timestamp().seconds(), Eq(0));
  ASSERT_THAT(firstPoint.timestamp().nanos(), Eq(0));
  ASSERT_THAT(firstPoint.position().x(), Eq(0.1));
  ASSERT_THAT(firstPoint.position().y(), Eq(-0.2));
  ASSERT_THAT(firstPoint.orientation().yaw(), Eq(0.3));

  const auto& secondPoint = trajectoryAction.trajectory_point(1);
  ASSERT_THAT(secondPoint.timestamp().seconds(), Eq(5));
  ASSERT_THAT(secondPoint.timestamp().nanos(), Eq(100000000));
  ASSERT_THAT(secondPoint.position().x(), Eq(-1.1));
  ASSERT_THAT(secondPoint.position().y(), Eq(1.2));
  ASSERT_THAT(secondPoint.orientation().yaw(), Eq(1.3));

  const auto& thirdPoint = trajectoryAction.trajectory_point(2);
  ASSERT_THAT(thirdPoint.timestamp().seconds(), Eq(15));
  ASSERT_THAT(thirdPoint.timestamp().nanos(), Eq(200000000));
  ASSERT_THAT(thirdPoint.position().x(), Eq(2.1));
  ASSERT_THAT(thirdPoint.position().y(), Eq(-2.2));
  ASSERT_THAT(thirdPoint.orientation().yaw(), Eq(-2.3));
}

TEST(OsmpFmuUnitTests, GetTrafficCommandFromOpenScenarioTrajectory_FollowPathAction)
{
  mantle_api::Pose fakePose1{{0.1_m, -0.2_m, 0_m}, {0.3_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose2{{-1.1_m, 1.2_m, 0_m}, {1.3_rad, 0_rad, 0_rad}};
  mantle_api::Pose fakePose3{{2.1_m, -2.2_m, 0_m}, {-2.3_rad, 0_rad, 0_rad}};

  mantle_api::PolyLinePoint fakePolyLinePoint1{fakePose1};
  mantle_api::PolyLinePoint fakePolyLinePoint2{fakePose2};
  mantle_api::PolyLinePoint fakePolyLinePoint3{fakePose3};

  mantle_api::PolyLine polyLine{fakePolyLinePoint1, fakePolyLinePoint2, fakePolyLinePoint3};

  mantle_api::Trajectory trajectory;
  trajectory.type = polyLine;

  osi3::TrafficCommand trafficCommand;
  FmuHelper::AddTrafficCommandActionFromOpenScenarioTrajectory(trafficCommand.add_action(), trajectory);

  const auto& pathAction = trafficCommand.action(0).follow_path_action();
  ASSERT_THAT(pathAction.path_point_size(), Eq(3));

  const auto& firstPoint = pathAction.path_point(0);
  ASSERT_THAT(firstPoint.position().x(), Eq(0.1));
  ASSERT_THAT(firstPoint.position().y(), Eq(-0.2));
  ASSERT_THAT(firstPoint.orientation().yaw(), Eq(0.3));

  const auto& secondPoint = pathAction.path_point(1);
  ASSERT_THAT(secondPoint.position().x(), Eq(-1.1));
  ASSERT_THAT(secondPoint.position().y(), Eq(1.2));
  ASSERT_THAT(secondPoint.orientation().yaw(), Eq(1.3));

  const auto& thirdPoint = pathAction.path_point(2);
  ASSERT_THAT(thirdPoint.position().x(), Eq(2.1));
  ASSERT_THAT(thirdPoint.position().y(), Eq(-2.2));
  ASSERT_THAT(thirdPoint.orientation().yaw(), Eq(-2.3));
}

TEST(OsmpFmuUnitTests, GetTrafficCommandFromOpenScenarioPosition)
{
  constexpr units::length::meter_t x = 3.14_m, y = 42.0_m;
  mantle_api::Position position = mantle_api::Vec3<units::length::meter_t>{x, y, 0_m};

  osi3::TrafficCommand trafficCommand;
  FmuHelper::AddTrafficCommandActionFromOpenScenarioPosition(trafficCommand.add_action(), position, nullptr, nullptr);

  const auto& action = trafficCommand.action(0);
  auto hasAcquireGlobalPositionAction = action.has_acquire_global_position_action();
  ASSERT_TRUE(hasAcquireGlobalPositionAction);
  const auto& positionAction = action.acquire_global_position_action();
  ASSERT_TRUE(positionAction.has_position());
  ASSERT_EQ(positionAction.position().x(), x.value());
  ASSERT_EQ(positionAction.position().y(), y.value());
  ASSERT_FALSE(positionAction.position().has_z());
  ASSERT_FALSE(positionAction.has_orientation());
  ASSERT_FALSE(positionAction.orientation().has_roll());
  ASSERT_FALSE(positionAction.orientation().has_pitch());
  ASSERT_FALSE(positionAction.orientation().has_yaw());
  ASSERT_FALSE(positionAction.has_action_header());
}