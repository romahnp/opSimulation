/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <tuple>
#include <typeindex>
#include <unordered_map>

#include "ChannelDefinitionParser.h"
#include "fakeCallback.h"

using ::testing::HasSubstr;
using ::testing::ThrowsMessage;
using ::testing::UnorderedElementsAre;

const FmuVariables testFmu1Variables = FmuVariables{
    std::in_place_index_t<0>(),
    Fmu1Variables{
        {"BoolTestChannel",
         FmuVariable1{0, VariableType::Bool, fmi1_causality_enu_input, fmi1_variability_enu_discrete}},
        {"IntTestChannel", FmuVariable1{1, VariableType::Int, fmi1_causality_enu_input, fmi1_variability_enu_discrete}},
        {"Real1TestChannel",
         FmuVariable1{2, VariableType::Double, fmi1_causality_enu_input, fmi1_variability_enu_discrete}},
        {"Real2TestChannel",
         FmuVariable1{3, VariableType::Double, fmi1_causality_enu_input, fmi1_variability_enu_discrete}},
        {"Real3TestChannel",
         FmuVariable1{4, VariableType::Double, fmi1_causality_enu_input, fmi1_variability_enu_discrete}},
        {"StringTestChannel",
         FmuVariable1{5, VariableType::String, fmi1_causality_enu_input, fmi1_variability_enu_discrete}},
        {"UnusedBoolTestChannel",
         FmuVariable1{6, VariableType::Bool, fmi1_causality_enu_input, fmi1_variability_enu_discrete}}},
};

bool operator==(const Fmu1Input& lhs, const Fmu1Input& rhs)
{
  return lhs.type == rhs.type && lhs.valueReference == rhs.valueReference
      && lhs.additionalParameter == rhs.additionalParameter;
};

template <typename T>
bool operator==(const Fmu1Parameter<T>& lhs, const Fmu1Parameter<T>& rhs)
{
  return lhs.value == rhs.value && lhs.valueReference == rhs.valueReference;
};

class FmuWrapper_ChannelDefinitionParser : public ::testing::Test
{
public:
  FmuWrapper_ChannelDefinitionParser() {}

protected:
  FakeCallback callback;
  ChannelDefinitionParser<FMI1> channelDefinitionParser{
      testFmu1Variables, fmi_version_enu_t::fmi_version_1_enu, &callback};
};

TEST_F(FmuWrapper_ChannelDefinitionParser, AddEmptyDefinition_ThrowError)
{
  EXPECT_THAT([&]() { channelDefinitionParser.AddOutputChannel("DynamicsSignal_Velocity", "", {}); },
              ThrowsMessage<std::runtime_error>(HasSubstr("Unable to add output channel: Variable")));
  EXPECT_THAT([&]() { channelDefinitionParser.AddInputChannel("VelocityEgo", " "); },
              ThrowsMessage<std::runtime_error>(HasSubstr("Unable to add input channel: Variable")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddInvalidOutputChannel_ThrowError)
{
  EXPECT_THAT(
      [&]() { channelDefinitionParser.AddOutputChannel("DynamicsSignal_Velocity", "NonExistantTestChannel", {}); },
      ThrowsMessage<std::runtime_error>(HasSubstr("Unable to add output channel: Variable <NonExistantTestChannel>")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddValidOutputChannelName)
{
  EXPECT_NO_THROW(channelDefinitionParser.AddOutputChannel("DynamicsSignal_Velocity", "Real1TestChannel", {}));
}
TEST_F(FmuWrapper_ChannelDefinitionParser, AddOutputChannelWithWrongType_ThrowError)
{
  EXPECT_THAT([&]() { channelDefinitionParser.AddOutputChannel("DynamicsSignal_Velocity", "BoolTestChannel", {}); },
              ThrowsMessage<std::runtime_error>(
                  HasSubstr("Unable to add output channel: Variable type in FMU not the same as in Wrapper")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddValidOutputChannelTwice_ThrowError)
{
  EXPECT_NO_THROW(channelDefinitionParser.AddOutputChannel("DynamicsSignal_Velocity", "Real1TestChannel", {}));
  EXPECT_THAT(
      [&]() { channelDefinitionParser.AddOutputChannel("DynamicsSignal_Acceleration", "Real1TestChannel", {}); },
      ThrowsMessage<std::runtime_error>(HasSubstr("Unable to add output channel: Variable <Real1TestChannel>")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddInvalidInputChannel_ThrowError)
{
  EXPECT_THAT(
      [&]() { channelDefinitionParser.AddInputChannel("VelocityEgo", "NonExistantTestChannel"); },
      ThrowsMessage<std::runtime_error>(HasSubstr("Unable to add input channel: Variable <NonExistantTestChannel>")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddValidInputChannelName)
{
  EXPECT_NO_THROW(channelDefinitionParser.AddInputChannel("VelocityEgo", "Real1TestChannel"));
  EXPECT_NO_THROW(channelDefinitionParser.AddInputChannel("SpeedLimit_100.0", "Real2TestChannel"));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddInputChannelWithWrongType_ThrowError)
{
  EXPECT_THAT([&]() { channelDefinitionParser.AddInputChannel("VelocityEgo", "BoolTestChannel"); },
              ThrowsMessage<std::runtime_error>(
                  HasSubstr("Unable to add input channel: Variable type in FMU not the same as in Wrapper")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, AddValidInputChannelTwice_ThrowError)
{
  EXPECT_NO_THROW(channelDefinitionParser.AddInputChannel("VelocityEgo", "Real1TestChannel"));
  EXPECT_THAT([&]() { channelDefinitionParser.AddInputChannel("AccelerationEgo", "Real1TestChannel"); },
              ThrowsMessage<std::runtime_error>(HasSubstr("Unable to add input channel: Variable <Real1TestChannel>")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, ValidOutputConfiguration_GeneratesValidMap)
{
  channelDefinitionParser.AddOutputChannel("DynamicsSignal_Velocity", "Real1TestChannel", {});
  channelDefinitionParser.AddOutputChannel("DynamicsSignal_Acceleration", "Real2TestChannel", {});
  channelDefinitionParser.AddOutputChannel("DynamicsSignal_PositionX", "Real3TestChannel", {});

  auto outputs = std::get<FMI1>(channelDefinitionParser.GetFmuOutputs());

  ASSERT_THAT(outputs,
              UnorderedElementsAre(
                  std::make_pair<SignalValue, fmi1_value_reference_t>(SignalValue::DynamicsSignal_Velocity, 2),
                  std::make_pair<SignalValue, fmi1_value_reference_t>(SignalValue::DynamicsSignal_Acceleration, 3),
                  std::make_pair<SignalValue, fmi1_value_reference_t>(SignalValue::DynamicsSignal_PositionX, 4)));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, ValidInputConfiguration_GeneratesValidMap)
{
  channelDefinitionParser.AddInputChannel("VelocityEgo", "Real1TestChannel");
  channelDefinitionParser.AddInputChannel("SpeedLimit_1.0", "Real2TestChannel");
  channelDefinitionParser.AddInputChannel("SpeedLimit_10.5", "Real3TestChannel");
  channelDefinitionParser.AddInputChannel("SensorFusionLane_0", "IntTestChannel");
  channelDefinitionParser.AddInputChannel("ExistenceFront", "BoolTestChannel");

  auto realInputs = std::get<FMI1>(channelDefinitionParser.GetFmuRealInputs());
  auto booleanInputs = std::get<FMI1>(channelDefinitionParser.GetFmuBooleanInputs());
  auto intInputs = std::get<FMI1>(channelDefinitionParser.GetFmuIntegerInputs());

  ASSERT_THAT(realInputs,
              UnorderedElementsAre(Fmu1Input(FmuInputType::VelocityEgo, {0.}, 2),
                                   Fmu1Input(FmuInputType::SpeedLimit, {1.0}, 3),
                                   Fmu1Input(FmuInputType::SpeedLimit, {10.5}, 4)));

  ASSERT_THAT(intInputs, UnorderedElementsAre(Fmu1Input(FmuInputType::SensorFusionLane, {size_t{0}}, 1)));

  ASSERT_THAT(booleanInputs, UnorderedElementsAre(Fmu1Input(FmuInputType::ExistenceFront, {0.}, 0)));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, ParseOutputSignalTypesIncompleteSignal_ThrowError)
{
  channelDefinitionParser.AddOutputChannel("LongitudinalSignal_AccPedalPos", "Real1TestChannel", {});
  channelDefinitionParser.AddOutputChannel("LongitudinalSignal_Gear", "IntTestChannel", {});

  EXPECT_THAT([&]() { channelDefinitionParser.ParseOutputSignalTypes(); },
              ThrowsMessage<std::runtime_error>(HasSubstr("Output signal for FMU incomplete")));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, ParseOutputSignalTypesCompleteSignal_ParseRightSignal)
{
  channelDefinitionParser.AddOutputChannel("LongitudinalSignal_AccPedalPos", "Real1TestChannel", {});
  channelDefinitionParser.AddOutputChannel("LongitudinalSignal_BrakePedalPos", "Real2TestChannel", {});
  channelDefinitionParser.AddOutputChannel("LongitudinalSignal_Gear", "IntTestChannel", {});

  EXPECT_NO_THROW(channelDefinitionParser.ParseOutputSignalTypes());
  auto signalType = channelDefinitionParser.GetOutputSignals();
  ASSERT_THAT(signalType, UnorderedElementsAre(SignalType::LongitudinalSignal));
}

TEST_F(FmuWrapper_ChannelDefinitionParser, ParseParameters)
{
  using namespace std::string_literals;

  channelDefinitionParser.AddParameter("Test"s, "StringTestChannel");
  channelDefinitionParser.AddParameter(12, "IntTestChannel");
  channelDefinitionParser.AddParameter(1.23, "Real1TestChannel");
  channelDefinitionParser.AddParameter(12.3, "Real2TestChannel");
  channelDefinitionParser.AddParameter(true, "BoolTestChannel");

  auto stringParameter = std::get<FMI1>(channelDefinitionParser.GetFmuStringParameters());
  auto intParameter = std::get<FMI1>(channelDefinitionParser.GetFmuIntegerParameters());
  auto doubleParameter = std::get<FMI1>(channelDefinitionParser.GetFmuDoubleParameters());
  auto boolParameter = std::get<FMI1>(channelDefinitionParser.GetFmuBoolParameters());

  ASSERT_THAT(stringParameter, UnorderedElementsAre(Fmu1Parameter<std::string>("Test", 5)));
  ASSERT_THAT(intParameter, UnorderedElementsAre(Fmu1Parameter<int>(12, 1)));
  ASSERT_THAT(doubleParameter, UnorderedElementsAre(Fmu1Parameter<double>(1.23, 2), Fmu1Parameter<double>(12.3, 3)));
  ASSERT_THAT(boolParameter, UnorderedElementsAre(Fmu1Parameter<bool>(true, 0)));
}
