/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <filesystem>

#include "SimpleAffineFMU.h"
#include "common/sensorDataSignal.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeWorld.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connection.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/GroupConnector.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/OSMPConnector.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/TriggerSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/TriggerVisit.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspTriggerVisitor.h"
#include "tests/fakes/gmock/fakeFmuWrapper.h"

using ::testing::_;
using ::testing::Mock;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

using namespace ssp;

struct SSPAlgorithmTests_Data
{
  SSPAlgorithmTests_Data() { SspLogger::SetLogger(&fakeCallback, 0, ""); }

  NiceMock<FakeWorld> fakeWorld;
  NiceMock<FakeAgent> fakeAgent;
  NiceMock<FakeParameter> fakeParameter;
  NiceMock<FakeCallback> fakeCallback;

  const double rate = 2.0;
  const double offset = 3.0;
  const double factor5 = 5.0;
  const double factor7 = 7.0;
  const double factor2 = 2.0;
  const double factor1 = 1.0;

  std::shared_ptr<SimpleAffineFMU> fmu5 = std::make_shared<SimpleAffineFMU>(factor5, 40);  // rate * 5.0 + offset
  std::shared_ptr<SimpleAffineFMU> fmu7 = std::make_shared<SimpleAffineFMU>(factor7, 30);  // rate * 7.0 + offset
  std::shared_ptr<SimpleAffineFMU> fmu2 = std::make_shared<SimpleAffineFMU>(factor2, 20);  // rate * 2.0 + offset
  std::shared_ptr<SimpleAffineFMU> fmu1 = std::make_shared<SimpleAffineFMU>(factor1, 10);  // rate * 1.0 + offset

  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu5_a
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_rate", fmu5->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu5_b
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_offset", fmu5->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu5_x
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_result", fmu5->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu5_y
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_result2", fmu5->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu7_a
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_rate", fmu7->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu7_b
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_offset", fmu7->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu7_x
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_result", fmu7->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu7_y
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_result2", fmu7->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu2_a
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu2, "fmu2_rate", fmu2->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu2_b
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu2, "fmu2_offset", fmu2->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu2_x
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu2, "fmu2_result", fmu2->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu2_y
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu2, "fmu2_result2", fmu2->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu1_a
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu1, "fmu1_rate", fmu1->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu1_b
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu1, "fmu1_offset", fmu1->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu1_x
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu1, "fmu1_result", fmu1->priority);
  std::shared_ptr<ScalarConnector<ssp::VariableTypeDouble>> pFmu1_y
      = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu1, "fmu1_result2", fmu1->priority);

  void reverse57()
  {
    pFmu7_a = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_rate", fmu7->priority);
    pFmu7_b = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_offset", fmu7->priority);
    pFmu7_x = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_result", fmu7->priority);
    pFmu7_y = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu7, "fmu7_result2", fmu7->priority);

    pFmu5_a = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_rate", fmu5->priority);
    pFmu5_b = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_offset", fmu5->priority);
    pFmu5_x = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_result", fmu5->priority);
    pFmu5_y = std::make_shared<ScalarConnector<ssp::VariableTypeDouble>>(fmu5, "fmu5_result2", fmu5->priority);
  }

  void Init()
  {
    fmu1->Init();
    fmu2->Init();
    fmu5->Init();
    fmu7->Init();
  }
};

TEST(CompileTimeTest, Connectors)
{
  static_assert(!std::is_default_constructible_v<ScalarConnector<VariableTypeDouble>>);
  SUCCEED();
}

TEST(CompileTimeTest, Visitors)
{
  static_assert(!std::is_default_constructible_v<UpdateInputVisitor<VariableTypeDouble>>);
  static_assert(std::is_default_constructible_v<UpdateOutputVisitor<VariableTypeDouble>>);
  SUCCEED();
}

TEST(SSPPrototypeTestSuite, direct_fmu_triggers)
{
  SSPAlgorithmTests_Data data{};
  data.Init();

  data.pFmu5_x->connectors.emplace_back(data.pFmu7_a);

  GroupConnector rateSystemConnector{{data.pFmu5_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b}};
  GroupConnector resultGroupConnector{{data.pFmu7_x}};

  const auto double_a = std::make_shared<DoubleSignal const>(data.rate, ComponentState::Acting);
  const auto double_b = std::make_shared<DoubleSignal const>(data.offset, ComponentState::Acting);
  UpdateInputSignalVisitor visitorFmu5_A{0, double_a, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  UpdateInputSignalVisitor visitorFmu5_B{1, double_b, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  rateSystemConnector.Accept(visitorFmu5_A);
  offsetSystemConnector.Accept(visitorFmu5_B);

  TriggerSignalVisitor triggerVisit{0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  data.pFmu5_x->Accept(triggerVisit);
  data.pFmu7_x->Accept(triggerVisit);

  std::shared_ptr<const SignalInterface> outputSignal;
  UpdateOutputSignalVisitor outVisitor{0, outputSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  resultGroupConnector.Accept(outVisitor);
  auto result = std::dynamic_pointer_cast<DoubleSignal const>(outputSignal)->value;

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = result_fmu5 * data.factor7 + data.offset;  // 94

  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);
  ASSERT_EQ(result, result_fmu7);
}

TEST(SSPPrototypeTestSuite, ssdConnector_triggers)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu5_x->connectors.emplace_back(data.pFmu7_a);  //If the Connectors are not joined correctly the test crashes
  data.pFmu7_x->connectors.emplace_back(data.pFmu2_a);

  GroupConnector rateSystemConnector{{data.pFmu5_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b, data.pFmu2_b}};
  GroupConnector triggerConnector{{data.pFmu5_x, data.pFmu7_x, data.pFmu2_x}};  //Careful trigger must be called on x
  GroupConnector resultSystemConnector{{data.pFmu2_x}};

  const auto double_a = std::make_shared<DoubleSignal const>(data.rate, ComponentState::Acting);
  const auto double_b = std::make_shared<DoubleSignal const>(data.offset, ComponentState::Acting);
  UpdateInputSignalVisitor visitor_A{0, double_a, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  UpdateInputSignalVisitor visitor_B{0, double_b, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerSignalVisitor triggerVisit{0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  triggerConnector.Accept(triggerVisit);

  std::shared_ptr<const SignalInterface> outputSignal;
  UpdateOutputSignalVisitor outVisitor{0, outputSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  resultSystemConnector.Accept(outVisitor);
  auto result = std::dynamic_pointer_cast<DoubleSignal const>(outputSignal)->value;

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = result_fmu5 * data.factor7 + data.offset;  // 94
  auto result_fmu2 = result_fmu7 * data.factor2 + data.offset;  // 191

  //rate = 2 , offset = 3
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);
  ASSERT_EQ(result, result_fmu2);
}

TEST(SSPPrototypeTestSuite, ssdConnector_triggers_noSignals)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu5_x->connectors.emplace_back(data.pFmu7_a);  //If the Connectors are not joined correctly the test crashes
  data.pFmu7_x->connectors.emplace_back(data.pFmu2_a);

  GroupConnector rateSystemConnector{{data.pFmu5_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b, data.pFmu2_b}};
  GroupConnector triggerConnector{{data.pFmu5_x, data.pFmu7_x, data.pFmu2_x}};  //Careful trigger must be called on x
  GroupConnector resultGroupConnector{{data.pFmu2_x}};

  UpdateInputVisitor<VariableTypeDouble> visitor_A{data.rate};
  UpdateInputVisitor<VariableTypeDouble> visitor_B{data.offset};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerVisit<VariableTypeDouble> triggerVisit{0};
  triggerConnector.Accept(triggerVisit);

  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};
  resultGroupConnector.Accept(outVisitor);
  auto result = data.pFmu2_x->GetValue();

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = result_fmu5 * data.factor7 + data.offset;  // 94
  auto result_fmu2 = result_fmu7 * data.factor2 + data.offset;  // 191

  //rate = 2 , offset = 3
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);
  ASSERT_EQ(result, result_fmu2);
}

TEST(SSPPrototypeTestSuite, ssdConnector_triggers_asTriangle)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu5_x->connectors.emplace_back(data.pFmu2_a);  //If the Connectors are not joined correctly the test crashes
  data.pFmu7_x->connectors.emplace_back(data.pFmu2_b);

  GroupConnector rateSystemConnector{{data.pFmu5_a, data.pFmu7_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b}};
  GroupConnector triggerConnector{{data.pFmu5_x, data.pFmu7_x, data.pFmu2_x}};  //Careful trigger must be called on x
  GroupConnector resultGroupConnector{{data.pFmu2_x}};

  const auto double_a = std::make_shared<DoubleSignal const>(data.rate, ComponentState::Acting);
  const auto double_b = std::make_shared<DoubleSignal const>(data.offset, ComponentState::Acting);
  UpdateInputSignalVisitor visitor_A{0, double_a, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  UpdateInputSignalVisitor visitor_B{1, double_b, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerSignalVisitor triggerVisit{0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  triggerConnector.Accept(triggerVisit);

  std::shared_ptr<const SignalInterface> outputSignal;
  UpdateOutputSignalVisitor outVisitor{0, outputSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  resultGroupConnector.Accept(outVisitor);
  auto result = std::dynamic_pointer_cast<DoubleSignal const>(outputSignal)->value;

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = data.rate * data.factor7 + data.offset;    // 17
  auto result_fmu2 = result_fmu5 * data.factor2 + result_fmu7;  // 43

  //rate = 2 , offset = 3
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);
  ASSERT_EQ(result, result_fmu2);
}

TEST(SSPPrototypeTestSuite, ssdConnector_triggers_with_loop)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu2_x->connectors.emplace_back(data.pFmu5_a);
  data.pFmu5_x->connectors.emplace_back(data.pFmu2_a);

  data.pFmu2_x->SetPriority(20);
  data.pFmu2_y->SetPriority(20);
  data.pFmu5_x->SetPriority(20);
  data.pFmu5_y->SetPriority(10);

  GroupConnector rateSystemConnector{{data.pFmu2_a}};
  GroupConnector offsetSystemConnector{{data.pFmu2_b, data.pFmu5_b}};
  GroupConnector triggerConnector{{data.pFmu2_x, data.pFmu5_x, data.pFmu2_y, data.pFmu5_y}};
  GroupConnector resultGroupConnector{{data.pFmu5_x}};

  UpdateInputVisitor<VariableTypeDouble> visitor_A{data.rate};
  UpdateInputVisitor<VariableTypeDouble> visitor_B{data.offset};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerVisit<VariableTypeDouble> triggerVisit{0};
  triggerConnector.Accept(triggerVisit);

  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};
  resultGroupConnector.Accept(outVisitor);
  auto result = data.pFmu5_x->GetValue();

  //rate = 2 , offset = 3
  auto result_fmu2 = data.rate * data.factor2 + data.offset;    // 7
  auto result_fmu5 = result_fmu2 * data.factor5 + data.offset;  // 38

  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, result_fmu2);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(result, result_fmu5);
}

TEST(SSPPrototypeTestSuite, ssdConnector_triggers_two_Outputs)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu2_x->connectors.emplace_back(data.pFmu5_a);
  data.pFmu2_y->connectors.emplace_back(data.pFmu5_b);

  data.pFmu2_x->SetPriority(20);
  data.pFmu2_y->SetPriority(20);
  data.pFmu5_x->SetPriority(10);
  data.pFmu5_y->SetPriority(10);

  GroupConnector offsetSystemConnector{{data.pFmu2_b}};
  GroupConnector rateSystemConnector{{data.pFmu2_a}};
  GroupConnector triggerConnector{{data.pFmu2_x, data.pFmu2_y, data.pFmu5_x, data.pFmu5_y}};
  GroupConnector resultGroupConnector{{data.pFmu5_x}};

  UpdateInputVisitor<VariableTypeDouble> visitor_A{data.rate};
  UpdateInputVisitor<VariableTypeDouble> visitor_B{data.offset};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerVisit<VariableTypeDouble> triggerVisit{0};
  triggerConnector.Accept(triggerVisit);

  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};
  resultGroupConnector.Accept(outVisitor);
  auto result = data.pFmu5_x->GetValue();

  //rate = 2 , offset = 3
  auto result_fmu2 = data.rate * data.factor2 + data.offset;  //7
  auto result2_fmu2 = result_fmu2 * 2;
  auto result_fmu5 = result_fmu2 * data.factor5 + result2_fmu2;  // 38
  auto result2_fmu5 = result_fmu5 * 2;

  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);
  ASSERT_EQ(data.fmu2->fmiValues[3].realValue, result2_fmu2);
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, result_fmu2);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, result2_fmu2);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu5->fmiValues[3].realValue, result2_fmu5);
  ASSERT_EQ(result, result_fmu5);

  ASSERT_EQ(data.fmu2->triggerCounter, 1);
  ASSERT_EQ(data.fmu5->triggerCounter, 1);
}

TEST(SSPPrototypeTestSuite, ssdConnector_triggers_testOnlyOneTriggerCall)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu1_x->connectors.emplace_back(data.pFmu7_a);
  data.pFmu1_y->connectors.emplace_back(data.pFmu7_b);

  GroupConnector offsetSystemConnector{{data.pFmu1_b}};
  GroupConnector rateSystemConnector{{data.pFmu1_a}};
  GroupConnector triggerConnector{{data.pFmu1_x, data.pFmu1_y, data.pFmu7_x}};
  GroupConnector resultGroupConnector{{data.pFmu2_x}};

  UpdateInputVisitor<VariableTypeDouble> visitor_A{data.rate};
  UpdateInputVisitor<VariableTypeDouble> visitor_B{data.offset};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerVisit<VariableTypeDouble> triggerVisit{0};
  triggerConnector.Accept(triggerVisit);

  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};
  resultGroupConnector.Accept(outVisitor);

  ASSERT_EQ(data.fmu1->triggerCounter, 1);
  ASSERT_EQ(data.fmu7->triggerCounter, 1);

  triggerConnector.Accept(triggerVisit);

  ASSERT_EQ(data.fmu1->triggerCounter, 1);
  ASSERT_EQ(data.fmu7->triggerCounter, 1);
}

TEST(SSPPrototypeTestSuite, ssdConnector_poorPrioritization_triggers)
{
  SSPAlgorithmTests_Data data;
  data.Init();

  data.pFmu5_x->connectors.emplace_back(data.pFmu7_a);
  data.pFmu7_x->connectors.emplace_back(data.pFmu2_a);
  data.pFmu2_x->connectors.emplace_back(data.pFmu1_a);

  data.pFmu1_a->SetPriority(35);
  data.pFmu1_b->SetPriority(35);
  data.pFmu1_x->SetPriority(35);
  data.pFmu1_y->SetPriority(35);

  GroupConnector rateSystemConnector{{data.pFmu5_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b, data.pFmu2_b, data.pFmu1_b}};
  GroupConnector triggerConnector{
      {data.pFmu5_x, data.pFmu7_x, data.pFmu2_x, data.pFmu1_x}};  //Careful trigger must be called on x (?)
  GroupConnector resultGroupConnector{{data.pFmu1_x}};

  const auto double_a = std::make_shared<DoubleSignal const>(data.rate, ComponentState::Acting);
  const auto double_b = std::make_shared<DoubleSignal const>(data.offset, ComponentState::Acting);
  UpdateInputSignalVisitor visitor_A{0, double_a, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  UpdateInputSignalVisitor visitor_B{1, double_b, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerSignalVisitor triggerVisit{0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  triggerConnector.Accept(triggerVisit);

  std::shared_ptr<const SignalInterface> outputSignal;
  UpdateOutputSignalVisitor outVisitor{0, outputSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  resultGroupConnector.Accept(outVisitor);
  auto result = std::dynamic_pointer_cast<DoubleSignal const>(outputSignal)->value;

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = result_fmu5 * data.factor7 + data.offset;  // 94
  auto result_fmu2 = result_fmu7 * data.factor2 + data.offset;  // 191
  auto result_fmu1 = 0 * data.factor1 + data.offset;            // 3

  //rate = 2 , offset = 3
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);

  ASSERT_EQ(data.fmu1->fmiValues[0].realValue, result_fmu2);
  ASSERT_EQ(data.fmu1->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu1->fmiValues[2].realValue, result_fmu1);

  ASSERT_EQ(result, result_fmu1);
}

TEST(SSPPrototypeTestSuite, ssdConnector_poorPrioritization_triggers_noSignal)
{
  SSPAlgorithmTests_Data data{};
  data.Init();

  data.pFmu1_a->SetPriority(35);
  data.pFmu1_b->SetPriority(35);
  data.pFmu1_x->SetPriority(35);
  data.pFmu1_y->SetPriority(35);

  data.pFmu5_x->connectors.emplace_back(data.pFmu7_a);  //If the Connectors are not joined correctly the test crashes
  data.pFmu7_x->connectors.emplace_back(data.pFmu2_a);
  data.pFmu2_x->connectors.emplace_back(data.pFmu1_a);

  GroupConnector rateSystemConnector{{data.pFmu5_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b, data.pFmu2_b, data.pFmu1_b}};
  GroupConnector triggerConnector{
      {data.pFmu5_x, data.pFmu7_x, data.pFmu2_x, data.pFmu1_x}};  //Careful trigger must be called on x
  GroupConnector resultGroupConnector{{data.pFmu1_x}};

  UpdateInputVisitor<VariableTypeDouble> visitor_A{data.rate};
  UpdateInputVisitor<VariableTypeDouble> visitor_B{data.offset};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerVisit<VariableTypeDouble> triggerVisit{0};
  triggerConnector.Accept(triggerVisit);

  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};
  resultGroupConnector.Accept(outVisitor);
  auto result = data.pFmu1_x->GetValue();

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = result_fmu5 * data.factor7 + data.offset;  // 94
  auto result_fmu2 = result_fmu7 * data.factor2 + data.offset;  // 191
  auto result_fmu1 = 0 * data.factor1 + data.offset;            // 3

  //rate = 2 , offset = 3
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[0].realValue, result_fmu7);
  ASSERT_EQ(data.fmu2->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu2->fmiValues[2].realValue, result_fmu2);

  ASSERT_EQ(data.fmu1->fmiValues[0].realValue, result_fmu2);
  ASSERT_EQ(data.fmu1->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu1->fmiValues[2].realValue, result_fmu1);

  ASSERT_EQ(result, result_fmu1);
}

void TestSamePriority()
{
  SSPAlgorithmTests_Data data{};
  data.Init();

  data.pFmu5_a->SetPriority(35);
  data.pFmu5_b->SetPriority(35);
  data.pFmu5_x->SetPriority(35);
  data.pFmu5_y->SetPriority(35);

  data.pFmu7_a->SetPriority(35);
  data.pFmu7_b->SetPriority(35);
  data.pFmu7_x->SetPriority(35);
  data.pFmu7_y->SetPriority(35);

  data.pFmu5_x->connectors.emplace_back(data.pFmu7_a);  //If the Connectors are not joined correctly the test crashes

  GroupConnector rateSystemConnector{{data.pFmu5_a}};
  GroupConnector offsetSystemConnector{{data.pFmu5_b, data.pFmu7_b}};
  GroupConnector triggerConnector{{data.pFmu5_x, data.pFmu7_x}};  //Careful trigger must be called on x
  GroupConnector resultGroupConnector{{data.pFmu7_x}};

  UpdateInputVisitor<VariableTypeDouble> visitor_A{data.rate};
  UpdateInputVisitor<VariableTypeDouble> visitor_B{data.offset};
  rateSystemConnector.Accept(visitor_A);
  offsetSystemConnector.Accept(visitor_B);

  TriggerVisit<VariableTypeDouble> triggerVisit{0};
  triggerConnector.Accept(triggerVisit);

  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};
  resultGroupConnector.Accept(outVisitor);
  auto result = data.pFmu7_x->GetValue();

  auto result_fmu5 = data.rate * data.factor5 + data.offset;    // 13
  auto result_fmu7 = result_fmu5 * data.factor7 + data.offset;  // 94

  //rate = 2 , offset = 3
  ASSERT_EQ(data.fmu5->fmiValues[0].realValue, data.rate);
  ASSERT_EQ(data.fmu5->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu5->fmiValues[2].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[0].realValue, result_fmu5);
  ASSERT_EQ(data.fmu7->fmiValues[1].realValue, data.offset);
  ASSERT_EQ(data.fmu7->fmiValues[2].realValue, result_fmu7);

  ASSERT_EQ(result, result_fmu7);
}

TEST(SSPPrototypeTestSuite, ssdConnector_test_Determinismus)
{
  for (int i = 0; i < 20; i++)
  {
    TestSamePriority();
  }
}

TEST(SSPAlgorithmTests, FmuConnectorTest)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("integer",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  fmi2_integer_t integer{42};
  auto integerValue = FmuValue{.intValue = integer};

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));
  ON_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillByDefault(ReturnRef(integerValue));

  ScalarConnector<VariableTypeInt> intConnector{fakeFmuWrapper, "integer", 10};
  intConnector.SetValue(integer);
  auto integerOut = intConnector.GetValue();
  ASSERT_EQ(integer, integerOut);
}

TEST(SSPAlgorithmTests, RealConnectorTest)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("double",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Double, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  fmi2_real_t real{42};
  auto realValue = FmuValue{.realValue = real};

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));
  ON_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Double)).WillByDefault(ReturnRef(realValue));

  ScalarConnector<VariableTypeDouble> realConnector{fakeFmuWrapper, "double", 10};
  realConnector.SetValue(real);
  auto integerOut = realConnector.GetValue();
  ASSERT_EQ(real, integerOut);
}

TEST(SSPAlgorithmTests, IntegerConnectorWithVisitor)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("integer",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  fmi2_integer_t integer{42};
  auto integerValue = FmuValue{.intValue = integer};

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));
  ON_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillByDefault(ReturnRef(integerValue));

  ScalarConnector<VariableTypeInt> intConnector{fakeFmuWrapper, "integer", 10};

  UpdateInputVisitor<VariableTypeInt> inVisitor{integer};
  UpdateOutputVisitor<VariableTypeInt> outVisitor{};

  intConnector.Accept(inVisitor);
  intConnector.Accept(outVisitor);

  ASSERT_EQ(integer, outVisitor.GetData());
}

TEST(SSPAlgorithmTests, RealFmuConnectorWithVisitor)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("real",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  fmi2_real_t real{42};
  auto realValue = FmuValue{.realValue = real};

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));
  ON_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Double)).WillByDefault(ReturnRef(realValue));

  ScalarConnector<VariableTypeDouble> intConnector{fakeFmuWrapper, "real", 10};

  UpdateInputVisitor<VariableTypeDouble> inVisitor{real};
  UpdateOutputVisitor<VariableTypeDouble> outVisitor{};

  intConnector.Accept(inVisitor);
  intConnector.Accept(outVisitor);

  ASSERT_EQ(real, outVisitor.GetData());
}

TEST(SSPAlgorithmTests, OSMPConnectorTest)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

  osi3::SensorData sensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);
  std::string serializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size;
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
  auto baseLoValue = FmuValue{.intValue = std::get<FMI2>(lo)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = std::get<FMI2>(hi)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = std::get<FMI2>(size)};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  OsmpConnector<osi3::SensorData, FMI2> sensorDataConnector{"SensorData", "SensorData", fakeFmuWrapper, 10};
  auto sensorDataOut = std::dynamic_pointer_cast<const osi3::SensorData>(sensorDataConnector.GetMessage());

  ASSERT_TRUE(sensorDataOut->version().has_version_major());
  ASSERT_TRUE(sensorDataOut->version().has_version_minor());
  ASSERT_TRUE(sensorDataOut->version().has_version_patch());
  ASSERT_EQ(1, sensorDataOut->version().version_major());
  ASSERT_EQ(2, sensorDataOut->version().version_minor());
  ASSERT_EQ(3, sensorDataOut->version().version_patch());
}

TEST(SSPAlgorithmTests, OSMPConnectorWithVisitor)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables1, fmuVariables2;
  fmuVariables1.emplace<FMI2>();
  fmuVariables2.emplace<FMI2>();
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataIn.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataIn.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataIn.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataOut.base.lo",
               *std::make_shared<FmuVariable2>(
                   3, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataOut.base.hi",
               *std::make_shared<FmuVariable2>(
                   4, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataOut.size",
               *std::make_shared<FmuVariable2>(
                   5, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2In.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2In.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2In.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2Out.base.lo",
               *std::make_shared<FmuVariable2>(
                   3, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2Out.base.hi",
               *std::make_shared<FmuVariable2>(
                   4, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2Out.size",
               *std::make_shared<FmuVariable2>(
                   5, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  auto fakeFmuWrapper2 = std::make_shared<NiceMock<FakeFmuWrapper>>();

  osi3::SensorData sensorData{}, emptySensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);
  std::string serializedSensorData{}, emptySerializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size;
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  osi3::SensorData sensorData2{}, emptySensorData2{};
  sensorData2.mutable_version()->set_version_major(2);
  sensorData2.mutable_version()->set_version_minor(2);
  sensorData2.mutable_version()->set_version_patch(3);
  std::string serializedSensorData2{}, emptySerializedSensorData2{};
  sensorData2.SerializeToString(&serializedSensorData2);
  fmi_integer_t lo2{}, hi2{}, size2;
  size2.emplace<FMI2>(serializedSensorData2.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData2.data(), hi2, lo2);

  osi3::SensorData sensorData3{}, emptySensorData3{};
  sensorData3.mutable_version()->set_version_major(3);
  sensorData3.mutable_version()->set_version_minor(2);
  sensorData3.mutable_version()->set_version_patch(3);
  std::string serializedSensorData3{}, emptySerializedSensorData3{};
  sensorData3.SerializeToString(&serializedSensorData3);
  fmi_integer_t lo3{}, hi3{}, size3;
  size3.emplace<FMI2>(serializedSensorData3.length());
  ;
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData3.data(), hi3, lo3);

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables1));
  auto baseLoValue_in = FmuValue{.intValue = std::get<FMI2>(lo)};
  ON_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue_in));
  auto baseHiValue_in = FmuValue{.intValue = std::get<FMI2>(hi)};
  ON_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue_in));
  auto sizeValue_in = FmuValue{.intValue = std::get<FMI2>(size)};
  ON_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillByDefault(ReturnRef(sizeValue_in));

  auto baseLoValue_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper, GetValue(3, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue_out));
  auto baseHiValue_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper, GetValue(4, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue_out));
  auto sizeValue_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper, GetValue(5, VariableType::Int)).WillByDefault(ReturnRef(sizeValue_out));

  ON_CALL(*fakeFmuWrapper2, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables2));
  auto baseLoValue2_in = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(0, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue2_in));
  auto baseHiValue2_in = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(1, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue2_in));
  auto sizeValue2_in = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(2, VariableType::Int)).WillByDefault(ReturnRef(sizeValue2_in));

  auto baseLoValue2_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(3, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue2_out));
  auto baseHiValue2_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(4, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue2_out));
  auto sizeValue2_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(5, VariableType::Int)).WillByDefault(ReturnRef(sizeValue2_out));

  ON_CALL(*fakeFmuWrapper, Trigger(0))
      .WillByDefault(
          [lo2, hi2, size2, &baseLoValue_out, &baseHiValue_out, &sizeValue_out](int time)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseLoValue_out.intValue = std::get<FMI2>(lo2);
            baseHiValue_out.intValue = std::get<FMI2>(hi2);
            sizeValue_out.intValue = std::get<FMI2>(size2);
          });

  ON_CALL(*fakeFmuWrapper2, SetValue(_, 0, VariableType::Int))
      .WillByDefault(
          [&baseLoValue2_in](const auto& baseLoValue_out, auto, auto)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseLoValue2_in.intValue = baseLoValue_out.intValue;
          });

  ON_CALL(*fakeFmuWrapper2, SetValue(_, 1, VariableType::Int))
      .WillByDefault(
          [&baseHiValue2_in](const auto& baseHiValue_out, auto, auto)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseHiValue2_in.intValue = baseHiValue_out.intValue;
          });

  ON_CALL(*fakeFmuWrapper2, SetValue(_, 2, VariableType::Int))
      .WillByDefault(
          [&sizeValue2_in](const auto& sizeValue_out, auto, auto)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            sizeValue2_in.intValue = sizeValue_out.intValue;
          });

  ON_CALL(*fakeFmuWrapper2, Trigger(0))
      .WillByDefault(
          [lo3, hi3, size3, &baseLoValue2_out, &baseHiValue2_out, &sizeValue2_out](int time)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseLoValue2_out.intValue = std::get<FMI2>(lo3);
            baseHiValue2_out.intValue = std::get<FMI2>(hi3);
            sizeValue2_out.intValue = std::get<FMI2>(size3);
          });

  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU1_in
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorDataIn", "SensorDataIn", fakeFmuWrapper, 30);
  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU1_out
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorDataOut", "SensorDataOut", fakeFmuWrapper, 30);
  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU2_in
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorData2In", "SensorData2In", fakeFmuWrapper2, 20);
  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU2_out
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>(
          "SensorData2Out", "SensorData2Out", fakeFmuWrapper2, 20);

  GroupConnector triggerGroupConnector{{sensorViewConnectorFMU1_out, sensorViewConnectorFMU2_out}};
  sensorViewConnectorFMU1_out->connectors.emplace_back(sensorViewConnectorFMU2_in);

  //ON_CALL(*fakeFmuWrapper, GetValue(GetScalarVariableReference(fakeFmuWrapper,"SensorData"),
  //decltype(sensorData_2)).WillByDefault(Return(sensorData_2)));

  /*auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
  UpdateInputSignalVisitor inVisitor{0, sensorDataSignal, 0};
  inVisitor.Visit(sensorViewConnectorFMU1_in.get()); */

  TriggerSignalVisitor triggerSignalVisit{0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  triggerSignalVisit.Visit(triggerGroupConnector);

  ON_CALL(*(fakeFmuWrapper2), UpdateOutput)
      .WillByDefault(
          [sensorViewConnectorFMU2_out](int localLinkId, std::shared_ptr<const SignalInterface>& data, int time) mutable
          {
            const std::shared_ptr<const osi3::SensorData> sensorData
                = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnectorFMU2_out->GetMessage());
            data = std::make_shared<const SensorDataSignal>(*sensorData);
          });

  std::shared_ptr<SignalInterface const> outSignal{};
  UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  outVisitor.Visit(sensorViewConnectorFMU2_out.get());
  auto sensorDataSignalOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data);

  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_major());
  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_minor());
  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_patch());
  ASSERT_EQ(3, sensorDataSignalOut->sensorData.version().version_major());
  ASSERT_EQ(2, sensorDataSignalOut->sensorData.version().version_minor());
  ASSERT_EQ(3, sensorDataSignalOut->sensorData.version().version_patch());

  Mock::AllowLeak(fakeFmuWrapper2.get());
}

TEST(SSPAlgorithmTests, TestPropagateDataVisitor)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables1, fmuVariables2;
  fmuVariables1.emplace<FMI2>();
  fmuVariables2.emplace<FMI2>();
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataIn.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataIn.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataIn.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataOut.base.lo",
               *std::make_shared<FmuVariable2>(
                   3, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataOut.base.hi",
               *std::make_shared<FmuVariable2>(
                   4, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables1)
      .emplace("SensorDataOut.size",
               *std::make_shared<FmuVariable2>(
                   5, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2In.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2In.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2In.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2Out.base.lo",
               *std::make_shared<FmuVariable2>(
                   3, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2Out.base.hi",
               *std::make_shared<FmuVariable2>(
                   4, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables2)
      .emplace("SensorData2Out.size",
               *std::make_shared<FmuVariable2>(
                   5, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  auto fakeFmuWrapper2 = std::make_shared<NiceMock<FakeFmuWrapper>>();

  osi3::SensorData sensorData{}, emptySensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);
  std::string serializedSensorData{}, emptySerializedSensorData{};
  sensorData.SerializeToString(&serializedSensorData);
  fmi_integer_t lo{}, hi{}, size;
  size.emplace<FMI2>(serializedSensorData.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData.data(), hi, lo);

  osi3::SensorData sensorData2{}, emptySensorData2{};
  sensorData2.mutable_version()->set_version_major(2);
  sensorData2.mutable_version()->set_version_minor(2);
  sensorData2.mutable_version()->set_version_patch(3);
  std::string serializedSensorData2{}, emptySerializedSensorData2{};
  sensorData2.SerializeToString(&serializedSensorData2);
  fmi_integer_t lo2{}, hi2{}, size2;
  size2.emplace<FMI2>(serializedSensorData2.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData2.data(), hi2, lo2);

  osi3::SensorData sensorData3{}, emptySensorData3{};
  sensorData3.mutable_version()->set_version_major(3);
  sensorData3.mutable_version()->set_version_minor(2);
  sensorData3.mutable_version()->set_version_patch(3);
  std::string serializedSensorData3{}, emptySerializedSensorData3{};
  sensorData3.SerializeToString(&serializedSensorData3);
  fmi_integer_t lo3{}, hi3{}, size3;
  size3.emplace<FMI2>(serializedSensorData3.length());
  FmuHelper::encode_pointer_to_integer<FMI2>(serializedSensorData3.data(), hi3, lo3);

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables1));
  auto baseLoValue_in = FmuValue{.intValue = std::get<FMI2>(lo)};
  ON_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue_in));
  auto baseHiValue_in = FmuValue{.intValue = std::get<FMI2>(hi)};
  ON_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue_in));
  auto sizeValue_in = FmuValue{.intValue = std::get<FMI2>(size)};
  ON_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillByDefault(ReturnRef(sizeValue_in));

  auto baseLoValue_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper, GetValue(3, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue_out));
  auto baseHiValue_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper, GetValue(4, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue_out));
  auto sizeValue_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper, GetValue(5, VariableType::Int)).WillByDefault(ReturnRef(sizeValue_out));

  ON_CALL(*fakeFmuWrapper2, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables2));
  auto baseLoValue2_in = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(0, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue2_in));
  auto baseHiValue2_in = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(1, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue2_in));
  auto sizeValue2_in = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(2, VariableType::Int)).WillByDefault(ReturnRef(sizeValue2_in));

  auto baseLoValue2_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(3, VariableType::Int)).WillByDefault(ReturnRef(baseLoValue2_out));
  auto baseHiValue2_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(4, VariableType::Int)).WillByDefault(ReturnRef(baseHiValue2_out));
  auto sizeValue2_out = FmuValue{.intValue = 0};
  ON_CALL(*fakeFmuWrapper2, GetValue(5, VariableType::Int)).WillByDefault(ReturnRef(sizeValue2_out));

  ON_CALL(*fakeFmuWrapper, Trigger(0))
      .WillByDefault(
          [lo2, hi2, size2, &baseLoValue_out, &baseHiValue_out, &sizeValue_out](int time)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseLoValue_out.intValue = std::get<FMI2>(lo2);
            baseHiValue_out.intValue = std::get<FMI2>(hi2);
            sizeValue_out.intValue = std::get<FMI2>(size2);
          });

  ON_CALL(*fakeFmuWrapper2, SetValue(_, 0, VariableType::Int))
      .WillByDefault(
          [&baseLoValue2_in](const auto& baseLoValue_out, auto, auto)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseLoValue2_in.intValue = baseLoValue_out.intValue;
          });

  ON_CALL(*fakeFmuWrapper2, SetValue(_, 1, VariableType::Int))
      .WillByDefault(
          [&baseHiValue2_in](const auto& baseHiValue_out, auto, auto)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseHiValue2_in.intValue = baseHiValue_out.intValue;
          });

  ON_CALL(*fakeFmuWrapper2, SetValue(_, 2, VariableType::Int))
      .WillByDefault(
          [&sizeValue2_in](const auto& sizeValue_out, auto, auto)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            sizeValue2_in.intValue = sizeValue_out.intValue;
          });

  ON_CALL(*fakeFmuWrapper2, Trigger(0))
      .WillByDefault(
          [lo3, hi3, size3, &baseLoValue2_out, &baseHiValue2_out, &sizeValue2_out](int time)
          {
            //sensorData2.mutable_version()->set_version_major(2);
            baseLoValue2_out.intValue = std::get<FMI2>(lo3);
            baseHiValue2_out.intValue = std::get<FMI2>(hi3);
            sizeValue2_out.intValue = std::get<FMI2>(size3);
          });

  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU1_in
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorDataIn", "SensorDataIn", fakeFmuWrapper, 30);
  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU1_out
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorDataOut", "SensorDataOut", fakeFmuWrapper, 30);
  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU2_in
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorData2In", "SensorData2In", fakeFmuWrapper2, 20);
  std::shared_ptr<OSMPConnectorBase> sensorViewConnectorFMU2_out
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>(
          "SensorData2Out", "SensorData2Out", fakeFmuWrapper2, 20);

  std::vector<std::weak_ptr<ssp::ConnectorInterface>> connectors{};

  std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements, emptyElements2;
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyCon1, emptyCon2, emptyCon3, emptyCon4;

  FmuComponent fmu1("fmu1", std::move(emptyElements), std::move(emptyCon1), std::move(emptyCon2), fakeFmuWrapper);
  FmuComponent fmu2("fmu2", std::move(emptyElements2), std::move(emptyCon3), std::move(emptyCon4), fakeFmuWrapper2);
  sensorViewConnectorFMU1_out->connectors.emplace_back(sensorViewConnectorFMU2_in);

  SspTriggerVisitor sspTriggerVisitor(0);
  sspTriggerVisitor.Visit(&fmu1);
  sspTriggerVisitor.Visit(&fmu2);

  ON_CALL(*(fakeFmuWrapper2), UpdateOutput)
      .WillByDefault(
          [sensorViewConnectorFMU2_out](int localLinkId, std::shared_ptr<const SignalInterface>& data, int time) mutable
          {
            const std::shared_ptr<const osi3::SensorData> sensorData
                = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnectorFMU2_out->GetMessage());
            data = std::make_shared<const SensorDataSignal>(*sensorData);
          });

  std::shared_ptr<SignalInterface const> outSignal{};
  UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  outVisitor.Visit(sensorViewConnectorFMU2_out.get());
  auto sensorDataSignalOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data);

  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_major());
  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_minor());
  ASSERT_TRUE(sensorDataSignalOut->sensorData.version().has_version_patch());
  ASSERT_EQ(3, sensorDataSignalOut->sensorData.version().version_major());
  ASSERT_EQ(2, sensorDataSignalOut->sensorData.version().version_minor());
  ASSERT_EQ(3, sensorDataSignalOut->sensorData.version().version_patch());

  Mock::AllowLeak(fakeFmuWrapper2.get());
}

TEST(SSPAlgorithmTests, TestSspNetworkVisitor_Priority_Queue)
{
  SSPAlgorithmTests_Data data{};

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  auto fakeFmuWrapper2 = std::make_shared<NiceMock<FakeFmuWrapper>>();
  std::vector<std::weak_ptr<ssp::ConnectorInterface>> connectors{};

  ON_CALL(*fakeFmuWrapper, getPriority()).WillByDefault(Return(10));
  ON_CALL(*fakeFmuWrapper2, getPriority()).WillByDefault(Return(20));

  std::vector<std::shared_ptr<VisitableNetworkElement>> emptyElements1, emptyElements2{};
  std::vector<std::shared_ptr<ConnectorInterface>> ec1, ec2, ec3, ec4{};

  auto fmu1Test = std::make_unique<FmuComponent>(
      "fmu1", std::move(emptyElements1), std::move(ec1), std::move(ec2), fakeFmuWrapper);
  auto fmu2Test = std::make_unique<FmuComponent>(
      "fmu2", std::move(emptyElements2), std::move(ec3), std::move(ec4), fakeFmuWrapper2);

  ssp::System twoFmuSystem("Test System");
  twoFmuSystem.EmplaceElement(std::move(fmu1Test));
  twoFmuSystem.EmplaceElement(std::move(fmu2Test));

  testing::InSequence seq;

  EXPECT_CALL(*fakeFmuWrapper2, Trigger(0));
  EXPECT_CALL(*fakeFmuWrapper, Trigger(0));

  SspTriggerVisitor sspTriggerVisitor(0);
  sspTriggerVisitor.Visit(&twoFmuSystem);

  Mock::AllowLeak(fakeFmuWrapper2.get());
}

TEST(SSPAlgorithmTests, TestSspNetworkVisitor_Priority_Queue_reverse)
{
  SSPAlgorithmTests_Data data{};

  auto fakeFmuWrapper = std::make_shared<NiceMock<FakeFmuWrapper>>();
  auto fakeFmuWrapper2 = std::make_shared<NiceMock<FakeFmuWrapper>>();

  std::vector<std::weak_ptr<ssp::ConnectorInterface>> connectors{};

  ON_CALL(*fakeFmuWrapper, getPriority()).WillByDefault(Return(20));
  ON_CALL(*fakeFmuWrapper2, getPriority()).WillByDefault(Return(10));

  std::vector<std::shared_ptr<VisitableNetworkElement>> emptyElements1, emptyElements2{};
  std::vector<std::shared_ptr<ConnectorInterface>> ec1, ec2, ec3, ec4{};

  auto fmu1Test = std::make_unique<FmuComponent>(
      "fmu1", std::move(emptyElements1), std::move(ec1), std::move(ec2), fakeFmuWrapper);
  auto fmu2Test = std::make_unique<FmuComponent>(
      "fmu2", std::move(emptyElements2), std::move(ec3), std::move(ec4), fakeFmuWrapper2);

  ssp::System twoFmuSystem("Test System");
  twoFmuSystem.EmplaceElement(std::move(fmu1Test));
  twoFmuSystem.EmplaceElement(std::move(fmu2Test));

  testing::InSequence seq;

  EXPECT_CALL(*fakeFmuWrapper, Trigger(0));
  EXPECT_CALL(*fakeFmuWrapper2, Trigger(0));

  SspTriggerVisitor sspTriggerVisitor(0);
  sspTriggerVisitor.Visit(&twoFmuSystem);

  Mock::AllowLeak(fakeFmuWrapper2.get());
}

TEST(SSPAlgorithmTests, OSMPConnectorSensorData)
{
  SSPAlgorithmTests_Data data{};

  FmuVariables fmuVariables;
  fmuVariables.emplace<FMI2>();
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.lo",
               *std::make_shared<FmuVariable2>(
                   0, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.base.hi",
               *std::make_shared<FmuVariable2>(
                   1, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));
  std::get<FMI2>(fmuVariables)
      .emplace("SensorData.size",
               *std::make_shared<FmuVariable2>(
                   2, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete));

  auto fakeFmuWrapper = std::make_shared<FakeFmuWrapper>();

  osi3::SensorData sensorData{};
  sensorData.mutable_version()->set_version_major(1);
  sensorData.mutable_version()->set_version_minor(2);
  sensorData.mutable_version()->set_version_patch(3);

  EXPECT_CALL(*fakeFmuWrapper, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));
  auto baseLoValue = FmuValue{.intValue = 0};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
  auto baseHiValue = FmuValue{.intValue = 0};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
  auto sizeValue = FmuValue{.intValue = 0};
  EXPECT_CALL(*fakeFmuWrapper, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 0, VariableType::Int))
      .WillRepeatedly([&baseLoValue](auto value, auto, auto) { baseLoValue = value; });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 1, VariableType::Int))
      .WillRepeatedly([&baseHiValue](auto value, auto, auto) { baseHiValue = value; });
  EXPECT_CALL(*fakeFmuWrapper, SetValue(_, 2, VariableType::Int))
      .WillRepeatedly([&sizeValue](auto value, auto, auto) { sizeValue = value; });

  ON_CALL(*fakeFmuWrapper, GetFmuVariables()).WillByDefault(ReturnRef(fmuVariables));

  std::shared_ptr<OSMPConnectorBase> sensorViewConnector
      = std::make_shared<OsmpConnector<osi3::SensorData, FMI2>>("SensorData", "SensorData", fakeFmuWrapper, 10);

  auto sensorDataSignal = std::make_shared<SensorDataSignal const>(sensorData);
  UpdateInputSignalVisitor inVisitor{2, sensorDataSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  inVisitor.Visit(sensorViewConnector.get());

  ON_CALL(*fakeFmuWrapper, UpdateOutput)
      .WillByDefault(
          [sensorViewConnector](int localLinkId, std::shared_ptr<const SignalInterface>& data, int time) mutable
          {
            const std::shared_ptr<const osi3::SensorData> sensorData
                = std::dynamic_pointer_cast<const osi3::SensorData>(sensorViewConnector->GetMessage());
            data = std::make_shared<const SensorDataSignal>(*sensorData);
          });

  Mock::AllowLeak(fakeFmuWrapper.get());

  std::shared_ptr<SignalInterface const> outSignal{};
  UpdateOutputSignalVisitor outVisitor{6, outSignal, 0, &data.fakeWorld, &data.fakeAgent, &data.fakeCallback};
  outVisitor.Visit(sensorViewConnector.get());
  auto sensorDataOut = std::dynamic_pointer_cast<SensorDataSignal const>(outVisitor.data)->sensorData;

  ASSERT_TRUE(sensorDataOut.version().has_version_major());
  ASSERT_TRUE(sensorDataOut.version().has_version_minor());
  ASSERT_TRUE(sensorDataOut.version().has_version_patch());
  ASSERT_EQ(1, sensorDataOut.version().version_major());
  ASSERT_EQ(2, sensorDataOut.version().version_minor());
  ASSERT_EQ(3, sensorDataOut.version().version_patch());
}