/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "OWL/DataTypes.h"
#include "laneGeometryElementGenerator.h"

using ::testing::DoubleNear;

struct LaneGeometryElementGenerator_DataSet
{
  // do not change order of items
  // unless you also change it in INSTANTIATE_TEST_CASE_P
  // (see bottom of file)

  // data for generator
  Common::Vector2d<units::length::meter_t> origin;
  units::length::meter_t width;
  units::length::meter_t length;
  units::angle::radian_t hdg;

  // expected values
  Common::Vector2d<units::length::meter_t> currentLeft;
  Common::Vector2d<units::length::meter_t> currentReference;
  Common::Vector2d<units::length::meter_t> currentRight;
  Common::Vector2d<units::length::meter_t> nextLeft;
  Common::Vector2d<units::length::meter_t> nextReference;
  Common::Vector2d<units::length::meter_t> nextRight;

  /// \brief This stream will be shown in case the test fails
  friend std::ostream& operator<<(std::ostream& os, const LaneGeometryElementGenerator_DataSet& obj)
  {
    return os << "origin " << obj.origin << ", "
              << "width " << obj.width << ", "
              << "length " << obj.length << ", "
              << "hdg " << obj.hdg << ","
              << "currentLeft " << obj.currentLeft << ", "
              << "currentReference " << obj.currentReference << ", "
              << "currentRight " << obj.currentRight << ", "
              << "nextLeft " << obj.nextLeft << ", "
              << "nextReference " << obj.nextReference << ", "
              << "nextRight " << obj.nextRight;
  }
};

/// \see https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md
class LaneGeometryElementGenerator : public ::testing::Test,
                                     public ::testing::WithParamInterface<LaneGeometryElementGenerator_DataSet>
{
};

TEST_P(LaneGeometryElementGenerator, RectangularLaneGeometryElementVerification)
{
  auto data = GetParam();

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement(
      data.origin, data.width, data.length, data.hdg);

  auto current = elementUnderTest.joints.current;
  auto next = elementUnderTest.joints.next;

  EXPECT_THAT(current.points.left.x.value(), DoubleNear(data.currentLeft.x.value(), 1e-2));
  EXPECT_THAT(current.points.left.y.value(), DoubleNear(data.currentLeft.y.value(), 1e-2));
  EXPECT_THAT(current.points.reference.x.value(), DoubleNear(data.currentReference.x.value(), 1e-2));
  EXPECT_THAT(current.points.reference.y.value(), DoubleNear(data.currentReference.y.value(), 1e-2));
  EXPECT_THAT(current.points.right.x.value(), DoubleNear(data.currentRight.x.value(), 1e-2));
  EXPECT_THAT(current.points.right.y.value(), DoubleNear(data.currentRight.y.value(), 1e-2));

  EXPECT_THAT(next.points.left.x.value(), DoubleNear(data.nextLeft.x.value(), 1e-2));
  EXPECT_THAT(next.points.left.y.value(), DoubleNear(data.nextLeft.y.value(), 1e-2));
  EXPECT_THAT(next.points.reference.x.value(), DoubleNear(data.nextReference.x.value(), 1e-2));
  EXPECT_THAT(next.points.reference.y.value(), DoubleNear(data.nextReference.y.value(), 1e-2));
  EXPECT_THAT(next.points.right.x.value(), DoubleNear(data.nextRight.x.value(), 1e-2));
  EXPECT_THAT(next.points.right.y.value(), DoubleNear(data.nextRight.y.value(), 1e-2));
}

TEST_P(LaneGeometryElementGenerator, TriangularLaneGeometryElementVerification)
{
  auto data = GetParam();

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::TriangularLaneGeometryElement(
      data.origin, data.width, data.length, data.hdg);

  auto current = elementUnderTest.joints.current;
  auto next = elementUnderTest.joints.next;

  EXPECT_THAT(current.points.left.x.value(), DoubleNear(data.currentReference.x.value(), 1e-2));
  EXPECT_THAT(current.points.left.y.value(), DoubleNear(data.currentReference.y.value(), 1e-2));
  EXPECT_THAT(current.points.reference.x.value(), DoubleNear(data.currentReference.x.value(), 1e-2));
  EXPECT_THAT(current.points.reference.y.value(), DoubleNear(data.currentReference.y.value(), 1e-2));
  EXPECT_THAT(current.points.right.x.value(), DoubleNear(data.currentReference.x.value(), 1e-2));
  EXPECT_THAT(current.points.right.y.value(), DoubleNear(data.currentReference.y.value(), 1e-2));

  EXPECT_THAT(next.points.left.x.value(), DoubleNear(data.nextLeft.x.value(), 1e-2));
  EXPECT_THAT(next.points.left.y.value(), DoubleNear(data.nextLeft.y.value(), 1e-2));
  EXPECT_THAT(next.points.reference.x.value(), DoubleNear(data.nextReference.x.value(), 1e-2));
  EXPECT_THAT(next.points.reference.y.value(), DoubleNear(data.nextReference.y.value(), 1e-2));
  EXPECT_THAT(next.points.right.x.value(), DoubleNear(data.nextRight.x.value(), 1e-2));
  EXPECT_THAT(next.points.right.y.value(), DoubleNear(data.nextRight.y.value(), 1e-2));
}

INSTANTIATE_TEST_CASE_P(RandomSetWithRotatingHeading,
                        LaneGeometryElementGenerator,
                        testing::Values(
                            /*                                         origin    width length    hdg          curr_left
                               curr_ref              curr_right            next_left             next_ref next_right  */
                            /*                                           v         v     v        v               v v v
                               v                     v                     v       */
                            LaneGeometryElementGenerator_DataSet{{-16_m, 1_m},
                                                                 8_m,
                                                                 12_m,
                                                                 -6.28_rad,
                                                                 {-16.01_m, 5.00_m},
                                                                 {-16.00_m, 1.00_m},
                                                                 {-15.99_m, -3.00_m},
                                                                 {-4.01_m, 5.04_m},
                                                                 {-4.00_m, 1.04_m},
                                                                 {-3.99_m, -2.96_m}},
                            LaneGeometryElementGenerator_DataSet{{7_m, 0_m},
                                                                 4_m,
                                                                 12_m,
                                                                 -5.50_rad,
                                                                 {5.59_m, 1.42_m},
                                                                 {7.00_m, 0.00_m},
                                                                 {8.41_m, -1.42_m},
                                                                 {14.09_m, 9.88_m},
                                                                 {15.50_m, 8.47_m},
                                                                 {16.92_m, 7.05_m}},
                            LaneGeometryElementGenerator_DataSet{{-7_m, 10_m},
                                                                 3_m,
                                                                 12_m,
                                                                 -4.71_rad,
                                                                 {-8.50_m, 10.00_m},
                                                                 {-7.00_m, 10.00_m},
                                                                 {-5.50_m, 10.00_m},
                                                                 {-8.53_m, 22.00_m},
                                                                 {-7.03_m, 22.00_m},
                                                                 {-5.53_m, 22.00_m}},
                            LaneGeometryElementGenerator_DataSet{{-17_m, 13_m},
                                                                 7_m,
                                                                 12_m,
                                                                 -3.93_rad,
                                                                 {-19.48_m, 10.53_m},
                                                                 {-17.00_m, 13.00_m},
                                                                 {-14.52_m, 15.47_m},
                                                                 {-27.94_m, 19.04_m},
                                                                 {-25.46_m, 21.51_m},
                                                                 {-22.98_m, 23.98_m}},
                            LaneGeometryElementGenerator_DataSet{{-17_m, 7_m},
                                                                 9_m,
                                                                 11_m,
                                                                 -3.14_rad,
                                                                 {-16.99_m, 2.50_m},
                                                                 {-17.00_m, 7.00_m},
                                                                 {-17.01_m, 11.50_m},
                                                                 {-27.99_m, 2.48_m},
                                                                 {-28.00_m, 6.98_m},
                                                                 {-28.01_m, 11.48_m}},
                            LaneGeometryElementGenerator_DataSet{{-1_m, 14_m},
                                                                 3_m,
                                                                 11_m,
                                                                 -2.36_rad,
                                                                 {0.06_m, 12.94_m},
                                                                 {-1.00_m, 14.00_m},
                                                                 {-2.06_m, 15.06_m},
                                                                 {-7.75_m, 5.19_m},
                                                                 {-8.81_m, 6.25_m},
                                                                 {-9.86_m, 7.32_m}},
                            LaneGeometryElementGenerator_DataSet{{-15_m, 8_m},
                                                                 3_m,
                                                                 11_m,
                                                                 -1.57_rad,
                                                                 {-13.50_m, 8.00_m},
                                                                 {-15.00_m, 8.00_m},
                                                                 {-16.50_m, 8.00_m},
                                                                 {-13.49_m, -3.00_m},
                                                                 {-14.99_m, -3.00_m},
                                                                 {-16.49_m, -3.00_m}},
                            LaneGeometryElementGenerator_DataSet{{-15_m, 6_m},
                                                                 3_m,
                                                                 11_m,
                                                                 -0.79_rad,
                                                                 {-13.93_m, 7.06_m},
                                                                 {-15.00_m, 6.00_m},
                                                                 {-16.07_m, 4.94_m},
                                                                 {-6.19_m, -0.76_m},
                                                                 {-7.26_m, -1.81_m},
                                                                 {-8.32_m, -2.87_m}},
                            LaneGeometryElementGenerator_DataSet{{19_m, 3_m},
                                                                 10_m,
                                                                 12_m,
                                                                 0.00_rad,
                                                                 {19.00_m, 8.00_m},
                                                                 {19.00_m, 3.00_m},
                                                                 {19.00_m, -2.00_m},
                                                                 {31.00_m, 8.00_m},
                                                                 {31.00_m, 3.00_m},
                                                                 {31.00_m, -2.00_m}},
                            LaneGeometryElementGenerator_DataSet{{-12_m, -9_m},
                                                                 6_m,
                                                                 12_m,
                                                                 0.79_rad,
                                                                 {-14.13_m, -6.89_m},
                                                                 {-12.00_m, -9.00_m},
                                                                 {-9.87_m, -11.11_m},
                                                                 {-5.68_m, 1.64_m},
                                                                 {-3.55_m, -0.48_m},
                                                                 {-1.42_m, -2.59_m}},
                            LaneGeometryElementGenerator_DataSet{{5_m, -17_m},
                                                                 3_m,
                                                                 10_m,
                                                                 1.57_rad,
                                                                 {3.50_m, -17.00_m},
                                                                 {5.00_m, -17.00_m},
                                                                 {6.50_m, -17.00_m},
                                                                 {3.51_m, -7.00_m},
                                                                 {5.01_m, -7.00_m},
                                                                 {6.51_m, -7.00_m}},
                            LaneGeometryElementGenerator_DataSet{{14_m, -8_m},
                                                                 9_m,
                                                                 11_m,
                                                                 2.36_rad,
                                                                 {10.83_m, -11.19_m},
                                                                 {14.00_m, -8.00_m},
                                                                 {17.17_m, -4.81_m},
                                                                 {3.02_m, -3.45_m},
                                                                 {6.19_m, -0.25_m},
                                                                 {9.36_m, 2.94_m}},
                            LaneGeometryElementGenerator_DataSet{{1_m, -2_m},
                                                                 6_m,
                                                                 12_m,
                                                                 3.14_rad,
                                                                 {1.00_m, -5.00_m},
                                                                 {1.00_m, -2.00_m},
                                                                 {1.00_m, 1.00_m},
                                                                 {-11.00_m, -4.98_m},
                                                                 {-11.00_m, -1.98_m},
                                                                 {-11.00_m, 1.02_m}},
                            LaneGeometryElementGenerator_DataSet{{-3_m, 16_m},
                                                                 5_m,
                                                                 10_m,
                                                                 3.93_rad,
                                                                 {-1.23_m, 14.24_m},
                                                                 {-3.00_m, 16.00_m},
                                                                 {-4.77_m, 17.76_m},
                                                                 {-8.28_m, 7.15_m},
                                                                 {-10.05_m, 8.91_m},
                                                                 {-11.82_m, 10.67_m}},
                            LaneGeometryElementGenerator_DataSet{{14_m, -20_m},
                                                                 6_m,
                                                                 11_m,
                                                                 4.71_rad,
                                                                 {17.00_m, -20.01_m},
                                                                 {14.00_m, -20.00_m},
                                                                 {11.00_m, -19.99_m},
                                                                 {16.97_m, -31.01_m},
                                                                 {13.97_m, -31.00_m},
                                                                 {10.97_m, -30.99_m}},
                            LaneGeometryElementGenerator_DataSet{{14_m, 1_m},
                                                                 9_m,
                                                                 11_m,
                                                                 5.5_rad,
                                                                 {17.17_m, 4.19_m},
                                                                 {14.00_m, 1.00_m},
                                                                 {10.83_m, -2.19_m},
                                                                 {24.97_m, -3.57_m},
                                                                 {21.80_m, -6.76_m},
                                                                 {18.62_m, -9.95_m}},
                            LaneGeometryElementGenerator_DataSet{{5_m, 6_m},
                                                                 8_m,
                                                                 11_m,
                                                                 6.28_rad,
                                                                 {5.01_m, 10.00_m},
                                                                 {5.00_m, 6.00_m},
                                                                 {4.99_m, 2.00_m},
                                                                 {16.01_m, 9.96_m},
                                                                 {16.00_m, 5.96_m},
                                                                 {15.99_m, 1.96_m}},
                            LaneGeometryElementGenerator_DataSet{{5_m, 6_m},
                                                                 6_m,
                                                                 0_m,
                                                                 0.0_rad,
                                                                 {5.00_m, 9.00_m},
                                                                 {5.00_m, 6.00_m},
                                                                 {5.00_m, 3.00_m},
                                                                 {5.00_m, 9.00_m},
                                                                 {5.00_m, 6.00_m},
                                                                 {5.00_m, 3.00_m}}));
