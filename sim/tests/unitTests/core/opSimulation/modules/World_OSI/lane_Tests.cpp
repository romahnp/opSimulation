/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Generators/laneGeometryElementGenerator.h"
#include "fakeAgent.h"
#include "fakeLane.h"
#include "fakeLaneManager.h"
#include "fakeMovingObject.h"
#include "fakeRadio.h"
#include "fakeSection.h"
#include "fakeWorld.h"
#include "fakeWorldData.h"

using ::testing::_;
using ::testing::AllOf;
using ::testing::Eq;
using ::testing::Ge;
using ::testing::IsNull;
using ::testing::Le;
using ::testing::Lt;
using ::testing::Return;
using ::testing::ReturnRef;

/////////////////////////////////////////////////////////////////////////////
TEST(GetNeighbouringJoints, DistanceBetweenTwoJoints_ReturnsJoints)
{
  const units::length::meter_t length{10.0};

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   0.0_m,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint* prevJoint;
  const OWL::Primitive::LaneGeometryJoint* nextJoint;

  std::tie(prevJoint, nextJoint) = lane.GetNeighbouringJoints(length / 2.0);

  ASSERT_NE(prevJoint, nullptr);
  ASSERT_NE(nextJoint, nullptr);

  ASSERT_EQ(prevJoint->points.reference.x.value(), 0.0);
  ASSERT_EQ(nextJoint->points.reference.x, length);
}

TEST(GetNeighbouringJoints, DistanceAtPrevJoint_ReturnsJoints)
{
  const units::length::meter_t length = 10.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   0.0_m,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint* prevJoint;
  const OWL::Primitive::LaneGeometryJoint* nextJoint;

  std::tie(prevJoint, nextJoint) = lane.GetNeighbouringJoints(0.0_m);

  ASSERT_NE(prevJoint, nullptr);
  ASSERT_NE(nextJoint, nullptr);

  ASSERT_EQ(prevJoint->points.reference.x.value(), 0.0);
  ASSERT_EQ(nextJoint->points.reference.x, length);
}

TEST(GetNeighbouringJoints, DistanceAtNextJoint_ReturnsPrevJointOnly)
{
  const units::length::meter_t length = 10.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   0.0_m,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint* prevJoint;
  const OWL::Primitive::LaneGeometryJoint* nextJoint;

  std::tie(prevJoint, nextJoint) = lane.GetNeighbouringJoints(10.0_m);

  ASSERT_NE(prevJoint, nullptr);
  ASSERT_EQ(nextJoint, nullptr);

  ASSERT_EQ(prevJoint->points.reference.x.value(), 10.0);
}

TEST(GetNeighbouringJoints, DistanceAtMiddleJoint_ReturnsJoints)
{
  const units::length::meter_t length = 10.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement1
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   0.0_m,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  auto laneGeometryElement2
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({10.0_m, 0.0_m},  // origin
                                                                                   0.0_m,            // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement1.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement2.joints.current;
  const OWL::Primitive::LaneGeometryJoint& thirdJoint = laneGeometryElement2.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint3 = thirdJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint3 = thirdJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint3 = thirdJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint3, referencePoint3, rightPoint3, 2.0 * length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint* prevJoint;
  const OWL::Primitive::LaneGeometryJoint* nextJoint;

  std::tie(prevJoint, nextJoint) = lane.GetNeighbouringJoints(10.0_m);

  ASSERT_NE(prevJoint, nullptr);
  ASSERT_NE(nextJoint, nullptr);

  ASSERT_EQ(prevJoint->points.reference.x.value(), 10.0);
  ASSERT_EQ(nextJoint->points.reference.x.value(), 20.0);
}

/////////////////////////////////////////////////////////////////////////////
//ToDo: Add more tests including CurvedRoad, heading and multilane

TEST(GetInterpolatedPointsAtDistance, DistanceAtPrevJoint_ReturnsPrevJoint)
{
  const units::length::meter_t length = 10.0_m;
  const units::length::meter_t width = 3.75_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint::Points ipPoints = lane.GetInterpolatedPointsAtDistance(0.0_m);

  ASSERT_NE(&firstJoint, &secondJoint);
  ASSERT_NE(&firstJoint, nullptr);
  ASSERT_EQ(ipPoints.left, firstJoint.points.left);
  ASSERT_EQ(ipPoints.right, firstJoint.points.right);
  ASSERT_EQ(ipPoints.reference, firstJoint.points.reference);
}

TEST(GetInterpolatedPointsAtDistance, DistanceAtNextJoint_ReturnsNextJoint)
{
  const units::length::meter_t length = 10.0_m;
  const units::length::meter_t width = 3.75_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint::Points ipPoints = lane.GetInterpolatedPointsAtDistance(10.0_m);

  ASSERT_NE(&firstJoint, &secondJoint);
  ASSERT_NE(&firstJoint, nullptr);
  ASSERT_EQ(ipPoints.left, secondJoint.points.left);
  ASSERT_EQ(ipPoints.right, secondJoint.points.right);
  ASSERT_EQ(ipPoints.reference, secondJoint.points.reference);
}

TEST(GetInterpolatedPointsAtDistance, DistanceBetweenJoints_ReturnsInterpolatedPoint)
{
  const units::length::meter_t length = 10.0_m;
  const units::length::meter_t width = 4.0_m;
  const units::length::meter_t distance = 5.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint::Points ipPoints = lane.GetInterpolatedPointsAtDistance(distance);
  const Common::Vector2d<units::length::meter_t> defaultValues;

  ASSERT_NE(&firstJoint, &secondJoint);
  ASSERT_NE(&firstJoint, nullptr);
  ASSERT_EQ(ipPoints.left.x, distance);
  ASSERT_EQ(ipPoints.left.y, width / 2);

  ASSERT_EQ(ipPoints.reference.x, distance);
  ASSERT_EQ(ipPoints.reference.y.value(), 0.0);

  ASSERT_EQ(ipPoints.right.x, distance);
  ASSERT_EQ(ipPoints.right.y, -width / 2);
}

TEST(GetInterpolatedPointsAtDistance, DistanceBetweenJointsWithHeading_ReturnsInterpolatedPoint)
{
  const units::length::meter_t length = 10.0_m;
  const units::length::meter_t width = 4.0_m;
  const units::length::meter_t distance = 5.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,           // width
                                                                                   length,
                                                                                   45_deg);  // heading

  const OWL::Primitive::LaneGeometryJoint& firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  const OWL::Primitive::LaneGeometryJoint::Points ipPoints = lane.GetInterpolatedPointsAtDistance(distance);
  const Common::Vector2d<units::length::meter_t> defaultValues;

  ASSERT_NE(&firstJoint, &secondJoint);
  ASSERT_NE(&firstJoint, nullptr);
  // ASSERT_EQ(ipPoints.left.x, );
  // ASSERT_EQ(ipPoints.left.y, );

  // ASSERT_EQ(ipPoints.reference.x, );
  // ASSERT_EQ(ipPoints.reference.y, );

  // ASSERT_EQ(ipPoints.right.x, );
  // ASSERT_EQ(ipPoints.right.y, );
}

/////////////////////////////////////////////////////////////////////////////

TEST(GetLaneWidth, LaneWidthQueryOnTriangularLane_ReturnsZeroWidthAtStartAndFullWidthAtEnd)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 0.0_rad;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest
      = OWL::Testing::LaneGeometryElementGenerator::TriangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                  width,
                                                                                  length,
                                                                                  heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, heading);

  ASSERT_DOUBLE_EQ(lane.GetWidth(0.0_m).value(), 0.0);
  ASSERT_EQ(lane.GetWidth(length).value(), width.value());
}

TEST(GetLaneWidth, LaneWidthQueryOnTriangularLane_ReturnsCorrectlyInterpolatedWidth)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 0.0_rad;
  const units::length::meter_t queryPosition = 3.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest
      = OWL::Testing::LaneGeometryElementGenerator::TriangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                  width,
                                                                                  length,
                                                                                  heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, heading);

  ASSERT_EQ(lane.GetWidth(queryPosition).value(), width.value() / length.value() * queryPosition.value());
}

TEST(GetLaneWidth, LaneWidthQueryOnRotatedTriangularLane_ReturnsCorrectlyInterpolatedWidth)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 30_deg;
  const units::length::meter_t queryPosition = 3.0_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest
      = OWL::Testing::LaneGeometryElementGenerator::TriangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                  width,
                                                                                  length,
                                                                                  heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, heading);

  ASSERT_EQ(lane.GetWidth(queryPosition).value(), width.value() / length.value() * queryPosition.value());
}

TEST(DISABLED_GetLaneWidth, LaneWidthQueryOnRectangularLane_ReturnsFullWidthAtStartAndEnd)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 0.0_rad;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,
                                                                                   length,
                                                                                   heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, heading);

  ASSERT_EQ(lane.GetWidth(0.0_m), width);
  ASSERT_EQ(lane.GetWidth(length), width);
}

/////////////////////////////////////////////////////////////////////////////

TEST(GetLaneCurvature, LaneCurvatureQueryOnCurvedLane_ReturnsZeroCurvatureAtStartAndFullCurvatureAtEnd)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 0.0_rad;
  const units::curvature::inverse_meter_t curvatureStart = 0.2_i_m;
  const units::curvature::inverse_meter_t curvatureEnd = 0.5_i_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElementWithCurvature(
      {0.0_m, 0.0_m},  // origin
      width,
      length,
      curvatureStart,
      curvatureEnd,
      heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  units::curvature::inverse_meter_t curvature1 = firstJoint.curvature;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  units::curvature::inverse_meter_t curvature2 = secondJoint.curvature;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, curvature1, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, curvature2, heading);

  ASSERT_EQ(lane.GetCurvature(0.0_m), curvatureStart);
  ASSERT_EQ(lane.GetCurvature(length), curvatureEnd);
}

TEST(GetLaneCurvature, LaneCurvatureQueryOnCurvedLane_ReturnsCorrectlyInterpolatedCurvature)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 0.0_rad;
  const units::length::meter_t queryPosition = 3.0_m;

  const units::curvature::inverse_meter_t curvatureStart = 0.2_i_m;
  const units::curvature::inverse_meter_t curvatureEnd = 0.5_i_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElementWithCurvature(
      {0.0_m, 0.0_m},  // origin
      width,
      length,
      curvatureStart,
      curvatureEnd,
      heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  units::curvature::inverse_meter_t curvature1 = firstJoint.curvature;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  units::curvature::inverse_meter_t curvature2 = secondJoint.curvature;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, curvature1, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, curvature2, heading);

  ASSERT_EQ(lane.GetCurvature(queryPosition),
            curvatureStart + (curvatureEnd - curvatureStart) / length * queryPosition);
}

TEST(GetLaneCurvature, LaneCurvatureQueryOnRotatedCurvedLane_ReturnsCorrectlyInterpolatedCurvature)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 30_deg;
  const units::length::meter_t queryPosition = 3.0_m;

  const units::curvature::inverse_meter_t curvatureStart = 0.2_i_m;
  const units::curvature::inverse_meter_t curvatureEnd = 0.5_i_m;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElementWithCurvature(
      {0.0_m, 0.0_m},  // origin
      width,
      length,
      curvatureStart,
      curvatureEnd,
      heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  units::curvature::inverse_meter_t curvature1 = firstJoint.curvature;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  units::curvature::inverse_meter_t curvature2 = secondJoint.curvature;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, curvature1, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, curvature2, heading);

  ASSERT_EQ(lane.GetCurvature(queryPosition),
            curvatureStart + (curvatureEnd - curvatureStart) / length * queryPosition);
}

TEST(GetLaneCurvature, LaneCurvatureQueryOnStraightLane_ReturnsZeroCurvature)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 0.0_rad;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,
                                                                                   length,
                                                                                   heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  units::curvature::inverse_meter_t curvature1 = firstJoint.curvature;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  units::curvature::inverse_meter_t curvature2 = secondJoint.curvature;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, curvature1, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, curvature2, heading);

  ASSERT_DOUBLE_EQ(lane.GetCurvature(0.0_m).value(), 0.0);
  ASSERT_DOUBLE_EQ(lane.GetCurvature(length / 2.0).value(), 0.0);
  ASSERT_DOUBLE_EQ(lane.GetCurvature(length).value(), 0.0);
}

/////////////////////////////////////////////////////////////////////////////

TEST(GetLaneDirection, LaneWidthQueryOnRotatedStraightLane_ReturnsCorrectDirection)
{
  const units::length::meter_t width = 3.75_m;
  const units::length::meter_t length = 10_m;
  const units::angle::radian_t heading = 30_deg;

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   width,
                                                                                   length,
                                                                                   heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, heading);

  ASSERT_EQ(lane.GetDirection(0.0_m), heading);
  ASSERT_EQ(lane.GetDirection(length / 2.0), heading);
  ASSERT_EQ(lane.GetDirection(length), heading);
}

TEST(GetLaneDirection, LaneWidthQueryOnRotatedConstantlyCurvedLane_ReturnsCorrectDirection)
{
  const units::length::meter_t width{3.75};
  const units::length::meter_t length{10};
  const units::angle::radian_t heading{30_deg};
  const units::curvature::inverse_meter_t curvatureStart{0.2};
  const units::curvature::inverse_meter_t curvatureEnd{0.2};

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElementWithCurvature(
      {0.0_m, 0.0_m},  // origin
      width,
      length,
      curvatureStart,
      curvatureEnd,
      heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  units::curvature::inverse_meter_t curvature1 = firstJoint.curvature;
  units::angle::radian_t direction1 = firstJoint.sHdg;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  units::curvature::inverse_meter_t curvature2 = secondJoint.curvature;
  units::angle::radian_t direction2 = firstJoint.sHdg;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, curvature1, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, curvature2, heading);

  ASSERT_EQ(lane.GetDirection(0.0_m), direction1);
  ASSERT_EQ(lane.GetDirection(length / 2.0), direction1 + (direction2 - direction1) * length / 2.0_m);
  ASSERT_EQ(lane.GetDirection(length), direction2);
}

TEST(GetLaneDirection, LaneWidthQueryOnRotatedCurvedLane_ReturnsCorrectDirection)
{
  const units::length::meter_t width{3.75};
  const units::length::meter_t length{10};
  const units::angle::radian_t heading{30_deg};
  const units::curvature::inverse_meter_t curvatureStart{0.2};
  const units::curvature::inverse_meter_t curvatureEnd{0.8};

  osi3::Lane osiLane;
  osi3::LogicalLane osiLogicalLane;
  OWL::Implementation::Lane lane(&osiLane, &osiLogicalLane, nullptr, -1);

  auto elementUnderTest = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElementWithCurvature(
      {0.0_m, 0.0_m},  // origin
      width,
      length,
      curvatureStart,
      curvatureEnd,
      heading);

  const OWL::Primitive::LaneGeometryJoint& firstJoint = elementUnderTest.joints.current;
  const OWL::Primitive::LaneGeometryJoint& secondJoint = elementUnderTest.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  units::curvature::inverse_meter_t curvature1 = firstJoint.curvature;
  units::angle::radian_t direction1 = firstJoint.sHdg;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;
  units::curvature::inverse_meter_t curvature2 = secondJoint.curvature;
  units::angle::radian_t direction2 = firstJoint.sHdg;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, curvature1, heading);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, curvature2, heading);

  ASSERT_EQ(lane.GetDirection(0.0_m), direction1);
  ASSERT_EQ(lane.GetDirection(length / 2.0), direction1 + (direction2 - direction1) * length / 2.0_m);
  ASSERT_EQ(lane.GetDirection(length), direction2);
}
