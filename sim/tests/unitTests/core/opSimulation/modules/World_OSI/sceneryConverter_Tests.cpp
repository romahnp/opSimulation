/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "EntityRepository.h"
#include "Generators/laneGeometryElementGenerator.h"
#include "SceneryConverter.h"
#include "fakeAgent.h"
#include "fakeConnection.h"
#include "fakeDataBuffer.h"
#include "fakeJunction.h"
#include "fakeLane.h"
#include "fakeLaneManager.h"
#include "fakeMovingObject.h"
#include "fakeOdRoad.h"
#include "fakeRadio.h"
#include "fakeRoadLane.h"
#include "fakeRoadLaneSection.h"
#include "fakeRoadObject.h"
#include "fakeSection.h"
#include "fakeWorld.h"
#include "fakeWorldData.h"

using namespace OWL;

using ::testing::_;
using ::testing::Const;
using ::testing::Invoke;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;
using ::testing::SetArgReferee;

class FakeRepository : public openpass::entity::RepositoryInterface
{
public:
  MOCK_METHOD0(Reset, void());
  MOCK_METHOD1(Register, openpass::type::EntityId(openpass::type::EntityInfo));
  MOCK_METHOD2(Register, openpass::type::EntityId(openpass::entity::EntityType entityType, openpass::type::EntityInfo));
};

std::tuple<const OWL::Primitive::LaneGeometryJoint *, const OWL::Primitive::LaneGeometryJoint *>
CreateSectionPartJointsRect(units::length::meter_t length)
{
  osi3::Lane osiLane;
  OWL::Implementation::Lane lane(&osiLane, nullptr, nullptr, -1);

  auto laneGeometryElement
      = OWL::Testing::LaneGeometryElementGenerator::RectangularLaneGeometryElement({0.0_m, 0.0_m},  // origin
                                                                                   0.0_m,           // width
                                                                                   length,
                                                                                   0.0_rad);  // heading

  const OWL::Primitive::LaneGeometryJoint &firstJoint = laneGeometryElement.joints.current;
  const OWL::Primitive::LaneGeometryJoint &secondJoint = laneGeometryElement.joints.next;

  Common::Vector2d<units::length::meter_t> leftPoint1 = firstJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint1 = firstJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint1 = firstJoint.points.right;
  Common::Vector2d<units::length::meter_t> leftPoint2 = secondJoint.points.left;
  Common::Vector2d<units::length::meter_t> referencePoint2 = secondJoint.points.reference;
  Common::Vector2d<units::length::meter_t> rightPoint2 = secondJoint.points.right;

  lane.AddLaneGeometryJoint(leftPoint1, referencePoint1, rightPoint1, 0.0_m, 0.0_i_m, 0.0_rad);
  lane.AddLaneGeometryJoint(leftPoint2, referencePoint2, rightPoint2, length, 0.0_i_m, 0.0_rad);

  return lane.GetNeighbouringJoints(length * 0.5);
}

class FakeScenery : public SceneryInterface
{
public:
  MOCK_METHOD0(Clear, void());
  MOCK_METHOD2(AddRoad, RoadInterface *(const std::string &id, const double length));
  MOCK_METHOD1(AddJunction, JunctionInterface *(const std::string &id));
  MOCK_CONST_METHOD0(GetRoads, std::map<std::string, RoadInterface *> &());
  MOCK_CONST_METHOD1(GetRoad, RoadInterface *(const std::string &id));
  MOCK_CONST_METHOD0(GetJunctions, std::map<std::string, JunctionInterface *> &());
  MOCK_CONST_METHOD1(GetJunction, JunctionInterface *(const std::string &id));
};

class FakeRoadLink : public RoadLinkInterface
{
public:
  MOCK_CONST_METHOD0(GetType, RoadLinkType());
  MOCK_CONST_METHOD0(GetElementType, RoadLinkElementType());
  MOCK_CONST_METHOD0(GetElementId, const std::string &());
  MOCK_CONST_METHOD0(GetContactPoint, ContactPointType());
};

void Connect(const RoadInterface *incomingRoad,
             const RoadInterface *connectingRoad,
             const RoadInterface *outgoingRoad,
             ContactPointType incomingContactPoint,
             ContactPointType connectingContactPoint,
             ContactPointType outgoingContactPoint,
             const std::map<int, int> &laneIdMapping)
{
}

TEST(SceneryConverter, RefactoringSafeguard_DoNotDelete)
{
  FakeScenery stubScenery;
  FakeConnection stubConnection;
  ON_CALL(stubConnection, GetContactPoint()).WillByDefault(Return(ContactPointType::Start));

  FakeOdRoad incomingRoad;
  std::string incomingRoadId = "incomingRoadId";
  ON_CALL(stubConnection, GetIncommingRoadId()).WillByDefault(ReturnRef(incomingRoadId));
  ON_CALL(stubScenery, GetRoad("incomingRoadId")).WillByDefault(Return(&incomingRoad));

  FakeOdRoad connectingRoad;
  std::string connectingRoadId = "connectingRoadId";
  ON_CALL(stubConnection, GetConnectingRoadId()).WillByDefault(ReturnRef(connectingRoadId));
  ON_CALL(stubScenery, GetRoad("connectingRoadId")).WillByDefault(Return(&connectingRoad));

  FakeRoadLink roadLink;
  std::string outgoingRoadId = "outgoingRoadId";
  ON_CALL(roadLink, GetType()).WillByDefault(Return(RoadLinkType::Successor));
  ON_CALL(roadLink, GetElementId()).WillByDefault(ReturnRef(outgoingRoadId));

  std::vector<RoadLinkInterface *> roadLinks = {{&roadLink}};
  ON_CALL(connectingRoad, GetRoadLinks()).WillByDefault(ReturnRef(roadLinks));

  FakeOdRoad outgoingRoad;
  ON_CALL(stubScenery, GetRoad("outgoingRoadId")).WillByDefault(Return(&outgoingRoad));

  FakeRoadLaneSection stubRoadLaneSection;
  std::vector<RoadLaneSectionInterface *> stubLaneSections = {&stubRoadLaneSection};
  ON_CALL(incomingRoad, GetLaneSections()).WillByDefault(ReturnRef(stubLaneSections));
  ON_CALL(connectingRoad, GetLaneSections()).WillByDefault(ReturnRef(stubLaneSections));

  FakeJunction stubJunction;
  std::map<std::string, ConnectionInterface *> stubConnections = {{"", &stubConnection}};
  ON_CALL(stubJunction, GetConnections()).WillByDefault(Return(stubConnections));

  std::map<int, int> links = {{}};
  ON_CALL(stubConnection, GetLinks()).WillByDefault(ReturnRef(links));

  auto [status, error_message] = Internal::ConnectJunction(&stubScenery,
                                                           &stubJunction,

                                                           [&](const JunctionInterface *,
                                                               const RoadInterface *incomingRoad,
                                                               const RoadInterface *connectingRoad,
                                                               const RoadInterface *outgoingRoad,
                                                               ContactPointType incomingContactPoint,
                                                               ContactPointType connectingContactPoint,
                                                               ContactPointType outgoingContactPoint,
                                                               const std::map<int, int> &laneIdMapping)
                                                           {
                                                             Connect(incomingRoad,
                                                                     connectingRoad,
                                                                     outgoingRoad,
                                                                     incomingContactPoint,
                                                                     connectingContactPoint,
                                                                     outgoingContactPoint,
                                                                     laneIdMapping);
                                                           });

  ASSERT_THAT(status, true);
}

struct RoadNetworkBuilderTestFixture
{
  FakeScenery stubScenery;
  FakeOdRoad fakeRoad1;
  FakeOdRoad fakeRoad2;
  FakeOdRoad fakeRoad12;
  std::string roadId1{"RoadR1"};
  std::string roadId2{"RoadR2"};
  std::string roadId12{"RoadR12"};
  std::map<std::string, RoadInterface *> roads{{roadId1, &fakeRoad1}, {roadId2, &fakeRoad2}, {roadId12, &fakeRoad12}};

  FakeRoadLane fakeRoadLane;
  FakeRoadLaneSection stubRoadLaneSection1;
  FakeRoadLaneSection stubRoadLaneSection2;
  FakeRoadLaneSection stubRoadLaneSection12;
  std::vector<RoadLaneSectionInterface *> stubLaneSections1{&stubRoadLaneSection1};
  std::vector<RoadLaneSectionInterface *> stubLaneSections2{&stubRoadLaneSection2};
  std::vector<RoadLaneSectionInterface *> stubLaneSections12{&stubRoadLaneSection12};
  std::map<int, RoadLaneInterface *> lanes_road1{{1, &fakeRoadLane}, {0, &fakeRoadLane}, {-1, &fakeRoadLane}};
  std::map<int, RoadLaneInterface *> lanes_road2{{1, &fakeRoadLane}, {0, &fakeRoadLane}, {-1, &fakeRoadLane}};

  FakeRoadLink roadLink1;
  FakeRoadLink roadLink2;
  FakeRoadLink roadLink12;
  FakeRoadLink roadLink21;
  std::vector<RoadLinkInterface *> roadLinks1{{&roadLink1}};
  std::vector<RoadLinkInterface *> roadLinks2{{&roadLink2}};
  std::vector<RoadLinkInterface *> roadLinks12{{&roadLink12}, {&roadLink21}};

  RoadNetworkBuilderTestFixture(ContactPointType expected_contactPointTypePredecessor,
                                ContactPointType expected_contactPointTypeSuccessor)
  {
    ON_CALL(stubScenery, GetRoads()).WillByDefault(ReturnRef(roads));
    ON_CALL(fakeRoad1, GetLaneSections()).WillByDefault(ReturnRef(stubLaneSections1));
    ON_CALL(fakeRoad2, GetLaneSections()).WillByDefault(ReturnRef(stubLaneSections2));
    ON_CALL(fakeRoad12, GetLaneSections()).WillByDefault(ReturnRef(stubLaneSections12));
    ON_CALL(stubRoadLaneSection1, GetLanes()).WillByDefault(ReturnRef(lanes_road1));
    ON_CALL(stubRoadLaneSection2, GetLanes()).WillByDefault(ReturnRef(lanes_road2));
    ON_CALL(fakeRoad1, GetRoadLinks()).WillByDefault(ReturnRef(roadLinks1));
    ON_CALL(fakeRoad2, GetRoadLinks()).WillByDefault(ReturnRef(roadLinks2));
    ON_CALL(fakeRoad12, GetRoadLinks()).WillByDefault(ReturnRef(roadLinks12));
    ON_CALL(roadLink1, GetElementType()).WillByDefault(Return(RoadLinkElementType::Junction));
    ON_CALL(roadLink2, GetElementType()).WillByDefault(Return(RoadLinkElementType::Junction));
    ON_CALL(roadLink12, GetElementType()).WillByDefault(Return(RoadLinkElementType::Road));
    ON_CALL(roadLink21, GetElementType()).WillByDefault(Return(RoadLinkElementType::Road));
    ON_CALL(roadLink12, GetType()).WillByDefault(Return(RoadLinkType::Predecessor));
    ON_CALL(roadLink21, GetType()).WillByDefault(Return(RoadLinkType::Successor));
    ON_CALL(roadLink12, GetElementId()).WillByDefault(ReturnRef(roadId1));
    ON_CALL(roadLink21, GetElementId()).WillByDefault(ReturnRef(roadId2));
    ON_CALL(roadLink12, GetContactPoint()).WillByDefault(Return(expected_contactPointTypePredecessor));
    ON_CALL(roadLink21, GetContactPoint()).WillByDefault(Return(expected_contactPointTypeSuccessor));
  }
};

void CheckRoadGraphEquality(const RoadGraph roadGraph1,
                            const RoadGraphVertex vertex1,
                            const RoadGraph roadGraph2,
                            const RoadGraphVertex vertex2)
{
  ASSERT_THAT(roadGraph1.vertex_set(), roadGraph2.vertex_set());
  ASSERT_THAT(out_degree(vertex1, roadGraph1), out_degree(vertex2, roadGraph2));

  for (auto edge1 : roadGraph1.out_edge_list(vertex1))
  {
    for (auto edge2 : roadGraph2.out_edge_list(vertex2))
    {
      ASSERT_THAT(edge1.get_iter()->m_source, edge2.get_iter()->m_source);
      ASSERT_THAT(edge1.m_target, edge2.m_target);
    }
  }
}

TEST(RoadNetworkBuilder, JunctionInOdDirection_ContactPointTypeEnd)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::End, ContactPointType::End);

  std::map<int, RoadLaneInterface *> lanes_road12{{0, &fixture.fakeRoadLane}, {-1, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, true};
  RouteElement routeElement2{fixture.roadId2, false};
  RouteElement routeElement12{fixture.roadId12, true};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, true}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR1_2, expectedVertexR12, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR2_1, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_2);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_1);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionAgainstOdDirection_ContactPointTypeEnd)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::End, ContactPointType::End);

  std::map<int, RoadLaneInterface *> lanes_road12{{1, &fixture.fakeRoadLane}, {0, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, false};
  RouteElement routeElement2{fixture.roadId2, true};
  RouteElement routeElement12{fixture.roadId12, false};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, false}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR1_1, expectedRoadGraph);
  add_edge(expectedVertexR2_2, expectedVertexR12, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_1);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_2);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionInOdDirection_ContactPointTypeStart)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::Start, ContactPointType::Start);

  std::map<int, RoadLaneInterface *> lanes_road12{{0, &fixture.fakeRoadLane}, {-1, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, false};
  RouteElement routeElement2{fixture.roadId2, true};
  RouteElement routeElement12{fixture.roadId12, true};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, true}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR1_1, expectedVertexR12, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR2_2, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_1);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_2);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionAgainstOdDirection_ContactPointTypeStart)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::Start, ContactPointType::Start);

  std::map<int, RoadLaneInterface *> lanes_road12{{1, &fixture.fakeRoadLane}, {0, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, true};
  RouteElement routeElement2{fixture.roadId2, false};
  RouteElement routeElement12{fixture.roadId12, false};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, false}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR1_2, expectedRoadGraph);
  add_edge(expectedVertexR2_1, expectedVertexR12, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_2);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_1);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionInOdDirection_PredecessorContactPointTypeEnd_SuccessorContactPointTypeStart)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::End, ContactPointType::Start);

  std::map<int, RoadLaneInterface *> lanes_road12{{0, &fixture.fakeRoadLane}, {-1, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, true};
  RouteElement routeElement2{fixture.roadId2, true};
  RouteElement routeElement12{fixture.roadId12, true};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, true}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR1_2, expectedVertexR12, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR2_2, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_2);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_2);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionAgainstOdDirection_PredecessorContactPointTypeEnd_SuccessorContactPointTypeStart)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::End, ContactPointType::Start);

  std::map<int, RoadLaneInterface *> lanes_road12{{1, &fixture.fakeRoadLane}, {0, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, false};
  RouteElement routeElement2{fixture.roadId2, false};
  RouteElement routeElement12{fixture.roadId12, false};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, true}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR1_1, expectedRoadGraph);
  add_edge(expectedVertexR2_1, expectedVertexR12, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_1);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_1);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionInOdDirection_PredecessorContactPointTypeStart_SuccessorContactPointTypeEnd)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::Start, ContactPointType::End);

  std::map<int, RoadLaneInterface *> lanes_road12{{0, &fixture.fakeRoadLane}, {-1, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, false};
  RouteElement routeElement2{fixture.roadId2, false};
  RouteElement routeElement12{fixture.roadId12, true};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, true}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR1_1, expectedVertexR12, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR2_1, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_1);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_1);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}

TEST(RoadNetworkBuilder, JunctionAgainstOdDirection_PredecessorContactPointTypeStart_SuccessorContactPointTypeEnd)
{
  RoadNetworkBuilderTestFixture fixture(ContactPointType::Start, ContactPointType::End);

  std::map<int, RoadLaneInterface *> lanes_road12{{1, &fixture.fakeRoadLane}, {0, &fixture.fakeRoadLane}};
  ON_CALL(fixture.stubRoadLaneSection12, GetLanes()).WillByDefault(ReturnRef(lanes_road12));

  RoadNetworkBuilder roadNetworkBuilder(fixture.stubScenery);
  auto [roadGraph, vertices] = roadNetworkBuilder.Build();
  RouteElement routeElement1{fixture.roadId1, true};
  RouteElement routeElement2{fixture.roadId2, true};
  RouteElement routeElement12{fixture.roadId12, false};
  auto vertexR1 = vertices[routeElement1];
  auto vertexR2 = vertices[routeElement2];
  auto vertexR12 = vertices[routeElement12];

  RoadGraph expectedRoadGraph;
  auto expectedVertexR1_1 = add_vertex(RouteElement{fixture.roadId1, false}, expectedRoadGraph);
  auto expectedVertexR1_2 = add_vertex(RouteElement{fixture.roadId1, true}, expectedRoadGraph);
  auto expectedVertexR12 = add_vertex(RouteElement{fixture.roadId12, true}, expectedRoadGraph);
  auto expectedVertexR2_1 = add_vertex(RouteElement{fixture.roadId2, false}, expectedRoadGraph);
  auto expectedVertexR2_2 = add_vertex(RouteElement{fixture.roadId2, true}, expectedRoadGraph);
  add_edge(expectedVertexR12, expectedVertexR1_2, expectedRoadGraph);
  add_edge(expectedVertexR2_2, expectedVertexR12, expectedRoadGraph);

  CheckRoadGraphEquality(roadGraph, vertexR1, expectedRoadGraph, expectedVertexR1_2);
  CheckRoadGraphEquality(roadGraph, vertexR2, expectedRoadGraph, expectedVertexR2_2);
  CheckRoadGraphEquality(roadGraph, vertexR12, expectedRoadGraph, expectedVertexR12);
}