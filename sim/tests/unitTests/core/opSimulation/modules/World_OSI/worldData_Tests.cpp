/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "WorldData.h"
#include "fakeCallback.h"

using ::testing::_;
using ::testing::Eq;
using ::testing::HasSubstr;

struct SetEnvironmentIllumination_Data
{
  mantle_api::Illumination sunIntensity;
  osi3::EnvironmentalConditions_AmbientIllumination expectedLevel;
};

class SetEnvironmentIlluminationTest : public ::testing::TestWithParam<SetEnvironmentIllumination_Data>
{
};

TEST_P(SetEnvironmentIlluminationTest, SetCorrectLevelInGroundtruth)
{
  mantle_api::Weather weather;
  weather.illumination = GetParam().sunIntensity;
  OWL::WorldData worldData{nullptr};

  worldData.SetEnvironment(weather);

  EXPECT_THAT(worldData.GetOsiGroundTruth().environmental_conditions().ambient_illumination(),
              Eq(GetParam().expectedLevel));
}

INSTANTIATE_TEST_SUITE_P(
    IlluminationTests,
    SetEnvironmentIlluminationTest,
    ::testing::Values(
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel1,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL1},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel2,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL2},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel3,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL3},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel4,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL4},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel5,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL5},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel6,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL6},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel7,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL7},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel8,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL8},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kLevel9,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL9},
        SetEnvironmentIllumination_Data{mantle_api::Illumination::kOther,
                                        osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_OTHER},
        SetEnvironmentIllumination_Data{
            mantle_api::Illumination::kUnknown,
            osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_UNKNOWN}));

struct SetEnvironmentFog_Data
{
  mantle_api::Fog visualRange;
  osi3::EnvironmentalConditions_Fog expectedLevel;
};

class SetEnvironmentFogTest : public ::testing::TestWithParam<SetEnvironmentFog_Data>
{
};

TEST_P(SetEnvironmentFogTest, SetCorrectLevelInGroundtruth)
{
  mantle_api::Weather weather;
  weather.fog = GetParam().visualRange;
  OWL::WorldData worldData{nullptr};

  worldData.SetEnvironment(weather);

  EXPECT_THAT(worldData.GetOsiGroundTruth().environmental_conditions().fog(), Eq(GetParam().expectedLevel));
}

INSTANTIATE_TEST_SUITE_P(
    Fog,
    SetEnvironmentFogTest,
    ::testing::Values(
        SetEnvironmentFog_Data{mantle_api::Fog::kDense, osi3::EnvironmentalConditions_Fog_FOG_DENSE},
        SetEnvironmentFog_Data{mantle_api::Fog::kThick, osi3::EnvironmentalConditions_Fog_FOG_THICK},
        SetEnvironmentFog_Data{mantle_api::Fog::kLight, osi3::EnvironmentalConditions_Fog_FOG_LIGHT},
        SetEnvironmentFog_Data{mantle_api::Fog::kMist, osi3::EnvironmentalConditions_Fog_FOG_MIST},
        SetEnvironmentFog_Data{mantle_api::Fog::kPoorVisibility, osi3::EnvironmentalConditions_Fog_FOG_POOR_VISIBILITY},
        SetEnvironmentFog_Data{mantle_api::Fog::kModerateVisibility,
                               osi3::EnvironmentalConditions_Fog_FOG_MODERATE_VISIBILITY},
        SetEnvironmentFog_Data{mantle_api::Fog::kGoodVisibility, osi3::EnvironmentalConditions_Fog_FOG_GOOD_VISIBILITY},
        SetEnvironmentFog_Data{mantle_api::Fog::kExcellentVisibility,
                               osi3::EnvironmentalConditions_Fog_FOG_EXCELLENT_VISIBILITY},
        SetEnvironmentFog_Data{mantle_api::Fog::kOther, osi3::EnvironmentalConditions_Fog_FOG_OTHER},
        SetEnvironmentFog_Data{mantle_api::Fog::kUnknown, osi3::EnvironmentalConditions_Fog_FOG_UNKNOWN}));

struct SetEnvironmentPrecipitation_Data
{
  mantle_api::Precipitation intensity;
  osi3::EnvironmentalConditions_Precipitation expectedLevel;
};

class SetEnvironmentPrecipitationTest : public ::testing::TestWithParam<SetEnvironmentPrecipitation_Data>
{
};

TEST_P(SetEnvironmentPrecipitationTest, SetCorrectLevelInGroundtruth)
{
  mantle_api::Weather weather;
  weather.precipitation = GetParam().intensity;
  OWL::WorldData worldData{nullptr};

  worldData.SetEnvironment(weather);

  EXPECT_THAT(worldData.GetOsiGroundTruth().environmental_conditions().precipitation(), Eq(GetParam().expectedLevel));
}

INSTANTIATE_TEST_SUITE_P(
    Precipitation,
    SetEnvironmentPrecipitationTest,
    ::testing::Values(
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kUnknown,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_UNKNOWN},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kOther,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_OTHER},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kNone,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_NONE},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kVeryLight,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_LIGHT},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kLight,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_LIGHT},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kModerate,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_MODERATE},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kHeavy,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_HEAVY},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kVeryHeavy,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_HEAVY},
        SetEnvironmentPrecipitation_Data{mantle_api::Precipitation::kExtreme,
                                         osi3::EnvironmentalConditions_Precipitation_PRECIPITATION_EXTREME}));

TEST(TurningRateImport, NonExistingConnection_LogsWarning)
{
  FakeCallback callback;

  OWL::WorldData worldData{&callback};
  RoadGraph roadGraph;
  auto nodeA = add_vertex(RouteElement{"RoadA", true}, roadGraph);
  auto nodeB = add_vertex(RouteElement{"RoadB", true}, roadGraph);
  auto nodeC = add_vertex(RouteElement{"RoadC", true}, roadGraph);
  add_edge(nodeA, nodeB, roadGraph);
  add_edge(nodeA, nodeC, roadGraph);
  RoadGraphVertexMapping vertexMapping;
  worldData.SetRoadGraph(std::move(roadGraph), std::move(vertexMapping));

  TurningRates turningRates{{"RoadA", "RoadB", 1.}, {"RoadA", "RoadC", 1.}, {"RoadB", "RoadC", 1.}};

  EXPECT_CALL(callback, Log(CbkLogLevel::Warning, _, _, HasSubstr("from RoadB to RoadC")));

  worldData.SetTurningRates(turningRates);
}

TEST(TurningRateImport, MissingTurningRate_LogsWarning)
{
  FakeCallback callback;

  OWL::WorldData worldData{&callback};
  RoadGraph roadGraph;
  auto nodeA = add_vertex(RouteElement{"RoadA", true}, roadGraph);
  auto nodeB = add_vertex(RouteElement{"RoadB", true}, roadGraph);
  auto nodeC = add_vertex(RouteElement{"RoadC", true}, roadGraph);
  add_edge(nodeA, nodeB, roadGraph);
  add_edge(nodeA, nodeC, roadGraph);
  RoadGraphVertexMapping vertexMapping;
  worldData.SetRoadGraph(std::move(roadGraph), std::move(vertexMapping));

  TurningRates turningRates{{"RoadA", "RoadB", 1.}};

  EXPECT_CALL(callback, Log(CbkLogLevel::Warning, _, _, HasSubstr("from RoadA to RoadC")));

  worldData.SetTurningRates(turningRates);
}

TEST(TurningRateImport, AllConnectionsDefined_LogsNoWarning)
{
  FakeCallback callback;

  OWL::WorldData worldData{&callback};
  RoadGraph roadGraph;
  auto nodeA = add_vertex(RouteElement{"RoadA", true}, roadGraph);
  auto nodeB = add_vertex(RouteElement{"RoadB", true}, roadGraph);
  auto nodeC = add_vertex(RouteElement{"RoadC", true}, roadGraph);
  add_edge(nodeA, nodeB, roadGraph);
  add_edge(nodeA, nodeC, roadGraph);
  RoadGraphVertexMapping vertexMapping;
  worldData.SetRoadGraph(std::move(roadGraph), std::move(vertexMapping));

  TurningRates turningRates{{"RoadA", "RoadB", 1.}, {"RoadA", "RoadC", 1.}};

  EXPECT_CALL(callback, Log(CbkLogLevel::Warning, _, _, _)).Times(0);

  worldData.SetTurningRates(turningRates);
}
