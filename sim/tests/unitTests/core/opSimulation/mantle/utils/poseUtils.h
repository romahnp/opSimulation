/*******************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <gtest/gtest.h>

#include <iostream>

#include "MantleAPI/Common/pose.h"

testing::AssertionResult AreNear(const mantle_api::Vec3<units::length::meter_t> &v1,
                                 mantle_api::Vec3<units::length::meter_t> v2)
{
  const auto length = v1.Length().value();
  const auto epsilon = length * 1e-6;

  if (std::abs(length - v2.Length().value()) > 1e-6 || std::abs(v1.x.value() - v2.x.value()) > epsilon
      || std::abs(v1.y.value() - v2.y.value()) > epsilon || std::abs(v1.z.value() - v2.z.value()) > epsilon)
  {
    return testing::AssertionFailure() << v1 << " and " << v2 << " have different length.";
  }
  return testing::AssertionSuccess();
}

namespace mantle_api
{

inline std::ostream &operator<<(std::ostream &os, const mantle_api::Vec3<units::length::meter_t> &v)
{
  return os << v.x << " / " << v.y << " / " << v.z;
}

inline std::ostream &operator<<(std::ostream &os, const mantle_api::Orientation3<units::angle::radian_t> &o)
{
  return os << o.yaw << " / " << o.pitch << " / " << o.roll;
}

}  // namespace mantle_api