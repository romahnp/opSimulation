/********************************************************************************
 * Copyright (c) 2017-2020 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "polygon.h"

using namespace units::literals;

bool Polygon::CalculateCentroid(Common::Vector2d<units::length::meter_t> &cog)
{
  // if empty, return empty vector
  if (vertices.size() == 0)
  {
    return false;
  }

  // close polygon by adding the first vertex at the end
  std::vector<Common::Vector2d<units::length::meter_t>> v;
  v = vertices;
  v.push_back(v[0]);

  units::area::square_meter_t area = 0_sq_m;

  Common::Vector2d<units::unit_t<units::cubed<units::length::meter>>> tmpVector(
      units::unit_t<units::cubed<units::length::meter>>(0), units::unit_t<units::cubed<units::length::meter>>(0));

  for (unsigned int iV = 0; iV < vertices.size(); iV++)
  {
    units::area::square_meter_t det;
    det = (v[iV].x * v[iV + 1].y - v[iV + 1].x * v[iV].y);
    area = area + det;
    tmpVector.x = tmpVector.x + (v[iV].x + v[iV + 1].x) * det;
    tmpVector.y = tmpVector.y + (v[iV].y + v[iV + 1].y) * det;
  }

  cog.x = tmpVector.x * (1 / (3 * area));
  cog.y = tmpVector.y * (1 / (3 * area));

  return true;
}

double Polygon::GetNumberOfVertices()
{
  return vertices.size();
}

std::vector<Common::Vector2d<units::length::meter_t>> Polygon::GetVertices() const
{
  return vertices;
}

void Polygon::SetVertices(const std::vector<Common::Vector2d<units::length::meter_t>> &value)
{
  vertices = value;
}

void Polygon::Translate(Common::Vector2d<units::length::meter_t> translationVector)
{
  for (unsigned int i = 0; i < vertices.size(); i++)
  {
    vertices.at(i).Translate(translationVector);
  }
}
