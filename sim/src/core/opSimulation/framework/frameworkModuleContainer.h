/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  frameworkModuleContainer.h
 *   \brief This file stores all core framework modules
 */
//-----------------------------------------------------------------------------

#pragma once

#include "agentBlueprintProvider.h"
#include "agentFactory.h"
#include "bindings/dataBuffer.h"
#include "bindings/dataBufferBinding.h"
#include "bindings/modelBinding.h"
#include "bindings/observationBinding.h"
#include "bindings/spawnPointBinding.h"
#include "bindings/stochastics.h"
#include "bindings/stochasticsBinding.h"
#include "bindings/world.h"
#include "collisionDetector.h"
#include "common/coreDataPublisher.h"
#include "common/opExport.h"
#include "directories.h"
#include "frameworkModules.h"
#include "include/agentBlueprintInterface.h"
#include "include/collisionDetectionInterface.h"
#include "include/configurationContainerInterface.h"
#include "include/frameworkModuleContainerInterface.h"
#include "observationNetwork.h"
#include "spawnPointNetwork.h"

namespace core
{

//! This class represents the container of all core framework modules
class SIMULATIONCOREEXPORT FrameworkModuleContainer final : public FrameworkModuleContainerInterface
{
public:
  /**
   * @brief FrameworkModuleContainer constructor
   *
   * @param[in] frameworkModules          Framework modules
   * @param[in] runtimeInformation        Common runtimeInformation
   * @param[in] callbacks                 Pointer to the callbacks
   */
  FrameworkModuleContainer(FrameworkModules frameworkModules,
                           const openpass::common::RuntimeInformation &runtimeInformation,
                           CallbackInterface *callbacks);

  CallbackInterface *GetCallbacks() override;
  AgentFactoryInterface *GetAgentFactory() override;
  DataBufferInterface *GetDataBuffer() override;
  CollisionDetectionInterface *GetCollisionDetection() override;
  ObservationNetworkInterface *GetObservationNetwork() override;
  SpawnPointNetworkInterface *GetSpawnPointNetwork() override;
  StochasticsInterface *GetStochastics() override;
  WorldInterface *GetWorld() override;

private:
  CallbackInterface *callbacks;

  DataBufferBinding dataBufferBinding;
  DataBuffer dataBuffer;
  openpass::publisher::CoreDataPublisher coreDataPublisher;

  StochasticsBinding stochasticsBinding;
  Stochastics stochastics;

  WorldBinding worldBinding;
  World world;

  std::map<std::string, ObservationBinding> observationBindings;
  ObservationNetwork observationNetwork;

  CollisionDetector collisionDetector;

  ModelBinding modelBinding;

  AgentFactory agentFactory;

  std::map<std::string, SpawnPointBinding> spawnPointBindings;
  SpawnPointNetwork spawnPointNetwork;
};

}  //namespace core
