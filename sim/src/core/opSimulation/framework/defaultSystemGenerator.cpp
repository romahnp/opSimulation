/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "defaultSystemGenerator.h"

#include "agentType.h"
#include "componentType.h"

System DefaultSystemGenerator::make()
{
  System system;
  system.agentProfileName = "DefaultSystem";
  system.agentType = std::make_shared<core::AgentType>();

  system.agentType->AddChannel(0);
  system.agentType->AddChannel(1);
  system.agentType->AddChannel(2);

  auto dynamics_Scenario
      = std::make_shared<core::ComponentType>("Dynamics_Scenario", false, 3, 0, 0, 100, "Dynamics_Scenario");
  dynamics_Scenario->AddOutputLink(0, 1);
  system.agentType->AddComponent(dynamics_Scenario);

  auto dynamics_Collision
      = std::make_shared<core::ComponentType>("Dynamics_Collision", false, 3, 0, 0, 100, "Dynamics_Collision");
  dynamics_Collision->AddOutputLink(0, 0);
  system.agentType->AddComponent(dynamics_Collision);

  auto signalPrioritizer
      = std::make_shared<core::ComponentType>("SignalPrioritizer", false, 2, 0, 0, 100, "SignalPrioritizer");
  signalPrioritizer->AddInputLink(0, 0);
  signalPrioritizer->AddInputLink(1, 1);
  signalPrioritizer->AddOutputLink(0, 2);
  openpass::parameter::ParameterSetLevel1 prioritizerParameter = {{"0", 1}, {"1", 0}};
  signalPrioritizer->SetModelParameter(prioritizerParameter);
  system.agentType->AddComponent(signalPrioritizer);

  auto agentUpdater = std::make_shared<core::ComponentType>("AgentUpdater", false, 1, 0, 0, 100, "AgentUpdater");
  agentUpdater->AddInputLink(0, 2);
  system.agentType->AddComponent(agentUpdater);

  return system;
}