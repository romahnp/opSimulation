/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *               2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  collisionDetector.cpp */
//-----------------------------------------------------------------------------

#include "collisionDetector.h"

#include <iostream>

#include "common/commonTools.h"
#include "common/eventTypes.h"
#include "include/agentInterface.h"
#include "include/trafficObjectInterface.h"
#include "include/worldObjectInterface.h"

CollisionDetector::CollisionDetector(WorldInterface *world, PublisherInterface *publisher)
    : world(world), publisher(publisher)
{
}

void CollisionDetector::Trigger(int time)
{
  // accumulate collisions
  auto agents = world->GetAgents();
  auto &trafficObjects = world->GetTrafficObjects();
  for (auto it = agents.cbegin(); it != agents.cend(); ++it)
  {
    AgentInterface *agent = it->second;
    assert(agent != nullptr);

    for (auto otherIt = std::next(it); otherIt != agents.cend(); ++otherIt)
    {
      AgentInterface *other = otherIt->second;
      if (!DetectCollision(other, agent))
      {
        continue;
      }
      else
      {
        // no separations given on any axis -> collision
        DetectedCollisionWithAgent(agent, other);
      }
    }

    // second loop to avoid comparing traffic objects with traffic objects
    for (auto trafficObjectIt = trafficObjects.begin(); trafficObjectIt != trafficObjects.end(); ++trafficObjectIt)
    {
      const TrafficObjectInterface *otherObject = *trafficObjectIt;
      if (!otherObject)
      {
        throw std::runtime_error("Invalid other worldObject. Collision detection cancled.");
      }
      if (!DetectCollision(otherObject, agent))
      {
        continue;
      }
      else
      {
        DetectedCollisionWithObject(agent, otherObject);
      }
    }
  }
  UpdateCollisions();
}

//! @brief Checks if the element is in vector
//! @tparam T       Typename
//! @param v        Vector of elements
//! @param element  Given element
//! @return True, if the element is in vector
template <typename T>
bool IsInVector(const std::vector<T> &v, T element)
{
  return std::find(v.begin(), v.end(), element) != v.end();
}

bool CollisionDetector::DetectCollision(const WorldObjectInterface *other, AgentInterface *agent)
{
  //Check if collision was already detected in a previous timestep
  if (IsInVector(agent->GetCollisionPartners(), std::make_pair(other->GetType(), other->GetId())))
  {
    return false;
  }

  // height check for static traffic objects
  if (const auto object = dynamic_cast<const TrafficObjectInterface *>(other); object)
  {
    if (object->GetIsCollidable() == false || agent->GetHeight() < object->GetPositionZ() - 0.5 * object->GetHeight())
    {
      return false;
    }
  }

  const auto &ownBoundingBox = agent->GetBoundingBox2D();
  const auto &otherBoundingBox = other->GetBoundingBox2D();
  const auto [netX, netY] = CommonHelper::GetCartesianNetDistance(ownBoundingBox, otherBoundingBox);
  if (netX != 0.0 || netY != 0.0)
  {
    return false;
  }

  return CommonHelper::IntersectionCalculation::GetIntersectionPoints(ownBoundingBox, otherBoundingBox).size() >= 3;
}

void CollisionDetector::DetectedCollisionWithObject(AgentInterface *agent, const WorldObjectInterface *other)
{
  const CollisionInfo collisionInfo = {false, agent->GetId(), other->GetId()};
  collisions.push_back(collisionInfo);
}

void CollisionDetector::DetectedCollisionWithAgent(AgentInterface *agent, AgentInterface *other)
{
  const CollisionInfo collisionInfo = {true, agent->GetId(), other->GetId()};
  collisions.push_back(collisionInfo);
}

void CollisionDetector::UpdateCollisions()
{
  for (const auto collision : collisions)
  {
    AgentInterface *collisionAgent = world->GetAgent(collision.collisionAgentId);

    AddCollision(collision.collisionAgentId);

    if (collision.collisionWithAgent)
    {
      AgentInterface *collisionOpponent = world->GetAgent(collision.collisionOpponentId);
      UpdateCollision(collisionAgent, collisionOpponent);
      AddCollision(collision.collisionOpponentId);

      const auto crashInfo = CalculateCrash(collisionAgent, collisionOpponent);
      PublishCrash(collision, crashInfo);
    }
    else
    {
      const auto pair = std::make_pair(ObjectTypeOSI::Object, collision.collisionOpponentId);
      for (auto partner : collisionAgent->GetCollisionPartners())
      {
        if (partner.first == ObjectTypeOSI::Vehicle)
        {
          AgentInterface *partnerAgent = world->GetAgent(partner.second);
          partnerAgent->UpdateCollision(pair);
        }
      }

      collisionAgent->UpdateCollision(pair);
      PublishCrash(collision);
    }
  }
  collisions.clear();
}

CollisionDetector::CrashInfo CollisionDetector::CalculateCrash(AgentInterface *agent, AgentInterface *opponent)
{
  //! Stores calculated dynamics for ego/host agent
  PostCrashDynamic postCrashDynamic1;
  //! Stores calculated dynamics for opponent agent
  PostCrashDynamic postCrashDynamic2;

  int timeOfFirstContact = 0;
  collisionPostCrash.CreatePostCrashDynamics(agent, opponent, &postCrashDynamic1, &postCrashDynamic2, timeOfFirstContact);

  agent->SetPostCrashVelocity({true,
                               postCrashDynamic1.GetVelocity(),
                               postCrashDynamic1.GetVelocityDirection(),
                               postCrashDynamic1.GetYawVelocity()});
  opponent->SetPostCrashVelocity({true,
                                  postCrashDynamic2.GetVelocity(),
                                  postCrashDynamic2.GetVelocityDirection(),
                                  postCrashDynamic2.GetYawVelocity()});

  return {collisionPostCrash.GetCollisionAngles(), postCrashDynamic1, postCrashDynamic2};
}

/// Creates a log entry for publishing a collision
/// \param collisionInfo  information on the collision
/// \return created log entry
openpass::publisher::LogEntry CreateLogEntry(const CollisionInfo &collisionInfo)
{
  openpass::publisher::LogEntry logEntry("Collision");
  logEntry.triggeringEntities
      = {{std::move(collisionInfo.collisionAgentId), std::move(collisionInfo.collisionOpponentId)}};
  logEntry.parameter = {{"CollisionWithAgent", collisionInfo.collisionWithAgent}};
  return logEntry;
}

void CollisionDetector::PublishCrash(const CollisionInfo &collisionInfo, const CrashInfo &crashInfo)
{
  auto logEntry = CreateLogEntry(collisionInfo);
  logEntry.parameter.insert(
      {{"Velocity", crashInfo.postCrashDynamic1.GetVelocity().value()},
       {"VelocityChange", crashInfo.postCrashDynamic1.GetVelocityChange().value()},
       {"VelocityDirection", crashInfo.postCrashDynamic1.GetVelocityDirection().value()},
       {"YawVelocity", crashInfo.postCrashDynamic1.GetYawVelocity().value()},
       {"PointOfContactLocalX", crashInfo.postCrashDynamic1.GetPointOfContactLocal().x.value()},
       {"PointOfContactLocalY", crashInfo.postCrashDynamic1.GetPointOfContactLocal().y.value()},
       {"CollisionVelocity", crashInfo.postCrashDynamic1.GetCollisionVelocity().value()},
       {"Sliding", crashInfo.postCrashDynamic1.GetSliding()},
       {"OpponentVelocity", crashInfo.postCrashDynamic2.GetVelocity().value()},
       {"OpponentVelocityChange", crashInfo.postCrashDynamic2.GetVelocityChange().value()},
       {"OpponentVelocityDirection", crashInfo.postCrashDynamic2.GetVelocityDirection().value()},
       {"OpponentYawVelocity", crashInfo.postCrashDynamic2.GetYawVelocity().value()},
       {"OpponentPointOfContactLocalX", crashInfo.postCrashDynamic2.GetPointOfContactLocal().x.value()},
       {"OpponentPointOfContactLocalY", crashInfo.postCrashDynamic2.GetPointOfContactLocal().y.value()},
       {"OpponentCollisionVelocity", crashInfo.postCrashDynamic2.GetCollisionVelocity().value()},
       {"OpponentSliding", crashInfo.postCrashDynamic2.GetSliding()},
       {"OYA", crashInfo.angles.OYA.value()},
       {"HCPAo", crashInfo.angles.HCPAo.value()},
       {"OCPAo", crashInfo.angles.OCPAo.value()},
       {"HCPA", crashInfo.angles.HCPA.value()},
       {"OCPA", crashInfo.angles.OCPA.value()}});
  publisher->Publish(EventDefinitions::utils::GetAsString(EventDefinitions::EventCategory::OpenPASS), logEntry);
}

void CollisionDetector::PublishCrash(const CollisionInfo &collisionInfo)
{
  publisher->Publish(EventDefinitions::utils::GetAsString(EventDefinitions::EventCategory::OpenPASS),
                     CreateLogEntry(collisionInfo));
}

void CollisionDetector::UpdateCollision(AgentInterface *agent, AgentInterface *opponent)
{
  if (!agent || !opponent || agent->GetId() == opponent->GetId())
  {
    return;
  }

  // check if object is already in stored collision partners
  for (const auto &[objectType, id] : agent->GetCollisionPartners())
  {
    if (id == opponent->GetId() && objectType == ObjectTypeOSI::Vehicle)
    {
      return;
    }
  }

  //Update the collision partners
  agent->UpdateCollision({opponent->GetType(), opponent->GetId()});
  opponent->UpdateCollision({agent->GetType(), agent->GetId()});

  //Recursion for agent
  for (const auto &partner : agent->GetCollisionPartners())
  {
    if (partner.first == ObjectTypeOSI::Object)
    {
      opponent->UpdateCollision({ObjectTypeOSI::Object, partner.second});
    }
    else
    {
      UpdateCollision(world->GetAgent(partner.second), opponent);
    }
  }

  //Recursion for opponent
  for (const auto &partner : opponent->GetCollisionPartners())
  {
    if (partner.first == ObjectTypeOSI::Object)
    {
      agent->UpdateCollision({ObjectTypeOSI::Object, partner.second});
    }
    else
    {
      UpdateCollision(world->GetAgent(partner.second), agent);
    }
  }
}

void CollisionDetector::AddCollision(const int agentId)
{
  if (runResult != nullptr)
  {
    runResult->AddCollisionId(agentId);
  }
}

void CollisionDetector::Initialize(RunResultInterface *runResult)
{
  this->runResult = runResult;
}
