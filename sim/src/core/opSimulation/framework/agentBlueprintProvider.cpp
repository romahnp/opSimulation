/********************************************************************************
 * Copyright (c) 2019-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "agentBlueprintProvider.h"

#include "common/log.h"
#include "defaultSystemGenerator.h"
#include "dynamicParametersSampler.h"
#include "dynamicProfileSampler.h"
#include "sampler.h"
#include "systemConfigImporter.h"

AgentBlueprintProvider::AgentBlueprintProvider(ConfigurationContainerInterface* configurationContainer,
                                               StochasticsInterface* stochastics)
    : stochastics(stochastics),
      profiles(configurationContainer->GetProfiles()),
      agentProfiles(&(configurationContainer->GetProfiles()->GetAgentProfiles())),
      systemConfigBlueprint(configurationContainer->GetSystemConfigBlueprint()),
      systemConfigs(&(configurationContainer->GetSystemConfigs()))
{
}

void AgentBlueprintProvider::Init(ConfigurationContainerInterface* configurationContainer,
                                  StochasticsInterface* stochastics)
{
  profiles = configurationContainer->GetProfiles();
  agentProfiles = &(configurationContainer->GetProfiles()->GetAgentProfiles());
  systemConfigBlueprint = configurationContainer->GetSystemConfigBlueprint();
  systemConfigs = &(configurationContainer->GetSystemConfigs());

  this->stochastics = stochastics;
}

std::string AgentBlueprintProvider::SampleVehicleModelName(const std::string& agentProfileName) const
{
  auto agentProfile = agentProfiles->at(agentProfileName);

  if (agentProfile.type == AgentProfileType::Dynamic)
  {
    auto probabilities = profiles->GetVehicleModelsProbabilities(agentProfileName);
    return Sampler::Sample(probabilities, stochastics);
  }
  else
  {
    return agentProfile.vehicleModel;
  }
}
System AgentBlueprintProvider::GetDefaultSystem() const
{
  return DefaultSystemGenerator::make();
}

System AgentBlueprintProvider::SampleSystem(const std::string& agentProfileName) const
{
  try
  {
    const auto agentProfile = agentProfiles->at(agentProfileName);
    if (agentProfile.type == AgentProfileType::Dynamic)
    {
      if (systemConfigBlueprint == nullptr)
      {
        LogErrorAndThrow("Couldn't instantiate AgentProfile:" + agentProfileName
                         + ". SystemConfigBlueprint is either missing or invalid.");
      }

      SampledProfiles sampledProfiles = SampledProfiles::make(agentProfileName, *stochastics, profiles)
                                            .SampleDriverProfile()
                                            .SampleSystemProfile()
                                            .SampleVehicleComponentProfiles();
      DynamicParameters dynamicParameters
          = DynamicParameters::make(*stochastics, sampledProfiles.systemProfileName, profiles).SampleSensorLatencies();
      AgentBuildInformation agentBuildInformation
          = AgentBuildInformation::make(sampledProfiles, dynamicParameters, systemConfigBlueprint, profiles)
                .GatherBasicComponents()
                .GatherDriverComponents()
                .GatherVehicleComponents()
                .GatherSensors();
      return GenerateDynamicAgentBlueprint(agentBuildInformation,
                                           agentProfileName,
                                           sampledProfiles.driverProfileName,
                                           sampledProfiles.vehicleComponentProfileNames);
    }
    else
    {
      return GenerateStaticAgentBlueprint(agentProfile.systemConfigFile, agentProfile.systemId, agentProfileName);
    }
  }
  catch (const std::out_of_range& e)
  {
    std::string message{"Error while sampling AgentProfile \"" + agentProfileName + "\": " + e.what()};
    LOG_INTERN(LogLevel::Error) << message;
    throw std::runtime_error(message);
  }
  catch (const std::runtime_error& e)
  {
    std::string message{"Error while sampling AgentProfile \"" + agentProfileName + "\": " + e.what()};
    LOG_INTERN(LogLevel::Error) << message;
    throw std::runtime_error(message);
  }
}

System AgentBlueprintProvider::GenerateDynamicAgentBlueprint(
    AgentBuildInformation agentBuildInformation,
    const std::string agentProfileName,
    const std::string& driverProfileName,
    const std::unordered_map<std::string, std::string>& vehicleComponentProfileNames) const
{
  System system;
  system.agentProfileName = agentProfileName;
  system.driverProfileName = driverProfileName;
  system.agentType = agentBuildInformation.agentType;
  system.sensorParameters = agentBuildInformation.sensorParameters;
  system.vehicleComponentProfileNames = vehicleComponentProfileNames;
  return system;
}

System AgentBlueprintProvider::GenerateStaticAgentBlueprint(std::string systemConfigName,
                                                            int systemId,
                                                            const std::string& agentProfileName) const
{
  System system;
  system.agentProfileName = agentProfileName;
  try
  {
    const auto& systems = systemConfigs->at(systemConfigName)->GetSystems();
    system.agentType = systems.at(systemId);
  }
  catch (const std::out_of_range& e)
  {
    const std::string message{"No system for specified id found in imported systemConfig: " + std::string(e.what())};
    LOG_INTERN(LogLevel::Error) << message;
    throw std::runtime_error(message);
  }
  return system;
}
