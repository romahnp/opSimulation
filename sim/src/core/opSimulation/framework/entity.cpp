/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "entity.h"

#include "agent.h"
#include "geometryHelper.h"
#include "scenarioControl.h"

namespace core
{
Entity::Entity(mantle_api::UniqueId id,
               const std::string &name,
               const RouteSamplerInterface *routeSampler,
               std::shared_ptr<mantle_api::EntityProperties> properties,
               AgentCategory agentCategory)
    : id(id),
      name(name),
      routeSampler(routeSampler),
      properties(properties),
      agentCategory(agentCategory),
      scenarioControl(std::make_shared<ScenarioControl>())
{
}

mantle_api::UniqueId Entity::GetUniqueId() const
{
  return id;
}

void Entity::SetName(const std::string &name)
{
  this->name = name;
}

const std::string &Entity::GetName() const
{
  return name;
}

void Entity::SetPosition(const mantle_api::Vec3<units::length::meter_t> &inert_pos)
{
  GeometryHelper geometryHelper;
  if (!agent)
  {
    auto position = geometryHelper.TranslateGlobalPositionLocally(
        inert_pos, spawnParameter.orientation, -properties->bounding_box.geometric_center);
    spawnParameter.position = position;
    spawnParameter.route = routeSampler->Sample(inert_pos, spawnParameter.orientation.yaw);
    shouldBeSpawned = true;
  }
  else
  {
    auto position = geometryHelper.TranslateGlobalPositionLocally(
        inert_pos, {agent->GetYaw(), 0_rad, 0_rad}, -properties->bounding_box.geometric_center);
    agent->SetPositionX(position.x);
    agent->SetPositionY(position.y);
  }
}

mantle_api::Vec3<units::length::meter_t> Entity::GetPosition() const
{
  GeometryHelper geometryHelper;
  if (!agent)
  {
    return geometryHelper.TranslateGlobalPositionLocally(
        spawnParameter.position, spawnParameter.orientation, properties->bounding_box.geometric_center);
  }
  else
  {
    return geometryHelper.TranslateGlobalPositionLocally({agent->GetPositionX(), agent->GetPositionY(), 0_m},
                                                         {agent->GetYaw(), 0_rad, 0_rad},
                                                         properties->bounding_box.geometric_center);
  }
}

void Entity::SetVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t> &velocity)
{
  if (!agent)
  {
    spawnParameter.velocity = velocity.Length();
  }
  else
  {
    agent->SetVelocityVector(velocity);
  }
}

mantle_api::Vec3<units::velocity::meters_per_second_t> Entity::GetVelocity() const
{
  if (!agent)
  {
    return {spawnParameter.velocity * units::math::cos(spawnParameter.orientation.yaw),
            spawnParameter.velocity * units::math::sin(spawnParameter.orientation.yaw),
            0_mps};
  }
  else
  {
    const auto velocity = agent->GetVelocity();
    return {velocity.x, velocity.y, 0_mps};
  }
}

void Entity::SetAcceleration(const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> &acceleration)
{
  if (!agent)
  {
    //spawn acceleration not implemented
  }
  else
  {
    //TODO SetAccelerationX/Y is not implemented
    const auto absAcceleration = units::math::hypot(acceleration.x, acceleration.y);
    agent->SetAcceleration(absAcceleration);
  }
}

mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> Entity::GetAcceleration() const
{
  if (!agent)
  {
    return {0_mps_sq, 0_mps_sq, 0_mps_sq};
  }
  else
  {
    const auto acceleration = agent->GetAcceleration();
    return {acceleration.x, acceleration.y, 0_mps_sq};
  }
}

void Entity::SetOrientation(const mantle_api::Orientation3<units::angle::radian_t> &orientation)
{
  if (!agent)
  {
    GeometryHelper geometryHelper;
    auto referencePosition = geometryHelper.TranslateGlobalPositionLocally(
        spawnParameter.position, spawnParameter.orientation, properties->bounding_box.geometric_center);
    auto position = geometryHelper.TranslateGlobalPositionLocally(
        referencePosition, orientation, -properties->bounding_box.geometric_center);
    spawnParameter.position = position;
    spawnParameter.orientation = orientation;
  }
  else
  {
    agent->SetYaw(orientation.yaw);
  }
}

mantle_api::Orientation3<units::angle::radian_t> Entity::GetOrientation() const
{
  if (!agent)
  {
    return spawnParameter.orientation;
  }
  else
  {
    return {agent->GetYaw(), 0_rad, 0_rad};
  }
}

void Entity::SetOrientationRate(
    const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> &orientation_rate)
{
  if (!agent)
  {
  }
  else
  {
    agent->SetYawRate(orientation_rate.yaw);
  }
}

mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> Entity::GetOrientationRate() const
{
  if (!agent)
  {
    return {0_rad_per_s, 0_rad_per_s, 0_rad_per_s};
  }
  else
  {
    return {agent->GetYawRate(), 0_rad_per_s, 0_rad_per_s};
  }
}

void Entity::SetOrientationAcceleration(
    const mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> &orientation_acceleration)
{
  if (!agent)
  {
  }
  else
  {
    agent->SetYawAcceleration(orientation_acceleration.yaw);
  }
}

mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> Entity::GetOrientationAcceleration()
    const
{
  if (!agent)
  {
    return {0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq};
  }
  else
  {
    return {agent->GetYawAcceleration(), 0_rad_per_s_sq, 0_rad_per_s_sq};
  }
}

void Entity::SetAssignedLaneIds(const std::vector<uint64_t> &assigned_lane_ids) {}

std::vector<uint64_t> Entity::GetAssignedLaneIds() const
{
  return {};
}

void Entity::SetVisibility(const mantle_api::EntityVisibilityConfig &visibility) {}

mantle_api::EntityVisibilityConfig Entity::GetVisibility() const
{
  return {};
}

void Entity::SetSystem(const System &system)
{
  this->system = system;
}

AgentBuildInstructions Entity::GetAgentBuildInstructions()
{
  return {name, agentCategory, properties, system, spawnParameter, scenarioControl};
}

void Entity::SetAgent(AgentInterface *agent)
{
  this->agent = agent;
}

std::shared_ptr<ScenarioControlInterface> const Entity::GetScenarioControl() const
{
  return scenarioControl;
}

bool Entity::ShouldBeSpawned()
{
  return shouldBeSpawned;
}

void Entity::AssignRoute(const mantle_api::RouteDefinition &routeDefinition)
{
  std::static_pointer_cast<ScenarioControl>(scenarioControl)->AddCommand(ScenarioCommand::AssignRoute{routeDefinition});
  if (!agent)
  {
    spawnParameter.route = routeSampler->CalculateRouteFromWaypoints(routeDefinition.waypoints);
    spawnParameter.routeDefinition = routeDefinition;
  }
  else
  {
    auto egoRoute = routeSampler->CalculateRouteFromWaypoints(routeDefinition.waypoints);
    agent->GetEgoAgent().SetRoadGraph(std::move(egoRoute.roadGraph), egoRoute.root, egoRoute.target);
  }
}

bool Entity::IsAgentInWorld()
{
  if (!agent)
  {
    return true;  //Don't delete if not spawned yet
  }
  else
  {
    return agent->IsAgentInWorld();
  }
}
}  // namespace core
