/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <openScenarioLib/src/common/SimpleMessageLogger.h>
#include <openScenarioLib/src/loader/FileResourceLocator.h>
#include <openScenarioLib/src/v1_1/loader/XmlScenarioLoaderFactoryV1_1.h>

#include "MantleAPI/Traffic/entity_properties.h"

using Vehicles = std::map<std::string, std::shared_ptr<const mantle_api::VehicleProperties>>;

namespace Importer
{

std::map<NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VehicleCategoryEnum, mantle_api::VehicleClass> map_vehicle_class{
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::UNKNOWN, mantle_api::VehicleClass::kOther},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::BICYCLE, mantle_api::VehicleClass::kBicycle},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::BUS, mantle_api::VehicleClass::kBus},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::CAR, mantle_api::VehicleClass::kMedium_car},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::MOTORBIKE, mantle_api::VehicleClass::kMotorbike},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::SEMITRAILER, mantle_api::VehicleClass::kSemitrailer},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::TRAILER, mantle_api::VehicleClass::kTrailer},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::TRAIN, mantle_api::VehicleClass::kTrain},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::TRAM, mantle_api::VehicleClass::kTram},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::TRUCK, mantle_api::VehicleClass::kHeavy_truck},
    {NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory::VAN, mantle_api::VehicleClass::kDelivery_van}};

//! This class represents the importer of the entity properties
class EntityPropertiesImporter
{
public:
  //! @brief Imports the entity properties from the vehicle model catalog file
  //!
  //! @param[in] filepath Path to the scenario file
  //! @return true on success
  bool Import(const std::string& filepath)
  {
    auto messageLogger
        = std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
    auto loaderFactory = NET_ASAM_OPENSCENARIO::v1_1::XmlScenarioLoaderFactory(filepath);
    auto loader = loaderFactory.CreateLoader(std::make_shared<NET_ASAM_OPENSCENARIO::FileResourceLocator>());

    auto scenarioPointer = std::static_pointer_cast<NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario>(
        loader->Load(messageLogger)->GetAdapter(typeid(NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario).name()));

    if (!(scenarioPointer && scenarioPointer->GetOpenScenarioCategory()
          && scenarioPointer->GetOpenScenarioCategory()->GetCatalogDefinition()))
    {
      throw std::runtime_error("Scenario file not found or file does not contain scenario definition.");
    }

    // TODO Verify all GetMethods to return != nullptr
    auto vehicleCatalog = scenarioPointer->GetOpenScenarioCategory()->GetCatalogDefinition()->GetCatalog();
    if (!vehicleCatalog)
    {
      throw std::runtime_error("VehiclesCatalog does not exist.");
    }
    const auto& vehicles = vehicleCatalog->GetVehicles();

    for (const auto& vehicle : vehicles)
    {
      const auto& name{vehicle->GetName()};

      mantle_api::VehicleProperties properties;
      properties.type = mantle_api::EntityType::kVehicle;
      properties.mass = units::mass::kilogram_t(vehicle->GetMass());
      properties.classification = GetVehicleClass(vehicle->GetVehicleCategory());
      properties.model = vehicle->GetModel3d();
      FillBoundingBoxProperties(properties, vehicle->GetBoundingBox(), vehicle->GetName());
      FillGenericProperties(properties, vehicle->GetProperties()->GetProperties());

      properties.performance.max_acceleration
          = units::acceleration::meters_per_second_squared_t(vehicle->GetPerformance()->GetMaxAcceleration());
      properties.performance.max_deceleration
          = units::acceleration::meters_per_second_squared_t(vehicle->GetPerformance()->GetMaxDeceleration());
      properties.performance.max_speed = units::velocity::meters_per_second_t(vehicle->GetPerformance()->GetMaxSpeed());

      FillAxleProperties(properties.front_axle, vehicle->GetAxles()->GetFrontAxle(), properties, name);
      FillAxleProperties(properties.rear_axle, vehicle->GetAxles()->GetRearAxle(), properties, name);

      vehicleProperties->insert({name, std::make_shared<const mantle_api::VehicleProperties>(properties)});
    }

    return true;
  }

  //! @brief Retrieves the vehicle properties
  //!
  //! @return the map which assigns vehicle properties to a specific vehicle type
  std::shared_ptr<Vehicles> GetVehicleProperties() { return vehicleProperties; }

  //! @brief Retrieves the mapping from the given OpenSCENARIO category to the vehicle classification
  //!
  //! @param[in] vehicle_category OpenSCENARIO category for entity object of type vehicle
  //! @return Vehicle classification
  mantle_api::VehicleClass GetVehicleClass(NET_ASAM_OPENSCENARIO::v1_1::VehicleCategory vehicle_category)
  {
    if (map_vehicle_class.find(vehicle_category.GetFromLiteral(vehicle_category.GetLiteral()))
        != map_vehicle_class.end())
    {
      return map_vehicle_class[vehicle_category.GetFromLiteral(vehicle_category.GetLiteral())];
    }
    throw std::runtime_error(std::string("No vehicle class for vehicle category " + vehicle_category.GetLiteral()));
  }

  //! @brief Fills the bounding box property of the entity with the value of the model property boundingBox from the
  //! vehicle model catalog file
  //!
  //! @param[in] properties   Entity properties to fill
  //! @param[in] bounding_box Value of model property boundingBox
  //! @param[in] name         Value of model property name
  void FillBoundingBoxProperties(mantle_api::EntityProperties& properties,
                                 std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IBoundingBox> bounding_box,
                                 const std::string& name)
  {
    if (!bounding_box)
    {
      throw std::runtime_error(std::string("Entity " + name + " without bounding box."));
    }
    auto dimensions = bounding_box->GetDimensions();
    if (!dimensions)
    {
      throw std::runtime_error(std::string("Bounding box of entity " + name + " without Dimensions."));
    }

    properties.bounding_box.dimension.length = units::length::meter_t(dimensions->GetLength());
    properties.bounding_box.dimension.width = units::length::meter_t(dimensions->GetWidth());
    properties.bounding_box.dimension.height = units::length::meter_t(dimensions->GetHeight());

    auto center = bounding_box->GetCenter();
    if (!center)
    {
      throw std::runtime_error(std::string("Bounding box of entity " + name + " without Center."));
    }

    properties.bounding_box.geometric_center.x = units::length::meter_t(center->GetX());
    properties.bounding_box.geometric_center.y = units::length::meter_t(center->GetY());
    properties.bounding_box.geometric_center.z = units::length::meter_t(center->GetZ());
  }

  //! @brief Fills the properties of the entity with the value of the model property properties from the vehicle model
  //! catalog file
  //!
  //! @param[in] entity_properties    Entity properties to fill
  //! @param[in] properties           Value of model property properties
  void FillGenericProperties(mantle_api::EntityProperties& entity_properties,
                             const std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IProperty>>& properties)
  {
    for (const auto& property : properties)
    {
      entity_properties.properties.emplace(property->GetName(), property->GetValue());
    }
  }

  //! @brief Fills the axle properties of the entity with the value of the model property axle from the vehicle model
  //! catalog file
  //!
  //! @param[in] axle                 Axle properties to fill
  //! @param[in] open_scenario_axle   value of model property axle (frontAxle or rearAxle)
  //! @param[in] properties           value of model property
  //! @param[in] name                 Value of model property name
  void FillAxleProperties(mantle_api::Axle& axle,
                          std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAxle> open_scenario_axle,
                          const mantle_api::EntityProperties& properties,
                          const std::string& name)
  {
    if (open_scenario_axle == nullptr)
    {
      throw std::runtime_error(std::string("Entity " + name + " is missing axle information."));
    }

    axle.bb_center_to_axle_center
        = {units::length::meter_t(open_scenario_axle->GetPositionX()) - properties.bounding_box.geometric_center.x,
           0.0_m,
           units::length::meter_t(open_scenario_axle->GetPositionZ()) - properties.bounding_box.geometric_center.z};
    axle.max_steering = units::angle::radian_t(open_scenario_axle->GetMaxSteering());
    axle.track_width = units::length::meter_t(open_scenario_axle->GetTrackWidth());
    axle.wheel_diameter = units::length::meter_t(open_scenario_axle->GetWheelDiameter());
  }

private:
  std::shared_ptr<Vehicles> vehicleProperties{std::make_shared<Vehicles>()};
};

}  // namespace Importer