/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include <utility>

#include "OWL/DataTypes.h"
#include "common/globalDefinitions.h"

namespace OWL::Fakes
{
class RoadMarking : public OWL::Interfaces::RoadMarking
{
public:
  MOCK_CONST_METHOD0(GetId, std::string());
  MOCK_CONST_METHOD0(GetS, units::length::meter_t());
  MOCK_CONST_METHOD1(GetSpecification, CommonTrafficSign::Entity(units::length::meter_t));
  MOCK_CONST_METHOD0(GetDimension, mantle_api::Dimension3());
  MOCK_CONST_METHOD0(GetReferencePointPosition, mantle_api::Vec3<units::length::meter_t>());
  MOCK_CONST_METHOD1(IsValidForLane, bool(OWL::Id laneId));
  MOCK_METHOD1(SetS, void(units::length::meter_t));
  MOCK_METHOD2(SetValidForLane, void(const OWL::Interfaces::Lane& lane, const RoadSignalInterface& specification));
  MOCK_METHOD2(SetValidForLane, void(const OWL::Interfaces::Lane& lane, const RoadObjectInterface& specification));
  MOCK_METHOD2(SetSpecification, bool(RoadSignalInterface*, Position position));
  MOCK_METHOD2(SetSpecification, bool(RoadObjectInterface*, Position position));
  MOCK_CONST_METHOD1(CopyToGroundTruth, void(osi3::GroundTruth&));
};
}  //namespace OWL::Fakes
