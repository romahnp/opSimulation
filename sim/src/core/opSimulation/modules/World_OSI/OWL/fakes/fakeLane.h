/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>

#include <string>

#include "OWL/DataTypes.h"
#include "WorldData.h"

namespace OWL::Fakes
{
class Lane : public OWL::Interfaces::Lane
{
public:
  MOCK_CONST_METHOD0(GetId, OWL::Id());
  MOCK_CONST_METHOD0(GetOdId, OdId());
  MOCK_CONST_METHOD0(GetLogicalLaneId, Id());
  MOCK_CONST_METHOD0(GetLaneType, LaneType());
  MOCK_CONST_METHOD0(Exists, bool());
  MOCK_CONST_METHOD0(GetSection, const OWL::Interfaces::Section&());
  MOCK_CONST_METHOD0(GetRoad, const OWL::Interfaces::Road&());
  MOCK_CONST_METHOD0(GetLaneGeometryElements, const OWL::Interfaces::LaneGeometryElements&());
  MOCK_CONST_METHOD0(GetLength, units::length::meter_t());
  MOCK_CONST_METHOD0(GetRightLaneCount, int());
  MOCK_CONST_METHOD1(GetCurvature, units::curvature::inverse_meter_t(units::length::meter_t distance));
  MOCK_CONST_METHOD1(GetWidth, units::length::meter_t(units::length::meter_t distance));
  MOCK_CONST_METHOD1(GetDirection, units::angle::radian_t(units::length::meter_t distance));
  MOCK_CONST_METHOD1(GetInterpolatedPointsAtDistance,
                     const OWL::Primitive::LaneGeometryJoint::Points(units::length::meter_t));
  MOCK_CONST_METHOD0(GetLeftLane, const OWL::Interfaces::Lane&());
  MOCK_CONST_METHOD0(GetRightLane, const OWL::Interfaces::Lane&());
  MOCK_CONST_METHOD1(GetDistance, units::length::meter_t(OWL::MeasurementPoint mp));
  MOCK_CONST_METHOD1(Covers, bool(units::length::meter_t));
  MOCK_METHOD6(AddLaneGeometryJoint,
               void(const Common::Vector2d<units::length::meter_t>& pointLeft,
                    const Common::Vector2d<units::length::meter_t>& pointCenter,
                    const Common::Vector2d<units::length::meter_t>& pointRight,
                    units::length::meter_t sOffset,
                    units::curvature::inverse_meter_t curvature,
                    units::angle::radian_t heading));
  MOCK_METHOD1(SetLaneType, void(LaneType specifiedType));
  MOCK_METHOD2(SetLeftLane, void(const OWL::Interfaces::Lane& lane, bool transferLaneBoundary));
  MOCK_METHOD2(SetRightLane, void(const OWL::Interfaces::Lane& lane, bool transferLaneBoundary));
  MOCK_CONST_METHOD1(GetWorldObjects, const OWL::Interfaces::LaneAssignments&(bool direction));
  MOCK_METHOD2(AddMovingObject, void(OWL::Interfaces::MovingObject& movingObject, const LaneOverlap& laneOverlap));
  MOCK_METHOD2(AddStationaryObject,
               void(OWL::Interfaces::StationaryObject& stationaryObject, const LaneOverlap& laneOverlap));
  MOCK_METHOD2(AddWorldObject, void(OWL::Interfaces::WorldObject& worldObject, const LaneOverlap& laneOverlap));
  MOCK_METHOD0(ClearMovingObjects, void());

  MOCK_METHOD2(AddNext, void(const OWL::Interfaces::Lane* lane, bool atBeginOfOtherLane));
  MOCK_METHOD2(AddPrevious, void(const OWL::Interfaces::Lane* lane, bool atBeginOfOtherLane));
  MOCK_METHOD0(SetLanePairings, void());
  MOCK_METHOD5(AddOverlappingLane,
               void(OWL::Id overlappingLaneId,
                    units::length::meter_t startS,
                    units::length::meter_t endS,
                    units::length::meter_t startSOther,
                    units::length::meter_t endSOther));
  MOCK_CONST_METHOD1(CopyToGroundTruth, void(osi3::GroundTruth&));
  MOCK_CONST_METHOD0(GetLeftLaneBoundaries, const std::vector<OWL::Id>());
  MOCK_CONST_METHOD0(GetRightLaneBoundaries, const std::vector<OWL::Id>());
  MOCK_CONST_METHOD0(GetLeftLogicalLaneBoundaries, const std::vector<OWL::Id>());
  MOCK_CONST_METHOD0(GetRightLogicalLaneBoundaries, const std::vector<OWL::Id>());
  MOCK_METHOD1(SetLeftLaneBoundaries, void(const std::vector<OWL::Id>& laneBoundaryIds));
  MOCK_METHOD1(SetRightLaneBoundaries, void(const std::vector<OWL::Id>& laneBoundaryIds));
  MOCK_METHOD1(SetLeftLogicalLaneBoundaries, void(const std::vector<OWL::Id>& laneBoundaryIds));
  MOCK_METHOD1(SetRightLogicalLaneBoundaries, void(const std::vector<OWL::Id>& laneBoundaryIds));
  MOCK_CONST_METHOD0(GetNext, const std::vector<OWL::Id>&());
  MOCK_CONST_METHOD0(GetPrevious, const std::vector<OWL::Id>&());
  MOCK_CONST_METHOD0(GetTrafficSigns, const OWL::Interfaces::TrafficSigns&());
  MOCK_CONST_METHOD0(GetTrafficLights, const OWL::Interfaces::TrafficLights&());
  MOCK_CONST_METHOD0(GetRoadMarkings, const OWL::Interfaces::RoadMarkings&());
  MOCK_METHOD1(AddTrafficSign, void(OWL::Interfaces::TrafficSign& trafficSign));
  MOCK_METHOD1(AddRoadMarking, void(OWL::Interfaces::RoadMarking& roadMarking));
  MOCK_METHOD1(AddTrafficLight, void(OWL::Interfaces::TrafficLight& trafficLight));
};
}  //namespace OWL::Fakes
