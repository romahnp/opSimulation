/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "WorldData.h"
#include "common/openPassTypes.h"
#include "include/worldObjectInterface.h"

/// An adapter class to the world object
class WorldObjectAdapter : public virtual WorldObjectInterface
{
public:
  const polygon_t& GetBoundingBox2D() const override;

  /// @return Returns height of the bounding box
  units::length::meter_t GetHeight() const override;

  /// @return Id of the world object
  int GetId() const override;

  /// @return Length of the world object
  units::length::meter_t GetLength() const override;

  /// @return X coordinate of the world object
  units::length::meter_t GetPositionX() const override;

  /// @return Y coordinate of the world object
  units::length::meter_t GetPositionY() const override;

  /// @return z coordinate of the world object
  units::length::meter_t GetPositionZ() const override;

  /// @return Lateral position of the world object
  units::length::meter_t GetPositionLateral() const;

  /// @return width of the world object
  units::length::meter_t GetWidth() const override;

  /// @return Yaw angle of the world object
  units::angle::radian_t GetYaw() const override;

  /// @return Roll angle of the world object
  units::angle::radian_t GetRoll() const override;

  /// @return Returns the distacnce between a reference point to the leading edge
  units::length::meter_t GetDistanceReferencePointToLeadingEdge() const override;

  /// @return Returns world object
  const OWL::Interfaces::WorldObject& GetBaseTrafficObject() const;

protected:
  OWL::Interfaces::WorldObject& baseTrafficObject;  ///< A traffic object

  units::length::meter_t s{0.0};              ///< S-coordinate of the world object
  mutable bool boundingBoxNeedsUpdate{true};  ///< True, if the bounding box of the world object needs an update

private:
  const polygon_t CalculateBoundingBox() const;

  mutable polygon_t boundingBox;

public:
  /// object is not inteded to be copied or assigned
  /// @param baseTrafficObject A traffic object
  WorldObjectAdapter(OWL::Interfaces::WorldObject& baseTrafficObject);
  WorldObjectAdapter(const WorldObjectAdapter&) = delete;
  WorldObjectAdapter(WorldObjectAdapter&&) = delete;
  WorldObjectAdapter& operator=(const WorldObjectAdapter&) = delete;
  WorldObjectAdapter& operator=(WorldObjectAdapter&&) = delete;
  virtual ~WorldObjectAdapter() = default;
};

namespace WorldObjectCommon
{

units::length::meter_t GetFrontDeltaS(units::length::meter_t length,
                                      units::length::meter_t width,
                                      units::angle::radian_t hdg,
                                      units::length::meter_t distanceReferencePointToLeadingEdge);
units::length::meter_t GetRearDeltaS(units::length::meter_t length,
                                     units::length::meter_t width,
                                     units::angle::radian_t hdg,
                                     units::length::meter_t distanceReferencePointToLeadingEdge);

}  // namespace WorldObjectCommon
