/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
//-----------------------------------------------------------------------------

#pragma once

#include <memory>

#include "WorldData.h"
#include "WorldToRoadCoordinateConverter.h"
#include "common/boostGeometryCommon.h"
#include "common/globalDefinitions.h"

namespace World
{
namespace Localization
{

using RTreeElement = std::pair<CoarseBoundingBox, const LocalizationElement*>;
using bg_rTree = boost::geometry::index::rtree<RTreeElement, boost::geometry::index::quadratic<8, 4>>;

//! This struct contains all the information about an objects position in the world, that result
//! from the Localization
class Result
{
public:
  std::map<ObjectPoint, GlobalRoadPositions> points;  //!< Points of the object in the world
  RoadIntervals touchedRoads;                         //!< Road intervals touched by the object
  bool isOnRoute{false};                              //!< The flag shows if the object is in the world located or not

  //! @return Invalid Result object
  static Result Invalid() { return Result{}; }

  Result() = default;

  //! Result constructor
  //!
  //! @param[in] points       Points of the object in the world
  //! @param[in] touchedRoads Road intervals touched by the object
  //! @param[in] isOnRoute    The flag shows if the object is in the world located or not
  Result(std::map<ObjectPoint, GlobalRoadPositions> points, RoadIntervals touchedRoads, bool isOnRoute)
      : points{points}, touchedRoads{touchedRoads}, isOnRoute{isOnRoute}
  {
  }
};

//! This struct is the result of the localization on all geometry element using an r-tree
//! It is than used to build the localization result
struct LocatedObject
{
  std::map<const OWL::Interfaces::Lane*, OWL::LaneOverlap> laneOverlaps;  ///< Overlap with all lanes
  GlobalRoadPositions referencePoint;                                     ///< Reference point in global road position
};

//! Calculates the intersection of the object boundary with an GeometryElement and stores the min and max s coordinates
//! and min and max deltas (for t) in the LocatedObject struct. Also calculates s, t and yaw of the referencePoint and
//! mainLaneLocator, if the lay on the GeometryElement
//!
//! \param worldData        OWL WorldData
//! \param objectBoundary   bounding box of the object
//! \param referencePoint   position of the reference point (x,y)
//! \param mainLaneLocator  position of the mainLaneLocator (x,y)
//! \param hdg              heading of the object
//! \param locatedObject    Output of the function is stored here
//! \return         function that the boost r-tree calls for every LaneGeometryElement, that possibly intersects with the object
//!
std::function<void(const RTreeElement&)> LocateOnGeometryElement(
    const OWL::Interfaces::WorldData& worldData,
    const std::vector<Common::Vector2d<units::length::meter_t>>& objectBoundary,
    const Common::Vector2d<units::length::meter_t>& referencePoint,
    const units::angle::radian_t& hdg,
    LocatedObject& locatedObject);

//! Calculates the coordinates of the point on the GeometryElement and stores the result in the map
//!
//! \param worldData        OWL WorldData
//! \param point            point to locate
//! \param hdg              heading of the object
//! \param result    Output of the function is stored here
//! \return         function that the boost r-tree calls for every LaneGeometryElement, that possibly intersects with the object
std::function<void(const RTreeElement&)> LocateOnGeometryElement(const OWL::Interfaces::WorldData& worldData,
                                                                 const Common::Vector2d<units::length::meter_t>& point,
                                                                 const units::angle::radian_t& hdg,
                                                                 GlobalRoadPositions& result);

polygon_t GetBoundingBox(units::length::meter_t x,
                         units::length::meter_t y,
                         units::length::meter_t length,
                         units::length::meter_t width,
                         units::angle::radian_t rotation,
                         units::length::meter_t center);

//! Assigns this object to all lanes it was located on, so that the result of Lane::GetWorldObjects
//! contains this object
void CreateLaneAssignments(OWL::Interfaces::WorldObject& object, const LocatedObject& locatedObject);

/// Localizer
class Localizer
{
public:
  //! Localizer constructor
  //!
  //! @param[in] worldData    OWL WorldData
  Localizer(const OWL::Interfaces::WorldData& worldData);

  //! Initialize elements and rTree
  void Init();

  /// @brief Locates a object within the underlying road network
  /// @param boundingBox  bounding box of the object
  /// @param object       object to locate
  /// @return location of the object in the road network
  Result Locate(const polygon_t& boundingBox, OWL::Interfaces::WorldObject& object) const;

  /// @brief Locates a point within the underlying road network
  /// @param point  point to locate
  /// @param hdg    heading in world coordinates
  /// @return road positions (could be multiple, e.g. on junctions)
  GlobalRoadPositions Locate(const Common::Vector2d<units::length::meter_t>& point,
                             const units::angle::radian_t& hdg) const;

  /// @brief Removes the saved location of a object from the world (e.g. lane assignments)
  /// @param object object to unlocate
  void Unlocate(OWL::Interfaces::WorldObject& object) const;

private:
  //!
  //! \brief Create the locate result
  //! \param locatedObject  located positions of object on all lanes
  //! \param route          route of the object
  //! \return
  //!
  Result BuildResult(const LocatedObject& locatedObject) const;

  const OWL::Interfaces::WorldData& worldData;
  std::vector<LocalizationElement> elements{};
  bg_rTree rTree;
};

}  //namespace Localization
}  //namespace World
