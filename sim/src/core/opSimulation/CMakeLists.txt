################################################################################
# Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_NAME SimulationCore)

add_subdirectory(modules)

find_package(OpenScenarioEngine REQUIRED)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE shared COMPONENT common

  HEADERS
    opSimulation.h
    bindings/dataBuffer.h
    bindings/dataBufferBinding.h
    bindings/dataBufferLibrary.h
    bindings/modelBinding.h
    bindings/modelLibrary.h
    bindings/observationBinding.h
    bindings/observationLibrary.h
    bindings/spawnPointBinding.h
    bindings/spawnPointLibrary.h
    bindings/stochastics.h
    bindings/stochasticsBinding.h
    bindings/stochasticsLibrary.h
    bindings/world.h
    bindings/worldBinding.h
    bindings/worldLibrary.h
    framework/agentBlueprintProvider.h
    framework/agentDataPublisher.h
    framework/agentFactory.h
    framework/collisionDetector.h
    framework/commandLineParser.h
    framework/configurationContainer.h
    framework/controllerRepository.h
    framework/directories.h
    framework/defaultSystemGenerator.h
    framework/dynamicAgentTypeGenerator.h
    framework/dynamicParametersSampler.h
    framework/dynamicProfileSampler.h
    framework/entity.h
    framework/entityRepository.h
    framework/controllerRepository.h
    framework/environment.h
    framework/frameworkModuleContainer.h
    framework/geometryHelper.h
    framework/idManager.h
    framework/laneLocationQueryService.h
    framework/observationModule.h
    framework/observationNetwork.h
    framework/parameterbuilder.h
    framework/pedestrian.h
    framework/routeSampler.h
    framework/runInstantiator.h
    framework/sampler.h
    framework/trafficSwarmService.h
    framework/vehicle.h
    framework/scheduler/agentParser.h
    framework/scheduler/runResult.h
    framework/scheduler/scheduler.h
    framework/scheduler/schedulerTasks.h
    framework/scheduler/taskBuilder.h
    framework/scheduler/tasks.h
    framework/srcCollisionPostCrash/collisionDetection_Impact_implementation.h
    framework/srcCollisionPostCrash/polygon.h
    importer/importerCommon.h
    importer/configurationFiles.h
    importer/connection.h
    importer/csvParser.h
    importer/frameworkModules.h
    importer/importerLoggingHelper.h
    importer/junction.h
    importer/parameterImporter.h
    importer/profiles.h
    importer/profilesImporter.h
    importer/road.h
    importer/scenery.h
    importer/sceneryImporter.h
    importer/simulationConfig.h
    importer/simulationConfigImporter.h
    importer/systemConfig.h
    importer/systemConfigImporter.h
    importer/road/roadObject.h
    importer/road/roadSignal.h
    modelElements/agent.h
    modelElements/agentType.h
    modelElements/channel.h
    modelElements/channelBuffer.h
    modelElements/component.h
    modelElements/componentType.h
    modelElements/parameters.h
    modelElements/scenarioControl.h
    modelElements/spawnItemParameter.h
    modelElements/spawnPoint.h
    modelElements/spawnPointNetwork.h
    ../common/log.h
    ../common/coreDataPublisher.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/Execution/i_environment.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/EnvironmentalConditions/road_condition.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/EnvironmentalConditions/weather.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/Map/i_coord_converter.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/Map/i_lane_location_query_service.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/Traffic/i_entity_repository.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/Traffic/entity_properties.h
    ${MantleAPI_INCLUDE_DIRS}/MantleAPI/Common/position.h
    ${units_INCLUDE_DIR}/units.h

  SOURCES
    bindings/dataBufferBinding.cpp
    bindings/dataBufferLibrary.cpp
    bindings/modelBinding.cpp
    bindings/modelLibrary.cpp
    bindings/observationBinding.cpp
    bindings/observationLibrary.cpp
    bindings/spawnPointBinding.cpp
    bindings/spawnPointLibrary.cpp
    bindings/stochasticsBinding.cpp
    bindings/stochasticsLibrary.cpp
    bindings/worldBinding.cpp
    bindings/worldLibrary.cpp
    framework/agentBlueprintProvider.cpp
    framework/agentDataPublisher.cpp
    framework/agentFactory.cpp
    framework/collisionDetector.cpp
    framework/commandLineParser.cpp
    framework/configurationContainer.cpp
    framework/controllerRepository.cpp
    framework/directories.cpp
    framework/defaultSystemGenerator.cpp
    framework/dynamicAgentTypeGenerator.cpp
    framework/dynamicParametersSampler.cpp
    framework/dynamicProfileSampler.cpp
    framework/entity.cpp
    framework/entityRepository.cpp
    framework/controllerRepository.cpp
    framework/environment.cpp
    framework/frameworkModuleContainer.cpp
    framework/geometryHelper.cpp
    framework/idManager.cpp
    framework/laneLocationQueryService.cpp
    framework/observationModule.cpp
    framework/observationNetwork.cpp
    framework/pedestrian.cpp
    framework/runInstantiator.cpp
    framework/sampler.cpp
    framework/vehicle.cpp
    framework/scheduler/agentParser.cpp
    framework/scheduler/runResult.cpp
    framework/scheduler/scheduler.cpp
    framework/scheduler/schedulerTasks.cpp
    framework/scheduler/taskBuilder.cpp
    framework/scheduler/tasks.cpp
    framework/srcCollisionPostCrash/collisionDetection_Impact_implementation.cpp
    framework/srcCollisionPostCrash/polygon.cpp
    importer/importerCommon.cpp
    importer/connection.cpp
    importer/csvParser.cpp
    importer/junction.cpp
    importer/parameterImporter.cpp
    importer/profiles.cpp
    importer/profilesImporter.cpp
    importer/road.cpp
    importer/scenery.cpp
    importer/sceneryImporter.cpp
    importer/simulationConfig.cpp
    importer/simulationConfigImporter.cpp
    importer/systemConfig.cpp
    importer/systemConfigImporter.cpp
    importer/road/roadObject.cpp
    importer/road/roadSignal.cpp
    modelElements/agent.cpp
    modelElements/agentType.cpp
    modelElements/channel.cpp
    modelElements/component.cpp
    modelElements/componentType.cpp
    modelElements/dataBuffer.cpp
    modelElements/parameters.cpp
    modelElements/scenarioControl.cpp
    modelElements/spawnPointNetwork.cpp
    ../common/log.cpp
    ../common/coreDataPublisher.cpp
  INCDIRS
    .
    ../..
    ../../../..
    ${MantleAPI_INCLUDE_DIRS}
    ${units_INCLUDE_DIR}
    ../common
    framework
    importer
    modelElements
    modules

  LIBRARIES
    Qt5::Core
    Qt5::Xml
    Qt5::XmlPatterns
    Boost::headers
    Common
    CoreCommon
    OpenScenarioEngine::OpenScenarioEngine
)

target_link_libraries( ${COMPONENT_NAME} ${XOSC_LIB} ${ANTLR4_LIB} ${EXP_LIB} )

set(COMPONENT_NAME opSimulation)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE executable COMPONENT bin

  HEADERS

  SOURCES
    framework/main.cpp

  LIBRARIES
    SimulationCore

  INCDIRS
    .
    ../../../..
    ${MantleAPI_INCLUDE_DIRS}
    ${units_INCLUDE_DIR}
    ../common
    ../../common
    bindings
    framework
    importer
    modelElements
    modules
)

