/********************************************************************************
 * Copyright (c) 2017-2018 ITK Engineering GmbH
 *               2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  simulationConfig.h
//! @brief This file contains the representation of the simulation
//!        configuration.
//-----------------------------------------------------------------------------

#pragma once

#include <optional>
#include <string>

namespace SimulationManager
{
namespace Configuration
{

/** @brief Container class for the simulation configuration
 * 	@details This class stores all information provided by the simulationConfig.xml
 *            and provides access to this information
 */
class SimulationConfig
{
public:
  /// @brief SimulationConfig constructor
  /// @param logFile Log file
  /// @param configs Configs file
  /// @param results Results of the simulation
  SimulationConfig(std::optional<std::string> logFile,
                   std::optional<std::string> configs,
                   std::optional<std::string> results)
      : logFile{logFile.value_or(defaultLogFile)},
        configs{configs.value_or(defaultConfigs)},
        results{results.value_or(defaultResults)}
  {
  }

  const std::string logFile;  ///< Log file
  const std::string configs;  ///< Configs file
  const std::string results;  ///< Results of the simulation

private:
  static constexpr char defaultLogFile[] = "opSimulation.log";
  static constexpr char defaultConfigs[] = "configs";
  static constexpr char defaultResults[] = "results";
};

}  // namespace Configuration
}  // namespace SimulationManager
