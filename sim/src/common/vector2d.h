/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2021 ITK Engineering GmbH
 *               2018-2020 in-tech GmbH
 *               2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! \brief This file implements a generic 2D vector.

#pragma once

#include <cmath>
#include <ostream>

#include "MantleAPI/Common/floating_point_helper.h"
#include "common/hypot.h"
#include "common/opExport.h"
#include "units.h"

namespace Common
{



/*!
 * class for 2d vectors in cartesian coordinate system
 */
template <typename T, class = typename std::enable_if<units::traits::is_unit<T>::value>>
class OPENPASSCOMMONEXPORT Vector2d final
{
public:
  static T constexpr EPSILON{1e-9};  //!< set epsilon

  /// x dimension
  T x;
  /// y dimension
  T y;

  Vector2d(const Vector2d &) = default;  ///< copy constructor
  Vector2d(Vector2d &&) = default;       ///< move constructor

  /// @return copy assignment operator
  Vector2d &operator=(const Vector2d &) = default;

  /// @return move assignment operator
  Vector2d &operator=(Vector2d &&) = default;

  /*!
   * create 2d vector from pair (x,y)
   *
   * \param[in] x     x-value
   * \param[in] y     y-value
   */
  constexpr Vector2d(T x = T(0), T y = T(0)) noexcept : x(x), y(y) {}

  /*!
   * translation of vector
   *
   * \param[in] x    x-value of displacement vector
   * \param[in] y    y-value of displacement vector
   */
  constexpr void Translate(T x, T y) noexcept
  {
    this->x += x;
    this->y += y;
  }

  /*!
   * \brief Translate
   * translation of vector via another vector
   * \param[in] translationVector vector of translation
   */
  constexpr void Translate(Vector2d<T> translationVector) noexcept
  {
    this->x += translationVector.x;
    this->y += translationVector.y;
  }

  /*!
   * rotates vector by angle
   *
   * \param[in] angle     angle, in radians
   */
  void Rotate(units::angle::radian_t angle) noexcept
  {
    auto cosAngle = units::math::cos(angle);
    auto sinAngle = units::math::sin(angle);
    *this = Vector2d(x * cosAngle - y * sinAngle, x * sinAngle + y * cosAngle);
  }

  /*!
   * scales vector by a factor
   *
   * \param[in] scale     scaling factor
   */
  constexpr void Scale(units::dimensionless::scalar_t scale) noexcept
  {
    x *= scale;
    y *= scale;
  }

  /*!
   * adds a vector
   *
   * \param[in] in     added 2d vector
   */
  constexpr void Add(const Vector2d<T> &in) noexcept
  {
    x += in.x;
    y += in.y;
  }

  /*!
   * subtracts a vector
   *
   * \param[in] in     subtracted 2d vector
   */
  constexpr void Sub(const Vector2d<T> &in) noexcept
  {
    x -= in.x;
    y -= in.y;
  }

  /*!
   * scalar product / dot product
   *
   * \param[in] in      2d vector
   * \return returns dot product of the 2 vectors
   */
  constexpr auto Dot(const Vector2d<T> &in) const noexcept { return x * in.x + y * in.y; }

  /*!
   * cross product with Z=0
   *
   * \param[in] in      2d vector
   * \return returns z-component of the cross product
   */
  template <typename Y, class = typename std::enable_if<units::traits::is_unit<T>::value>>
  constexpr auto Cross(const Vector2d<Y> &in) const noexcept
  {
    return x * in.y - y * in.x;
  }

  //! @brief Method which checks whether the vector has a length
  //!
  //! @return true if the vector has no length
  bool HasNoLength() { return units::math::abs(Length()) < EPSILON; }

  /*!
   * Normalizes the 2d vector
   *
   * Each component of the vector is devided by the length of the vector.
   *
   * In case of a vector with length 0, the vector cannot be normalized and false is returned.
   *
   * \return returns true if vector could be normalized, false otherwise
   */
  Vector2d<units::dimensionless::scalar_t> Norm()
  {
    auto length = Length();

    if (HasNoLength())
    {
      throw std::runtime_error("Can't normalize Vector2d with length of 0.0");
    }

    return Vector2d<units::dimensionless::scalar_t>(x / length, y / length);
  }

  /*!
   * returns length of the vector
   *
   * \return length of the vector
   */
  T Length() const noexcept { return openpass::hypot(x, y); }

  /*!
   * \brief Angle
   * returns the angle of the vector [-pi,+pi]
   * \return angle of vector
   */
  units::angle::radian_t Angle() const noexcept { return units::math::atan2(y, x); }

  /*!
   * returns the length of the projection of the vector onto a axis
   *
   * \param[in] yaw    yaw of the projection axis
   * \return the length of the projection of the vector onto a axis
   */
  T Projection(units::angle::radian_t yaw) const { return x * units::math::cos(yaw) + y * units::math::sin(yaw); }

  /*!
   * returns the difference of two 2d vectors
   *
   * \param[in] in    a 2d vector
   * \return returns the difference of two 2d vectors
   */
  constexpr Vector2d<T> operator-(const Vector2d<T> &in) const noexcept { return Vector2d<T>(x - in.x, y - in.y); }

  /*!
   * returns the sum of two 2d vectors
   *
   * \param[in] in    a 2d vector
   * \return returns the sum of two 2d vectors
   */
  constexpr Vector2d<T> operator+(const Vector2d<T> &in) const noexcept { return Vector2d<T>(x + in.x, y + in.y); }

  /*!
   * returns the multiplication of two 2d vectors
   *
   * \param[in] in    a 2d vector
   * \return returns the multiplication of two 2d vectors
   */
  constexpr Vector2d<T> operator*(double in) const noexcept { return Vector2d<T>(x * in, y * in); }

  /*!
   * \brief Comparison operator taking EPSILON of 1e-9 into account
   *
   * \param[in]    in    Vector to compare to
   *
   * \return   true if vectors are considered equal, false otherwise
   */
  constexpr bool operator==(const Vector2d<T> &in) const noexcept
  {
    return (mantle_api::AlmostEqual(x, in.x, EPSILON) && mantle_api::AlmostEqual(y, in.y, EPSILON));
  }

  /*!
   * \brief Inequality operator taking EPSILON of 1e-9 into account
   *
   * \param[in]    in    Vector to compare to
   *
   * \return   false if vectors are considered equal, true otherwise
   */
  constexpr bool operator!=(const Vector2d<T> &in) const noexcept { return !operator==(in); }

  /// @brief Overload << operator for Vector2d
  /// @param os     the output stream
  /// @param vector the vector
  friend std::ostream &operator<<(std::ostream &os, const Vector2d<T> &vector)
  {
    return os << "(" << vector.x << ", " << vector.y << ")";
  }
};

/// line
template <typename T, class = typename std::enable_if<units::traits::is_unit<T>::value>>
struct Line
{
  /// @brief constructor for line
  /// @param startPoint start point of the line in 2d
  /// @param endPoint   end point of the line in 2d
  explicit constexpr Line(const Common::Vector2d<T> &startPoint, const Common::Vector2d<T> &endPoint) noexcept
      : startPoint{startPoint}, directionalVector{endPoint - startPoint}
  {
  }

  Common::Vector2d<T> startPoint;         //!< start point of the line in 2d
  Common::Vector2d<T> directionalVector;  //!< directional vector of the line in 2d
};

}  // namespace Common
