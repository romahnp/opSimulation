/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <map>
#include <unordered_map>

#include "include/modelInterface.h"

//! Class representing implementation of controller switch
class ControllerSwitchImplementation : public UnrestrictedControllStrategyModelInterface
{
public:
  //! Name of this component
  const std::string COMPONENTNAME = "SignalPrioritizer";

  //! @brief Construct a new Controller Switch Implementation object
  //!
  //! @param[in]     componentName     Name of the component
  //! @param[in]     isInit            Corresponds to "init" of "Component"
  //! @param[in]     priority          Corresponds to "priority" of "Component"
  //! @param[in]     offsetTime        Corresponds to "offsetTime" of "Component"
  //! @param[in]     responseTime      Corresponds to "responseTime" of "Component"
  //! @param[in]     cycleTime         Corresponds to "cycleTime" of "Component"
  //! @param[in]     stochastics       Pointer to the stochastics class loaded by the framework
  //! @param[in]     world             Pointer to the world
  //! @param[in]     parameters        Pointer to the parameters of the module
  //! @param[in]     publisher         Pointer to the publisher instance
  //! @param[in]     callbacks         Pointer to the callbacks
  //! @param[in]     agent             Pointer to agent instance
  //! @param[in]     scenarioControl   scenarioControl of entity
  ControllerSwitchImplementation(std::string componentName,
                                 bool isInit,
                                 int priority,
                                 int offsetTime,
                                 int responseTime,
                                 int cycleTime,
                                 StochasticsInterface *stochastics,
                                 WorldInterface *world,
                                 const ParameterInterface *parameters,
                                 PublisherInterface *const publisher,
                                 const CallbackInterface *callbacks,
                                 AgentInterface *agent,
                                 std::shared_ptr<ScenarioControlInterface> const scenarioControl);

  //! @brief Holds the signal if there are other with higher priority.
  //!
  //! @param[in]    localLinkId Corresponds to "id" of "ComponentInput"
  //! @param[in]    signal      Signal.
  //! @param[in]    time        Timestamp in milliseconds.
  void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &signal, int time) override;

  //! @brief Changes the signal to a signal on hold if there is still on on hold in the current time step.
  //!
  //! @param[in]    localLinkId Corresponds to "id" of "ComponentInput"
  //! @param[out]   signal      Signal.
  //! @param[in]    time        Timestamp in milliseconds.
  void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &signal, int time) override;

  void Trigger(int) override {}

private:
  std::shared_ptr<SignalInterface const> defaultControllerSignal;
  std::shared_ptr<SignalInterface const> customControllerSignal;
};
