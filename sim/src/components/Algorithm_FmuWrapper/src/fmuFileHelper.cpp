/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "fmuFileHelper.h"

#include <chrono>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <optional>
#include <osi3/osi_sensordata.pb.h>
#include <sstream>

constexpr double EPSILON = 0.001;

std::filesystem::path FmuFileHelper::CreateOrOpenOutputFolder(const std::filesystem::path& outputDir,
                                                              const std::string& componentName,
                                                              std::optional<const std::string> appendedFolder)
{
  std::filesystem::path outputDirForComponent = outputDir;
  if (outputDir.string().find(componentName) == std::string::npos)
  {
    outputDirForComponent /= componentName;  //only append component folder if the path is not describing it
  }

  if (appendedFolder.has_value() && (outputDirForComponent.string().find(appendedFolder.value()) == std::string::npos))
  {
    outputDirForComponent /= appendedFolder.value();
  }

  if (!std::filesystem::exists(outputDirForComponent))
  {
    std::filesystem::create_directories(outputDirForComponent);
  }
  return outputDirForComponent;
}

std::string FmuFileHelper::GenerateTraceFileName(
    const std::string outputType, const std::pair<const std::string, FmuFileHelper::TraceEntry>& fileToOutputTrace)
{
  std::stringstream fileName;

  // timestamp
  const auto clockNow = std::chrono::system_clock::now();
  const auto t_clockNow = std::chrono::system_clock::to_time_t(clockNow);
  tm* tm_clockNow = gmtime(&t_clockNow);

  fileName << std::put_time(tm_clockNow, "%Y%m%d");
  fileName << "T";
  fileName << std::put_time(tm_clockNow, "%H%M%S");
  fileName << "Z";

  // osi type
  fileName << "_";
  fileName << fileToOutputTrace.second.osiType;

  // Osi version
  fileName << "_";
  const auto currentInterfaceVersion
      = osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version);
  fileName << currentInterfaceVersion.version_major();
  fileName << currentInterfaceVersion.version_minor();
  fileName << currentInterfaceVersion.version_patch();

  // protobuff version
  fileName << "_";
  fileName << GOOGLE_PROTOBUF_VERSION;

  // number of frames
  fileName << "_";
  fileName << fileToOutputTrace.second.time / 10;

  fileName << "_";
  fileName << outputType;
  fileName << ".osi";

  return fileName.str();
}

void FmuFileHelper::WriteBinaryTrace(const std::string& message,
                                     const std::string& fileName,
                                     const std::string& componentName,
                                     int time,
                                     std::string osiType,
                                     std::map<std::string, FmuFileHelper::TraceEntry>& targetOutputTracesMap)
{
  std::string outputTraceKey = componentName + "_" + fileName;
  targetOutputTracesMap[outputTraceKey].message = message;
  targetOutputTracesMap[outputTraceKey].time = time;
  targetOutputTracesMap[outputTraceKey].osiType = osiType;
}

void FmuFileHelper::WriteTracesToFile(const std::filesystem::path& outputDir,
                                      const std::map<std::string, FmuFileHelper::TraceEntry>& fileToOutputTracesMap)
{
  for (const auto& file_trace_element : fileToOutputTracesMap)
  {
    size_t separator = file_trace_element.first.find("_");
    std::string outputType = file_trace_element.first.substr(separator + 1, file_trace_element.first.length());
    std::string componentName = file_trace_element.first.substr(0, separator);
    const auto currentInterfaceVersion
        = osi3::InterfaceVersion::descriptor()->file()->options().GetExtension(osi3::current_interface_version);

    std::filesystem::path dir = CreateOrOpenOutputFolder(outputDir, componentName, "BinaryTraceFiles");

    std::string filename = GenerateTraceFileName(outputType, file_trace_element);

    std::ofstream file{};
    std::string path = (dir / filename).string();
    if (!file.is_open()) file.open(path, std::ios::out | std::ios::trunc);
    file << file_trace_element.second.message;
    file.close();
  }
}

void FmuFileHelper::WriteJson(const google::protobuf::Message& message,
                              const std::string& fileName,
                              const std::filesystem::path& outputDir)
{
  std::filesystem::path filePath{outputDir / fileName};
  std::fstream file{filePath.string(), std::ios::out};
  std::string outputString;
  google::protobuf::util::JsonPrintOptions options;
  options.add_whitespace = true;
  google::protobuf::util::MessageToJsonString(message, &outputString, options);
  file.write(outputString.data(), outputString.size());
  file.close();
}

std::filesystem::path FmuFileHelper::temporaryDirectoryName()
{
  return tmpnam(nullptr);
}

std::string FmuFileHelper::CreateAgentIdString(int agentId)
{
  std::stringstream ss;
  ss << std::setw(4) << std::setfill('0');
  ss << agentId;
  return ss.str();
}

std::filesystem::path FmuFileHelper::CreateOutputDir(const std::filesystem::path& fmuName,
                                                     int agentId,
                                                     const std::filesystem::path& runtimeOutputDir)
{
  return runtimeOutputDir / "FmuWrapper" / (std::string("Agent") + CreateAgentIdString(agentId)) / fmuName;
}
