/*
    Copyright (C) 2012 Modelon AB <http://www.modelon.com>
                  2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)


        You should have received a copy of the LICENCE-FMUChecker.txt
    along with this program. If not, contact Modelon AB.
*/
/**
        \file fmuChecker.h
        Header file for fmuChecker application.
*/

#ifndef FMUCHECKER_H_
#define FMUCHECKER_H_

#include <JM/jm_portability.h>
#include <fmilib.h>
#include <stdio.h>

/** string constant used for logging. */
extern const char* fmu_checker_module;

/// @brief Common exit routine
/// @param code
void do_exit(int code);

/** Global variable used for checking alloc/free consistency */
extern int allocated_mem_blocks;

/// @brief calloc proxy
/// @param nobj
/// @param size
/// @return
void* check_calloc(size_t nobj, size_t size);

/// @brief free proxy
/// @param obj object
void check_free(void* obj);

/** Print information on command line options */
void print_usage();

#define MAX_URL_LENGTH 10000  ///< maximum length of the url

/// @brief convert wide character type to string
/// @param s 	string
/// @param wcs 	wide character type
/// @return status
int wcstostr(char* s, const wchar_t* wcs);

/**  Checker data structure is used to pass information between different routines */
struct fmu_check_data_t
{
  /** FMU file */
  const char* FMUPath;
  /** Temporary directory with unique name where FMU is unpacked */
  char* tmpPath;

  /** Same as tmpPath unless specified by an option */
  char unzipPathBuf[10000];
  const char* unzipPath;  ///< Path to unzip

  /** Directory to be used for temporary files. Either user specified or system-wide*/
  const char* temp_dir;

  /** Counter for the warnings */
  unsigned int num_warnings;

  /** counter for the non-fatal errors */
  unsigned int num_errors;

  /** counter for the non-fatal errors */
  unsigned int num_fatal;

  /** counter for the non-info messages from FMU */
  unsigned int num_fmu_messages;

  /** A flag that makes instance name error appear only once */
  int printed_instance_name_error_flg;

  /** FMIL callbacks*/
  jm_callbacks callbacks;
  /** FMIL context */
  fmi_import_context_t* context;

  /** Model information */
  const char* modelIdentifierFMI1;    ///< FMI model information
  const char* modelIdentifierME;      ///< ME model information
  const char* modelIdentifierCS;      ///< CS model information
  const char* modelName;              ///< Model name
  const char* GUID;                   ///< GUID
  const char* instanceNameSavedPtr;   ///< Pointer of an instance name
  const char* instanceNameToCompare;  ///< Compare an instance name

  /** Simulation stop time */
  double stopTime;
  /** Step size for the simulation*/
  double stepSize;
  /** Flag indicating if step size is user defined in command line */
  int stepSizeSetByUser;

#define DEFAULT_MAX_OUTPUT_PTS 500  ///< Checker data structure is used to pass information between different routines
#define DEFAULT_MAX_OUTPUT_PTS_STR \
  "500"  ///< Checker data structure is used to pass information between different routines

  /** Maximum number of steps to save to output */
  size_t maxOutputPts;
  /** Flag indicating if max number of steps is user defined in command line */
  int maxOutputPtsSetByUser;
  /** Next output time */
  double nextOutputTime;
  /** Next output step number*/
  double nextOutputStep;
  /** separator character to use */
  char CSV_separator;

/** this feature is currently off */
#undef SUPPORT_out_enum_as_int_flag
#ifdef SUPPORT_out_enum_as_int_flag
  /** print enums and bools as integers */
  char out_enum_as_int_flag;
#endif

  int write_output_files;  //!< Flag activates generation of Output files when 1
                           /** Name of the output file (NULL is stdout)*/
  const char* output_file_name;
  /** Output file stream */
  FILE* out_file;

  int write_log_files;  //!< Flag activates generation of log files when 1
                        /** Name of the log file (NULL is stderr)*/
  const char* log_file_name;
  /** Log file stream */
  FILE* log_file;

  /** input data file name */
  char* inputFileName;

  /** Should simulation be done (or only XML checking) */
  int do_simulate_flg;

  /** Should we simulate ME if available? */
  int do_test_me;

  /** Should we simulate CS if available? */
  int do_test_cs;

  /** Should we require ME to be available? */
  int require_me;

  /** Should we require CS to be available? */
  int require_cs;

  /** should variable names be mangled to avoid quoting (-m switch) */
  int do_mangle_var_names;

  /** should all variables be printed (-f switch) */
  int do_output_all_vars;

  /** should variables be printed before event handling (-d switch) */
  int print_all_event_vars;

  /** FMI standard version of the FMU */
  fmi_version_enu_t version;

  /** FMI1 main struct */
  fmi1_import_t* fmu1;
  /** Kind of the FMI */
  fmi1_fmu_kind_enu_t fmu1_kind;
  /** model variables */
  fmi1_import_variable_list_t* vl;

  /** FMI2 main struct */
  fmi2_import_t* fmu2;
  /** Kind of the FMI */
  fmi2_fmu_kind_enu_t fmu2_kind;
  /** model variables */
  fmi2_import_variable_list_t* vl2;

  int simulation_initialized;  //!< Flag indicating simulation was initialized
};

typedef struct fmu_check_data_t fmu_check_data_t;  ///< fmu checker data

/** Global pointer is necessary to support FMI 1.0 logging */
extern fmu_check_data_t* cdata_global_ptr;

/// @brief  parse command line options and set fields in cdata accordingly
/// @param cdata
/// @return
int parse_options(fmu_check_data_t* cdata);

/// @brief nit checker data with defaults
/// @param cdata
void init_fmu_check_data(fmu_check_data_t* cdata);

/// @brief Release allocated resources
/// @param cdata
/// @param close_log
void clear_fmu_check_data(fmu_check_data_t* cdata, int close_log);

/// @brief Logger function for FMI library
/// @param c
/// @param module
/// @param log_level
/// @param message
void checker_logger(jm_callbacks* c, jm_string module, jm_log_level_enu_t log_level, jm_string message);

/// @brief Check an FMI 1.0 FMU
/// @param cdata
/// @return
jm_status_enu_t fmi1_check(fmu_check_data_t* cdata);

/// @brief Check if the fmi status is ok or warning
/// @param fmistatus
/// @return
static int fmi1_status_ok_or_warning(fmi1_status_t fmistatus)
{
  return (fmistatus == fmi1_status_ok) || (fmistatus == fmi1_status_warning);
}

/// @brief Print the string in double quotes replacing any occurrence of '"' in the string with \'
/// @param cdata
/// @param str
/// @return
jm_status_enu_t checked_print_quoted_str(fmu_check_data_t* cdata, const char* str);

/// @brief Write out the data into the output file
/// @param cdata
/// @param fmt
/// @param  ...
/// @return
jm_status_enu_t checked_fprintf(fmu_check_data_t* cdata, const char* fmt, ...);

/// @brief Write out separator and variable name. Variable name is quoted/mangled if needed
/// @param cdata
/// @param vn
/// @return
jm_status_enu_t check_fprintf_var_name(fmu_check_data_t* cdata, const char* vn);

/// @brief Simulate an FMI 1.0 ME FMU
/// @param cdata
/// @return
jm_status_enu_t fmi1_me_simulate(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 1.0 CS FMU
/// @param cdata
/// @return
jm_status_enu_t fmi1_cs_prep_init(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 1.0 CS PREP FMU
/// @param cdata
/// @return
jm_status_enu_t fmi1_cs_prep_simulate(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 1.0 CS FMU step
/// @param cdata
/// @param tcur
/// @return
jm_status_enu_t fmi1_cs_simulate_step(fmu_check_data_t* cdata, fmi1_real_t tcur);

/// @brief Write an FMI 1.0 header
/// @param cdata
/// @return
jm_status_enu_t fmi1_write_csv_header(fmu_check_data_t* cdata);

/// @brief Write an FMI 1.0 data
/// @param cdata
/// @param time
/// @return
jm_status_enu_t fmi1_write_csv_data(fmu_check_data_t* cdata, double time);

/// @brief FMI 1.0 end handling
/// @param cdata
/// @return
jm_status_enu_t fmi1_end_handling(fmu_check_data_t* cdata);

/// @brief Check an FMI 2.0 FMU
/// @param cdata
/// @return
jm_status_enu_t fmi2_check(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 2.0 ME FMU
/// @param cdata
/// @return
jm_status_enu_t fmi2_me_simulate(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 2.0 CS FMU
/// @param cdata
/// @return
jm_status_enu_t fmi2_cs_prep_init(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 2.0 CS PREP FMU
/// @param cdata
/// @return
jm_status_enu_t fmi2_cs_prep_simulate(fmu_check_data_t* cdata);

/// @brief Simulate an FMI 2.0 CS FMU step
/// @param cdata
/// @param tcur
/// @return
jm_status_enu_t fmi2_cs_simulate_step(fmu_check_data_t* cdata, fmi2_real_t tcur);

/// @brief Write FMI 2.0 CSV header
/// @param cdata
/// @return
jm_status_enu_t fmi2_write_csv_header(fmu_check_data_t* cdata);

/// @brief Write FMI 2.0 CSV data
/// @param cdata
/// @param time
/// @return
jm_status_enu_t fmi2_write_csv_data(fmu_check_data_t* cdata, double time);

/// @brief FMI2.0 end handling
/// @param cdata
/// @return
jm_status_enu_t fmi2_end_handling(fmu_check_data_t* cdata);

/// @brief Check if the fmi status is ok or warning
/// @param fmistatus
/// @return
static int fmi2_status_ok_or_warning(fmi2_status_t fmistatus)
{
  return (fmistatus == fmi2_status_ok) || (fmistatus == fmi2_status_warning);
}

/// @brief Prepare the time step, time end and number of steps info for the simulation.
///		   Input/output: information from default experiment
/// @param cdata
/// @param timeEnd
/// @param timeStep
void prepare_time_step_info(fmu_check_data_t* cdata, double* timeEnd, double* timeStep);

/// @brief Calls the FMI 1.0 get functions with zero length arrays and logs any problems and
///	returns either fmi ok/warning status when successful and other fmi status otherwise
/// @param fmu
/// @param cb
/// @return
fmi1_status_t check_fmi1_get_with_zero_len_array(fmi1_import_t* fmu, jm_callbacks* cb);

/// @brief Calls the FMI 1.0 set functions with zero length arrays and logs any problems and
///	returns either fmi ok/warning status when successful and other fmi status otherwise
/// @param fmu
/// @param cb
/// @return
fmi1_status_t check_fmi1_set_with_zero_len_array(fmi1_import_t* fmu, jm_callbacks* cb);

/// @brief FMU checker
/// @param cdata
/// @return
jm_status_enu_t fmuChecker(fmu_check_data_t* cdata);
#endif
