/*
    Copyright (C) 2012 Modelon AB <http://www.modelon.com>
                  2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
                

        You should have received a copy of the LICENCE-FMUChecker.txt
    along with this program. If not, contact Modelon AB.
*/
/**
        \file fmi2_input_reader.h
        Structure and functions supporting input files.
*/

#ifndef fmi2_input_reader_h
#define fmi2_input_reader_h

#include <JM/jm_vector.h>
#include <fmilib.h>

/** Structure incapsulating information on input data*/
typedef struct fmi2_csv_input_t
{
  jm_callbacks* cb;  ///< callbacks

  fmi2_import_t* fmu;  ///< FMI2 main struct

  jm_vector(double) timeStamps;  ///< list of time stamps
  size_t numTimeStamps;          ///< size of time stamps

  fmi2_import_variable_list_t* allInputs;  ///< list of all inputs

  fmi2_import_variable_list_t* realInputVariabilities;  ///< list of all inputs
  jm_vector(jm_voidp) * realInputVariabilityData;       ///< real input variablity data

  fmi2_import_variable_list_t* realInputs;  ///< list of real inputs
  jm_vector(jm_voidp) * realInputData;      ///< real input data

  //	fmi2_import_variable_list_t* discreteRealInputs;
  // jm_vector(jm_voidp)* discsrealInputData;

  //	fmi2_import_variable_list_t* continuousRealInputs;
  //    jm_vector(jm_voidp)* contrealInputData;

  fmi2_import_variable_list_t* intInputs;  ///< a subset of real inputs
  jm_vector(jm_voidp) * intInputData;      ///< int input data

  fmi2_import_variable_list_t* boolInputs;  ///< a subset of real inputs
  jm_vector(jm_voidp) * boolInputData;      ///< boolean of input data

  /** interpolation data for doubles. */
  /*  v[t] = v[i1]*lambda+v[i2](1-lambda) */
  double interpTime;       /** time instance where the coeff is calculated */
  size_t discreteIndex;    /** current data element index for discrete inputs */
  size_t interpIndex1;     /** first data element index for interpolation */
  size_t interpIndex2;     /** second data element index for interpolation */
  double interpLambda;     /** interpolation coefficient */
  fmi2_real_t* interpData; /** interpolated inputs */

  /*input event check data*/
  size_t eventIndex1; /** first data element index for interpolation */
  size_t eventIndex2; /** first data element index for interpolation */

} fmi2_csv_input_t;

//typedef struct fmu_check_data_t fmu_check_data_t;

/// @brief initialize the fmi1_csv_input_t strucuture
/// @param indata   input data
/// @param cb       callbacks
/// @param fmu      import
/// @return status of the event
jm_status_enu_t fmi2_init_input_data(fmi2_csv_input_t* indata, jm_callbacks* cb, fmi2_import_t* fmu);

/// @brief free memory allocated for the input data
/// @param indata input data
void fmi2_free_input_data(fmi2_csv_input_t* indata);

/// @brief update the interpolation coefficients inside the input data
/// @param indata input data
/// @param t time
void fmi2_update_input_interpolation(fmi2_csv_input_t* indata, double t);

/// @brief set all inputs on the fmu
/// @param cdata check data
/// @param time  time
/// @return status of the event
fmi2_status_t fmi2_set_inputs(fmu_check_data_t* cdata, double time);

/// @brief read input data from the file
/// @param cdata check data
/// @return status of the event
jm_status_enu_t fmi2_read_input_file(fmu_check_data_t* cdata);

/**
 * check input data interval for event trigger from data
 *
 * @param tcur      current data
 * @param tnext     next data
 * @param eventInfo information of the event
 * @param indata    input data
 *
 * @return status of the event
 */
jm_status_enu_t fmi2_check_external_events(fmi2_real_t tcur,
                                           fmi2_real_t tnext,
                                           fmi2_event_info_t* eventInfo,
                                           fmi2_csv_input_t* indata);
#endif
