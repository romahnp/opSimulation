
#include "FmuCommunication.h"

/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_string_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_string_t>(dataVecIn, size);
  return fmi1_import_set_string(cdata.fmu1,
                                valueReferences,  // array of value reference
                                size,             // number of elements
                                dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_string_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_string_t>(dataVecIn, size);
  return fmi2_import_set_string(cdata.fmu2,
                                valueReferences,  // array of value reference
                                size,             // number of elements
                                dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_real_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_real_t>(dataVecIn, size);
  return fmi1_import_set_real(cdata.fmu1,
                              valueReferences,  // array of value reference
                              size,             // number of elements
                              dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_real_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_real_t>(dataVecIn, size);
  return fmi2_import_set_real(cdata.fmu2,
                              valueReferences,  // array of value reference
                              size,             // number of elements
                              dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_integer_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_integer_t>(dataVecIn, size);
  return fmi1_import_set_integer(cdata.fmu1,
                                 valueReferences,  // array of value reference
                                 size,             // number of elements
                                 dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_integer_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_integer_t>(dataVecIn, size);
  return fmi2_import_set_integer(cdata.fmu2,
                                 valueReferences,  // array of value reference
                                 size,             // number of elements
                                 dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::SetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_boolean_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI1, fmi_boolean_t>(dataVecIn, size);
  return fmi1_import_set_boolean(cdata.fmu1,
                                 valueReferences,  // array of value reference
                                 size,             // number of elements
                                 dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::SetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_boolean_t> dataVecIn,
                                              size_t size)
{
  auto dataIn = FmuHelper::FmiDataFromValues<FMI2, fmi_boolean_t>(dataVecIn, size);
  return fmi2_import_set_boolean(cdata.fmu2,
                                 valueReferences,  // array of value reference
                                 size,             // number of elements
                                 dataIn);          // array of values
  delete[] dataIn;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_string_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi1_string_t[size];

  auto status = fmi1_import_get_string(cdata.fmu1,
                                       valueReferences,  // array of value reference
                                       size,             // number of elements
                                       dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI1>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_string_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi2_string_t[size];

  auto status = fmi2_import_get_string(cdata.fmu2,
                                       valueReferences,  // array of value reference
                                       size,             // number of elements
                                       dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI2>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_real_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi1_real_t[size];

  auto status = fmi1_import_get_real(cdata.fmu1,
                                     valueReferences,  // array of value reference
                                     size,             // number of elements
                                     dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI1>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_real_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi2_real_t[size];

  auto status = fmi2_import_get_real(cdata.fmu2,
                                     valueReferences,  // array of value reference
                                     size,             // number of elements
                                     dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI2>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_integer_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi1_integer_t[size];

  auto status = fmi1_import_get_integer(cdata.fmu1,
                                        valueReferences,  // array of value reference
                                        size,             // number of elements
                                        dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI1>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_integer_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi2_integer_t[size];

  auto status = fmi2_import_get_integer(cdata.fmu2,
                                        valueReferences,  // array of value reference
                                        size,             // number of elements
                                        dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI2>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI1>::GetValue(fmi1_value_reference_t valueReferences[],
                                              std::vector<fmi_boolean_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi1_boolean_t[size];

  auto status = fmi1_import_get_boolean(cdata.fmu1,
                                        valueReferences,  // array of value reference
                                        size,             // number of elements
                                        dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI1>(dataOut[i]);

  delete[] dataOut;
  return status;
}

template <>
template <>
fmi_status_t FmuCommunication<FMI2>::GetValue(fmi2_value_reference_t valueReferences[],
                                              std::vector<fmi_boolean_t>& dataVecOut,
                                              size_t size)
{
  dataVecOut.resize(size);
  auto dataOut = new fmi2_boolean_t[size];

  auto status = fmi2_import_get_boolean(cdata.fmu2,
                                        valueReferences,  // array of value reference
                                        size,             // number of elements
                                        dataOut);         // array of values

  for (int i = 0; i < size; i++) dataVecOut[i].emplace<FMI2>(dataOut[i]);

  delete[] dataOut;
  return status;
}