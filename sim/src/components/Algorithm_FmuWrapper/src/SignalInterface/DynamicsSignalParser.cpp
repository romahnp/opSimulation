/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "DynamicsSignalParser.h"

std::shared_ptr<const SignalInterface> DynamicsSignalParser::translate(
    std::set<SignalType> outputSignals,
    const std::string& componentName,
    ComponentState componentState,
    std::function<FmuValue&(SignalValue, VariableType)> GetFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::DynamicsSignal) != outputSignals.cend())
  {
    const DynamicsInformation dynamicsInformation
        = {units::acceleration::meters_per_second_squared_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_Acceleration, VariableType::Double).realValue),
           units::velocity::meters_per_second_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_Velocity, VariableType::Double).realValue)
               * units::math::cos(dynamicsInformation.yaw),
           units::velocity::meters_per_second_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_Velocity, VariableType::Double).realValue)
               * units::math::sin(dynamicsInformation.yaw),
           units::length::meter_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_PositionX, VariableType::Double).realValue),
           units::length::meter_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_PositionY, VariableType::Double).realValue),
           units::angle::radian_t(GetFmuSignalValue(SignalValue::DynamicsSignal_Yaw, VariableType::Double).realValue),
           units::angular_velocity::radians_per_second_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_YawRate, VariableType::Double).realValue),
           units::angular_acceleration::radians_per_second_squared_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_YawAcceleration, VariableType::Double).realValue),
           0.0_rad,
           units::angle::radian_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_SteeringWheelAngle, VariableType::Double).realValue),
           units::acceleration::meters_per_second_squared_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_CentripetalAcceleration, VariableType::Double).realValue),
           units::length::meter_t(
               GetFmuSignalValue(SignalValue::DynamicsSignal_TravelDistance, VariableType::Double).realValue)

        };

    return std::make_shared<DynamicsSignal const>(
        ComponentState::Acting, dynamicsInformation, componentName, componentName);
  }
  else
  {
    return std::make_shared<DynamicsSignal const>(
        ComponentState::Disabled, DynamicsInformation{}, componentName, componentName);
  }
}