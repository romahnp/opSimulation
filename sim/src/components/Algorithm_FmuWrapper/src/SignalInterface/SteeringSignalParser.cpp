/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SteeringSignalParser.h"

std::shared_ptr<const SignalInterface> SteeringSignalParser::translate(
    std::set<SignalType> outputSignals,
    const std::string& componentName,
    ComponentState componentState,
    std::function<FmuValue&(SignalValue, VariableType)> GetFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::SteeringSignal) != outputSignals.cend())
  {
    units::angle::radian_t steeringWheelAngle{
        GetFmuSignalValue(SignalValue::SteeringSignal_SteeringWheelAngle, VariableType::Double).realValue};
    return std::make_shared<SteeringSignal const>(componentState, steeringWheelAngle, componentName);
  }
  else
  {
    return std::make_shared<SteeringSignal const>(ComponentState::Disabled, 0.0_rad, componentName);
  }
}