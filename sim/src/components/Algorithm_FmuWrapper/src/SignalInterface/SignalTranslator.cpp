/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SignalTranslator.h"

#include <google/protobuf/message.h>
#include <memory>
#include <osi3/osi_hostvehicledata.pb.h>
#include <osi3/osi_trafficcommand.pb.h>
#include <osi3/osi_trafficupdate.pb.h>
#include <sstream>

#include "common/dynamicsSignal.h"
#include "common/osiUtils.h"
#include "common/sensorDataSignal.h"
#include "components/Algorithm_FmuWrapper/src/variant_visitor.h"
#include "include/signalInterface.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/FmuHelper.h"

/// @brief Translator function for Sensordata signal
struct SensorDataSignalTranslator : public InputSignalTranslator
{
  /// @brief Create a corresponding input signal translator object
  /// @param world                Reference to the world interface
  /// @param agent                Reference to the agent interface
  /// @param callbackInterface    Reference to the call back interface
  SensorDataSignalTranslator(WorldInterface &world, AgentInterface &agent, const CallbackInterface &callbackInterface)
      : InputSignalTranslator(world, agent, callbackInterface)
  {
  }

  /// @brief Translate input sensor data signal to protobuf message
  /// @param signalInterface Input sensor data signal
  /// @return  protobuf message
  const google::protobuf::Message *translate(std::shared_ptr<const SignalInterface> signalInterface,
                                             [[maybe_unused]] const google::protobuf::Message *const) override
  {
    if (const auto signal = std::dynamic_pointer_cast<SensorDataSignal const>(signalInterface)) /*[[likely]]*/
      return &signal->sensorData;

    const auto errorMessage
        = FmuHelper::log_prefix(std::to_string(agent.GetId())) + "AlgorithmFmuHandler invalid signaltype";
    callbackInterface.LOGERROR(errorMessage);
    throw std::runtime_error(errorMessage);
  }
};

InputSignalTranslator::InputSignalTranslator(WorldInterface &world,
                                             AgentInterface &agent,
                                             const CallbackInterface &callbackInterface)
    : world(world), agent(agent), callbackInterface(callbackInterface)
{
}

std::optional<std::shared_ptr<InputSignalTranslator>> InputSignalTranslatorFactory::build(
    int localLinkId, WorldInterface &world, AgentInterface &agent, const CallbackInterface &callbackInterface)
{
  if (localLinkId == 2)
  {
    return std::make_shared<SensorDataSignalTranslator>(world, agent, callbackInterface);
  }
  return std::nullopt;
}
