/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalSignalParser.h"

std::shared_ptr<const SignalInterface> LongitudinalSignalParser::translate(
    std::set<SignalType> outputSignals,
    const std::string& componentName,
    ComponentState componentState,
    std::function<FmuValue&(SignalValue, VariableType)> GetFmuSignalValue)
{
  if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::LongitudinalSignal) != outputSignals.cend())
  {
    auto accPedalPos = GetFmuSignalValue(SignalValue::LongitudinalSignal_AccPedalPos, VariableType::Double).realValue;
    auto brakePedalPos
        = GetFmuSignalValue(SignalValue::LongitudinalSignal_BrakePedalPos, VariableType::Double).realValue;
    auto gear = GetFmuSignalValue(SignalValue::LongitudinalSignal_Gear, VariableType::Int).intValue;
    return std::make_shared<LongitudinalSignal const>(componentState, accPedalPos, brakePedalPos, gear, componentName);
  }
  else
  {
    return std::make_shared<LongitudinalSignal const>(ComponentState::Disabled, 0.0, 0.0, 0, componentName);
  }
}