/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <google/protobuf/message.h>

#include "DynamicsOutputSignalBaseTranslator.h"
#include "common/accelerationSignal.h"
#include "common/dynamicsSignal.h"
#include "common/longitudinalSignal.h"
#include "common/steeringSignal.h"
#include "components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h"
#include "include/signalInterface.h"
#include "osi3/osi_trafficupdate.pb.h"

//! @brief Parser for TrafficUpdate, which will be converted to a DynamicsSignal
struct TrafficUpdateSignalParser : public DynamicsOutputSignalBaseTranslator
{
  //! Constructor
  //! @param agent  Reference to the agent interface
  TrafficUpdateSignalParser(AgentInterface &agent) : DynamicsOutputSignalBaseTranslator(agent) {}

  //! @brief Translator function for TrafficUpdate
  //!
  //! @param agent          Reference to the agent interface
  //! @param componentName  Name of this component
  //! @param outputType     Type of signal to generate
  //! @param trafficUpdate  OSI TrafficUpdate message
  //! @return pointer to the signal interface
  static std::shared_ptr<const SignalInterface> translate(AgentInterface &agent,
                                                          const std::string &componentName,
                                                          SignalType outputType,
                                                          const osi3::TrafficUpdate &trafficUpdate);
};