/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <any>
#include <components/Algorithm_FmuWrapper/src/variant_visitor.h>
#include <fstream>
#include <google/protobuf/util/json_util.h>
#include <utility>

#include <queue>

#include "ParserTypes.h"
#include "SSPElements/Connector/OSMPConnector.h"
#include "Visitors/Connector/UpdateInputSignalVisitor.h"
#include "components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "include/fmuHandlerInterface.h"
#include "include/fmuWrapperInterface.h"
#include "sim/src/components/Algorithm_FmuWrapper/src/FmuHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "type_helper.h"

extern "C"
{
#include "fmuChecker.h"
}

enum class RegisteredConnectorDirection
{
  None,
  Input,
  Output
};

/// @brief Class representing a factory for OSMP connector
class OSMPConnectorFactory
{
public:
  /// @brief Construct a OSMP connector factory
  /// @param componentName Name of the OSMP connector factory component
  explicit OSMPConnectorFactory(const std::string &componentName) : componentName(componentName) {}

  OSMPConnectorFactory() = delete;
  ~OSMPConnectorFactory() = default;

  /// @brief Register a connector in OSMP connector factory
  /// @param connector    Reference to the connector of ssp parser type
  /// @param fmuWrapper   Reference to the pointer of Fmu wrapper interface
  /// @param priority     Priority of the connector
  /// @param kind         Kind of the connector
  /// @return Returns the registered connector direction, if it is input/output/none
  RegisteredConnectorDirection RegisterConnector(const SspParserTypes::Connector &connector,
                                                 const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                                                 int priority,
                                                 ConnectorKind kind);

  /// @brief Create an input OSMP connectors
  /// @param  writeMessageParameters
  /// @param  outputDir
  /// @param  targetOutputTracesMap
  /// @return List of pointer to the ssp connector interface
  template <size_t FMI>
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> CreateInputOSMPConnectors(
      const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
      const std::filesystem::path &outputDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap)
  {
    return CreateOSMPConnectors<FMI>(
        inputOsmpLinks, osmpInput, writeMessageParameters, outputDir, targetOutputTracesMap);
  }

  /// @brief Create an output OSMP connectors
  /// @param  writeMessageParameters
  /// @param  outputDir
  /// @param  targetOutputTracesMap
  /// @return List of pointer to the ssp connector interface
  template <size_t FMI>
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> CreateOutputOSMPConnectors(
      const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
      const std::filesystem::path &outputDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap)
  {
    return CreateOSMPConnectors<FMI>(
        outputOsmpLinks, osmpOutput, writeMessageParameters, outputDir, targetOutputTracesMap);
  }

private:
  struct OSMPRoles
  {
    bool lo = false;
    bool high = false;
    bool size = false;

    bool IsComplete() const { return lo && high && size; }
  };

  struct OSMPConnectorBlueprint
  {
    std::string connectorName;
    std::string osmpLinkName;
    const std::shared_ptr<FmuWrapperInterface> fmuPointer;
    int fmuPriority;
    OsiType osiType;
    const std::vector<std::pair<std::string, std::string>> &writeMessageParameters;
    const std::filesystem::path &outputDir;
    const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap;
    bool OnlyInitialized;
  };

  struct OSMPData
  {
    std::map<std::string, std::string, std::less<>> osmpLinkToConnectorName{};
    std::map<std::string, const std::shared_ptr<FmuWrapperInterface> &, std::less<>> osmpLinkToFMU{};
    std::map<std::string, int> osmpLinkToPriority{};
    std::map<std::string, OSMPRoles, std::less<>> osmpLinkToOsmpRoles{};
    std::map<std::string, std::string, std::less<>> osmpLinkToSignalIdentifier{};
    std::map<std::string, bool, std::less<>> osmpLinkToIsParameter{};
  } osmpInput, osmpOutput;

  std::vector<std::string> inputOsmpLinks = {};
  std::vector<std::string> outputOsmpLinks = {};
  const std::string &componentName;

  void RegisterSingleConnector(std::vector<std::string> &osmpLinks,
                               OSMPData &osmpData,
                               const std::string &connectorName,
                               const std::string &osmpLinkName,
                               const std::shared_ptr<FmuWrapperInterface> &fmuWrapper,
                               int priority,
                               OSMPConnector::OsmpLinkRole role,
                               const std::string &signalString,
                               bool isParameter);

  template <size_t FMI, typename T>
  std::shared_ptr<ssp::Connector> SetOsmpConnector(std::shared_ptr<ssp::OsmpConnector<T, FMI>> osmpConnector,
                                                   const OSMPConnectorBlueprint &blueprint) const
  {
    osmpConnector->ApplyWriteOutputParameters(
        blueprint.writeMessageParameters, blueprint.outputDir, blueprint.targetOutputTracesMap, componentName);
    if (blueprint.OnlyInitialized)
    {
      osmpConnector->SetParameterConnector();
    }
    return osmpConnector;
  }

  template <size_t FMI>
  std::vector<std::shared_ptr<ssp::ConnectorInterface>> CreateOSMPConnectors(
      std::vector<std::string> osmpLink,
      OSMPData osmpData,
      const std::vector<std::pair<std::string, std::string>> &writeMessageParameters,
      const std::filesystem::path &outputDir,
      const std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetOutputTracesMap)
  {
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> connectors{};
    for (const auto &connectorLink : osmpLink)
    {
      if (osmpData.osmpLinkToOsmpRoles.at(connectorLink).IsComplete())
      {
        OsiType osiType = OSMPConnector::OsiFromString(osmpData.osmpLinkToSignalIdentifier.at(connectorLink));

        auto osmpConnectorBlueprint = OSMPConnectorBlueprint{osmpData.osmpLinkToConnectorName.at(connectorLink),
                                                             connectorLink,
                                                             osmpData.osmpLinkToFMU.at(connectorLink),
                                                             osmpData.osmpLinkToPriority.at(connectorLink),
                                                             osiType,
                                                             writeMessageParameters,
                                                             outputDir,
                                                             targetOutputTracesMap,
                                                             osmpData.osmpLinkToIsParameter.at(connectorLink)};
        connectors.emplace_back(MakeOsmpConnector<FMI>(osmpConnectorBlueprint));
      }
    }
    return connectors;
  }

  template <size_t FMI>
  std::shared_ptr<ssp::Connector> MakeOsmpConnector(const OSMPConnectorBlueprint &blueprint) const
  {
    switch (blueprint.osiType)
    {
      case SensorView:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::SensorView, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case SensorViewConfiguration:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::SensorViewConfiguration, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case SensorViewConfigurationRequest:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::SensorViewConfiguration, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case SensorData:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::SensorData, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case TrafficUpdate:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::TrafficUpdate, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case HostVehicleData:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::HostVehicleData, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case GroundTruth:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::GroundTruth, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      case TrafficCommand:
      {
        auto osmpCon = std::make_shared<ssp::OsmpConnector<osi3::TrafficCommand, FMI>>(
            blueprint.connectorName, blueprint.osmpLinkName, blueprint.fmuPointer, blueprint.fmuPriority);
        return SetOsmpConnector<FMI>(osmpCon, blueprint);
      }
      default:
        throw std::runtime_error(" Trying to create an OSMPConnector from the ssd file with an unknown OSI type");
    }
  }
};