/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Connection.h"

namespace ssp
{

Connection::Connection(std::string componentStartName,
                       std::string variableReferenceStart,
                       std::string componentEndName,
                       std::string variableReferenceEnd)
    : componentStartName(std::move(componentStartName)),
      variableReferenceStart(std::move(variableReferenceStart)),
      componentEndName(std::move(componentEndName)),
      variableReferenceEnd(std::move(variableReferenceEnd))
{
}

bool Connection::operator==(const Connection &other) const
{
  return componentStartName == other.componentStartName && variableReferenceStart == other.variableReferenceStart
      && componentEndName == other.componentEndName && variableReferenceEnd == other.variableReferenceEnd;
}

OSMPConnection::OSMPConnection(std::string componentStartName,
                               std::string variableReferenceStart,
                               std::string componentEndName,
                               std::string variableReferenceEnd,
                               std::string osmpRole,
                               std::string osmpLinkName)
    : Connection(std::move(componentStartName),
                 std::move(variableReferenceStart),
                 std::move(componentEndName),
                 std::move(variableReferenceEnd)),
      osmpRole(std::move(osmpRole)),
      osmpLinkName(std::move(osmpLinkName))
{
}

bool OSMPConnection::operator==(const OSMPConnection &other) const
{
  return osmpLinkName == other.osmpLinkName && componentStartName == other.componentStartName
      && componentEndName == other.componentEndName;
}

std::optional<OSMPConnection> OSMPConnectionCompletenessCheck::ReturnOSMPConnectionIfComplete(
    const OSMPConnection connection)
{
  {
    if (connectionCompletenessMap.find(connection) != connectionCompletenessMap.end())
    {
      connectionCompletenessMap.at(connection).SetComplete(connection.osmpRole);
      if (connectionCompletenessMap.at(connection).IsComplete())
      {
        return std::make_optional(connection);
      }
    }
    else
    {
      connectionCompletenessMap.try_emplace(connection, OSMPConnectionCompleteness());
      connectionCompletenessMap.at(connection).SetComplete(connection.osmpRole);
    }
    return std::nullopt;
  }
}

void OSMPConnectionCompletenessCheck::OSMPConnectionCompleteness::SetComplete(const std::string &role)
{
  if (role == ".base.lo") hasBaseLo = true;
  if (role == ".base.hi") hasBaseHi = true;
  if (role == ".size") hasSize = true;
}
}  //namespace ssp