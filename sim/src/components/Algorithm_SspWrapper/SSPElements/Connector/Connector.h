/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "ConnectorInterface.h"

namespace ssp
{

/// Class representing a connector for ssp connector interface
class Connector : public ssp::ConnectorInterface
{
public:
  virtual ~Connector() = default;
  Connector() = default;
  Connector(Connector &&) = default;  ///< move constructor

  /**
   * @brief move assignment operator
   *
   * @param other
   * @return Connector&
   */
  Connector &operator=(Connector &&other) = default;

  /// @brief copy assignment operator
  /// @param other connector
  /// @return connector
  Connector &operator=(const Connector &other) = default;

  /// @brief copy constructor
  /// @param other Connector
  Connector(const Connector &other) = default;

  /// @brief Constructor for the connector with priority as an argument
  /// @param priority Priority of the FMU component
  explicit Connector(const int &priority);

  /// @brief Constructor for the connector with connectorName as an argument
  /// @param connectorName Name of the connector
  explicit Connector(std::string connectorName) : name(std::move(connectorName)) {}

  /// @brief For this moment, Base Connector accepts visitor and does nothing
  /// @param visitor A reference to the connector visitor interface
  void Accept(ConnectorVisitorInterface &visitor) override
  {
    LOGWARN("Base Connector accepts visitor and does nothing");
  }

  /// @return Returns the priority of the connector
  [[nodiscard]] int GetPriority() const override { return priority; };

  /// @brief Set the priority of the connector
  /// @param prior Priority number
  void SetPriority(int prior) { priority = prior; }

  /// @return Returns the connector name
  [[nodiscard]] const std::string &GetConnectorName() const override { return name; }

  /// @return Returns true, if the connector is a parameter connector
  bool IsParameterConnector() const override { return parameterConnector; }

  /// @brief Set the parameter connector as true
  void SetParameterConnector() { parameterConnector = true; }

  /// @brief Function to get the list o all non parameter connectors
  /// @return List of all non parameter connectors
  std::vector<std::shared_ptr<ConnectorInterface>> GetNonParameterConnectors() const override;

  /**
   * @brief Log message
   *
   * @param logLevel
   * @param file
   * @param line
   * @param message
   */
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const;

private:
  int priority = 0;
  std::string name;
  bool parameterConnector = false;
};
}  //namespace ssp
