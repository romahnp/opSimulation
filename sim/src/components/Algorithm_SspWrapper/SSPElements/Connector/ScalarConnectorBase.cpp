/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ScalarConnectorBase.h"

void ssp::ScalarConnectorBase::Accept(ssp::ConnectorVisitorInterface &visitor)
{
  visitor.Visit(this);
}
const std::string &ssp::ScalarConnectorBase::GetConnectorName() const
{
  return fmuScalarVariableName;
}
ssp::ScalarConnectorBase::ScalarConnectorBase(std::shared_ptr<FmuWrapperInterface> fmuWrapperInterface,
                                              std::string fmuScalarVariableName,
                                              int priority)
    : Connector(priority),
      fmuWrapperInterface(std::move(fmuWrapperInterface)),
      fmuScalarVariableName(std::move(fmuScalarVariableName))
{
}
