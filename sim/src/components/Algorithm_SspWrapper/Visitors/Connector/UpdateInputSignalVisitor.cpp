/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "UpdateInputSignalVisitor.h"

ssp::UpdateInputSignalVisitor::UpdateInputSignalVisitor(const int localLinkId,
                                                        std::shared_ptr<const SignalInterface> signalInterface,
                                                        const int time,
                                                        WorldInterface *world,
                                                        AgentInterface *agent,
                                                        const CallbackInterface *callbacks)
    : localLinkId(localLinkId),
      signalInterface(std::move(signalInterface)),
      time(time),
      world(world),
      agent(agent),
      callbacks(callbacks)
{
}

void ssp::UpdateInputSignalVisitor::Visit(ssp::ScalarConnectorBase *connector)
{
  if (connector->IsParameterConnector())
  {
    LOGDEBUG(connector->GetConnectorName() + " is a parameter Connector, UpdateInputSignal Skipped");
    return;
  }

  LOGDEBUG("SSP Input Signal Visitor: Visit FMU connector " + connector->GetConnectorName());
  if (!signalInterface)
  {
    LOGWARN("SSP Input Signal Visitor: No signal interface");
    return;
  }
  auto doubleSignal = std::dynamic_pointer_cast<DoubleSignal const>(signalInterface);
  ConnectorHelper::SetFmuWrapperValue<VariableTypeDouble>(
      connector->fmuWrapperInterface, connector->fmuScalarVariableName, doubleSignal->value);
}

void ssp::UpdateInputSignalVisitor::Visit(ssp::GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Input Signal Visitor: Visit system connector ");            //+ connector->GetConnectorName());
  LOGDEBUG("SSP priority queue of system connector will now be handled.");  //+ connector.GetConnectorName() + "will no
                                                                            //be handled.");
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

void ssp::UpdateInputSignalVisitor::Visit(ssp::OSMPConnectorBase *connector)
{
  if (connector->IsParameterConnector())
  {
    LOGDEBUG(connector->GetConnectorName() + " is a parameter Connector, UpdateInputSignal Skipped");
    return;
  }

  LOGDEBUG("SSP Input Signal Visitor: Visit OSMP connector " + connector->GetConnectorName());

  if (auto inputSignalTranslator = InputSignalTranslatorFactory::build(localLinkId, *world, *agent, *callbacks))
  {
    if (const auto trafficCommand = std::dynamic_pointer_cast<const osi3::TrafficCommand>(connector->GetMessage()))
    {
      if (!trafficCommand->has_timestamp() || osi3::utils::GetTimestampInMilliseconds(*trafficCommand) != time)
      {
        osi3::TrafficCommand newTrafficCommand;
        newTrafficCommand.CopyFrom(*trafficCommand);
        osi3::utils::SetTimestamp(newTrafficCommand, time);
        osi3::utils::SetVersion(newTrafficCommand);
        connector->SetMessage(&newTrafficCommand);
      }
    }

    connector->SetMessage(inputSignalTranslator.value()->translate(signalInterface, connector->GetMessage().get()));
    connector->HandleFileWriting(this->time);
  }
  connector->GetFmuWrapperInterface()->UpdateInput(localLinkId, signalInterface, time);
}

void ssp::UpdateInputSignalVisitor::Log(CbkLogLevel logLevel,
                                        const char *file,
                                        int line,
                                        const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}