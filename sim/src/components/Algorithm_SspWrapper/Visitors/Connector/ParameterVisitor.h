/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "../../SSPElements/Connector/ConnectorHelper.h"
#include "../../SSPElements/Connector/ScalarConnectorBase.h"
#include "../SspVisitorHelper.h"
#include "ConnectorVisitorInterface.h"

namespace ssp
{

/// @brief Struct representing an input for a parameter
/// @tparam T
template <typename T>
struct ParameterInput
{
  std::string connectorName;  ///< Name of the SSP connector
  cvt_t<T> data;              ///< cvt data
};

/// @brief visitior class for a parameter of connector visitior interface
/// @tparam T
template <typename T>
class ParameterVisitor : public ConnectorVisitorInterface
{
public:
  /// @brief Construct a parameter visitor
  /// @param parameterInput  input parameter of typename T
  explicit ParameterVisitor(ParameterInput<T> parameterInput) : input(parameterInput) {}

  /// @brief Visitor function to update output
  /// @param connector Pointer to the scalar connector base
  void Visit(ScalarConnectorBase *connector) override;

  /// @brief Visitor function to update output
  /// @param groupConnector Reference to the group connector
  void Visit(GroupConnector &groupConnector) override;

  /// @brief Visitor function to update output
  /// @param connector Reference to OSMP Connector base
  void Visit(OSMPConnectorBase *connector) override;

private:
  const ParameterInput<T> input;

protected:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;
};

template <typename T>
void ParameterVisitor<T>::Visit(ScalarConnectorBase *connector)
{
  if (!connector->IsParameterConnector() && connector->GetConnectorName() == input.connectorName)
  {
    LOGDEBUG("SSP Parameter Visitor: Visit FMU connector " + connector->GetConnectorName());
    ConnectorHelper::SetFmuWrapperValue<T>(
        connector->fmuWrapperInterface, connector->fmuScalarVariableName, input.data);
  }
}

template <typename T>
void ParameterVisitor<T>::Visit(GroupConnector &groupConnector)
{
  LOGDEBUG("SSP Parameter Visitor: Visit system connector ");  //+ connector.GetConnectorName());
  SSPVisitorHelper::PriorityAccept(groupConnector, *this);
}

template <typename T>
void ssp::ParameterVisitor<T>::Visit(OSMPConnectorBase *connector)
{
  LOGDEBUG("SSP Parameter Visitor: Visit OSMP connector and skip");
}

template <typename T>
void ssp::ParameterVisitor<T>::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
  SspLogger::Log(logLevel, file, line, message);
}
}  //namespace ssp