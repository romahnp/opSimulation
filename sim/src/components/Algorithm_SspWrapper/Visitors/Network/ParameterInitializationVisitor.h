/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include <algorithm>
#include <memory>
#include <utility>
#include <vector>

#include <queue>

#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/ConnectorVisitorInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/ParameterVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspNetworkVisitorInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/SspVisitorHelper.h"

namespace ssp
{

/// @brief Class representing a visitor for parameter initialization
/// @tparam T
template <typename T>
class ParameterInitializationVisitor : public SspNetworkVisitorInterface
{
public:
  ~ParameterInitializationVisitor() override = default;

  /// @brief Constructor for parameter initialization
  /// @param _visitor                 Reference to the parameter visitor
  /// @param affectedComponentName    Name of the affected component
  ParameterInitializationVisitor(ParameterVisitor<T> &_visitor, const std::string affectedComponentName)
      : parameterVisitor(_visitor), affectedComponentName(std::move(affectedComponentName)){};
  ParameterInitializationVisitor() = delete;

  void Visit(const System *ssdSystem) final
  {
    LOGDEBUG("SSP Parameter Visitor: Visit System " + ssdSystem->elementName);
    SSPVisitorHelper::PriorityAcceptVisitableNetworkElements(*this, ssdSystem->elements);
  }

  void Visit(const FmuComponent *component) final
  {
    if (affectedComponentName == component->elementName)
    {
      LOGDEBUG("SSP Parameter Visitor: Visit FMU component " + component->elementName);
      ssp::GroupConnector inputGroup{component->input_connectors};
      inputGroup.Accept(parameterVisitor);
    }
  }

  /// @brief Visitor function for ssp component.
  /// Note: the function is not yet implemented
  void Visit(const ssp::SspComponent *) final
  {
    LOGDEBUG("SSP Parameter Visitor: Visit SSP Component");
    LOGWARN("SSP Parameter Visitor: Visit SSP Component not implemented");
  }

protected:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const final
  {
    SspLogger::Log(logLevel, file, line, message);
  }

private:
  std::string affectedComponentName;
  ParameterVisitor<T> &parameterVisitor;
};

}  //namespace ssp