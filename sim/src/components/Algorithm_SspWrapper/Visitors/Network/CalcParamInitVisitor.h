/*******************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#pragma once

#include <algorithm>
#include <memory>
#include <utility>
#include <vector>

#include <queue>

#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/CalculatedParameterVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/ConnectorVisitorInterface.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspNetworkVisitorInterface.h"

namespace ssp
{

/// @brief Class representing a visitor for initializing calculated parameters
class CalcParamInitVisitor : public SspNetworkVisitorInterface
{
public:
  ~CalcParamInitVisitor() override = default;

  /// @brief Constructor for Calculate parameter initialization visitor
  /// @param _visitor Reference to the CalculatedParameterVisitor
  explicit CalcParamInitVisitor(CalculatedParameterVisitor &_visitor) : calculatedParameterVisitor(_visitor){};
  CalcParamInitVisitor() = delete;
  void Visit(const System *ssdSystem) override;
  void Visit(const FmuComponent *component) override;
  void Visit(const SspComponent *) override;

protected:
  void Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const override;

private:
  CalculatedParameterVisitor &calculatedParameterVisitor;
};

}  //namespace ssp