/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SspVisitorHelper.h"

#include "../SSPElements/Connector/Connector.h"
#include "../SSPElements/Connector/GroupConnector.h"
#include "../SSPElements/Connector/OSMPConnectorBase.h"
#include "../SSPElements/Connector/ScalarConnectorBase.h"

bool ssp::SSPVisitorHelper::ComparePriorities(const std::shared_ptr<ssp::Connector> &val1,
                                              const std::shared_ptr<ssp::Connector> &val2)
{
  return val1->GetPriority() < val2->GetPriority();
}

void ssp::SSPVisitorHelper::TriggerScalarConnectorOnce(
    std::vector<std::weak_ptr<FmuWrapperInterface>> &alreadyTriggered, const ScalarConnectorBase *connector, int time)
{
  if (std::find_if(cbegin(alreadyTriggered),
                   cend(alreadyTriggered),
                   [&connector](const std::weak_ptr<FmuWrapperInterface> &fmuWrapperInterface)
                   { return fmuWrapperInterface.lock() == connector->fmuWrapperInterface; })
      == cend(alreadyTriggered))
  {
    connector->fmuWrapperInterface->Trigger(time);
    alreadyTriggered.emplace_back(connector->fmuWrapperInterface);
  }
}
void ssp::SSPVisitorHelper::TriggerOMSPConnectorOnce(std::vector<std::weak_ptr<FmuWrapperInterface>> &alreadyTriggered,
                                                     ssp::OSMPConnectorBase *connector,
                                                     int time)
{
  if (std::find_if(cbegin(alreadyTriggered),
                   cend(alreadyTriggered),
                   [&connector](const std::weak_ptr<FmuWrapperInterface> &fmuWrapperInterface)
                   { return fmuWrapperInterface.lock() == connector->GetFmuWrapperInterface(); })
      == cend(alreadyTriggered))
  {
    connector->GetFmuWrapperInterface()->Trigger(time);
    alreadyTriggered.emplace_back(connector->GetFmuWrapperInterface());
  }
}
void ssp::SSPVisitorHelper::PriorityAccept(const ssp::GroupConnector &groupConnector,
                                           ssp::ConnectorVisitorInterface &connectorVisitor)
{
  std::priority_queue<std::shared_ptr<Connector>, std::vector<std::shared_ptr<Connector>>, decltype(&ComparePriorities)>
      tempQueue(&ComparePriorities);

  std::for_each(groupConnector.connectors.cbegin(),
                groupConnector.connectors.cend(),
                [&tempQueue](const std::shared_ptr<ConnectorInterface> &p_Connector)
                { tempQueue.emplace(std::dynamic_pointer_cast<Connector>(p_Connector)); });

  while (!tempQueue.empty())
  {
    tempQueue.top()->Accept(connectorVisitor);
    tempQueue.pop();
  }
}
