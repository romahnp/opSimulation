/********************************************************************************
 * Copyright (c) 2020-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "zlibUnzip.h"

#include <array>
#include <filesystem>
#include <fstream>
#include <functional>
#include <memory>
#include <ostream>
#include <stdexcept>

#include "unzip.h"

unzFile OpenZip(const std::filesystem::path &);
std::string ReadCurrentFileName(unzFile &);
void ReadCurrentFileContent(unzFile &, std::ostream &);
auto BranchOnFileType(const std::string &) -> std::function<void(unzFile &, std::ostream &)>;
void StreamCurrentFileContentToDisk(unzFile &fileHandle,
                                    const std::function<std::string(unzFile &)> &fileName,
                                    const std::function<void(unzFile &, std::ostream &)> &fileContent);

void CreateDirectory(const std::string &path)
{
  std::filesystem::create_directories({path});
}

auto BranchOnFileType(const std::string &targetPath) -> std::function<void(unzFile &, std::ostream &)>
{
  if (*targetPath.crbegin() == '/') return [&targetPath](unzFile &, std::ostream &) { CreateDirectory(targetPath); };
  return ReadCurrentFileContent;
}

std::set<std::filesystem::path> UnpackZip(const std::filesystem::path &zipFile,
                                          const std::filesystem::path &targetLocation)
{
  unzFile fileHandle = OpenZip(zipFile);
  std::set<std::filesystem::path> unzippedFiles;
  do
  {
    auto path = targetLocation / ReadCurrentFileName(fileHandle);
    StreamCurrentFileContentToDisk(
        fileHandle,
        [&unzippedFiles, &path](unzFile &fileHandle)
        {
          unzippedFiles.emplace(path);
          return path.string();
        },
        BranchOnFileType(path.string()));
  } while (unzGoToNextFile(fileHandle) == UNZ_OK);
  return unzippedFiles;
}

unzFile OpenZip(const std::filesystem::path &path)
{
  unzFile fileHandle = unzOpen(path.string().c_str());  // calls unzGoToFirstFile if file != nullptr
  if (fileHandle == nullptr)
    throw std::runtime_error("Zip file '" + path.string() + "' not found or unable to open as zip file.");
  return fileHandle;
}

std::string ReadCurrentFileName(unzFile &fileHandle)
{
  unz_file_info_s unzFileInfoS{};
  const uLong fileNameBufferSize = 1024;
  char fileName[fileNameBufferSize];

  unzGetCurrentFileInfo(fileHandle, &unzFileInfoS, fileName, fileNameBufferSize, nullptr, 0, nullptr, 0);

  return std::string(fileName);
}

void ReadCurrentFileContent(unzFile &fileHandle, std::ostream &ostream)
{
  if (int error = unzOpenCurrentFile(fileHandle) != UNZ_OK)
    throw std::runtime_error("Error unzipping current file:\n\tError opening current file with code: "
                             + std::to_string(error));
  constexpr unsigned bufferLength = 1024;
  auto buffer = std::make_shared<std::array<char, bufferLength>>();
  int readBytes;
  do
  {
    readBytes = unzReadCurrentFile(fileHandle, buffer.get(), bufferLength);
    if (readBytes < 0)
      throw std::runtime_error("Error unzipping current file:\n\tError reading current file with code: "
                               + std::to_string(readBytes));
    ostream.write(buffer->data(), readBytes);
    buffer = std::make_shared<std::array<char, bufferLength>>();
  } while (readBytes == bufferLength);
}

void StreamCurrentFileContentToDisk(unzFile &fileHandle,
                                    const std::function<std::string(unzFile &)> &fileName,
                                    const std::function<void(unzFile &, std::ostream &)> &fileContent)
{
  std::ofstream ofstream;
  ofstream.open(fileName(fileHandle), std::ios::out | std::ios::trunc | std::ios::binary);
  fileContent(fileHandle, ofstream);
  ofstream.close();
}
