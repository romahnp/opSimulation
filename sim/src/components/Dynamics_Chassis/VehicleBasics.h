/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef VEHICLEBASICS_H
#define VEHICLEBASICS_H

#define NUMBER_WHEELS 4

#include <cmath>

#include "units.h"

const units::acceleration::meters_per_second_squared_t GRAVITY_ACC{9.81};

using namespace units::literals;

/// @brief Class representing basics of the vehicle
class VehicleBasics
{
public:
  VehicleBasics() = default;

  /**
   * @brief Construct a new Vehicle Basics object
   *
   * @param lLeft TODO
   * @param lRight
   * @param lFront
   * @param lRear
   * @param hMC
   * @param m
   */
  VehicleBasics(std::vector<units::length::meter_t> lLeft,
                std::vector<units::length::meter_t> lRight,
                units::length::meter_t lFront,
                units::length::meter_t lRear,
                units::length::meter_t hMC,
                units::mass::kilogram_t m)
      : lenLeft(lLeft), lenRight(lRight), lenFront(lFront), lenRear(lRear), heightMC(hMC), mass(m)
  {
    CalculateRatio();
    CalculateWheelMass();
  }

  /// TODO
  std::vector<units::length::meter_t> lenLeft;
  /// TODO
  std::vector<units::length::meter_t> lenRight;
  /// TODO
  units::length::meter_t lenFront;
  /// TODO
  units::length::meter_t lenRear;

  double ratioX;                    //!< front length / rear length
  double ratioY[2];                 //!< left length / right length
  units::length::meter_t heightMC;  //!< hight of mass center
  units::mass::kilogram_t mass;     //!< vehicle mass

  /**
   * @brief Calculate ratio of TODO
   *
   */
  void CalculateRatio()
  {
    ratioX = (lenFront - mcOffsetX) / (lenRear + mcOffsetX);
    ratioY[0] = (lenLeft[0] - mcOffsetY[0]) / (lenRight[0] + mcOffsetY[0]);
    ratioY[1] = (lenLeft[1] - mcOffsetY[0]) / (lenRight[1] + mcOffsetY[0]);
  }

  /**
   * @brief Calculate wheel mass
   *
   */
  void CalculateWheelMass()
  {
    units::mass::kilogram_t massFront = mass / (1 + ratioX);
    units::mass::kilogram_t massRear = ratioX * massFront;

    // separate front mass to frontleft and frontright wheels
    wheelMass[0] = massFront / (1 + ratioY[0]);  // frontleft
    wheelMass[1] = ratioY[0] * wheelMass[0];     // frontright

    // separate rear mass to rearleft and rearright wheels
    wheelMass[2] = massRear / (1 + ratioY[1]);  // rearleft
    wheelMass[3] = ratioY[1] * wheelMass[2];    // rearright
  }

  /**
   * @brief Calculate deformation
   *
   * @param pitchAngle angle of pitch
   * @param rollAngle  angle of roll
   */
  void Deformation(units::angle::radian_t pitchAngle, units::angle::radian_t rollAngle)
  {
    if (pitchAngle >= 0_rad)  // pitching backward, and mcOffsetX will be negative
    {
      mcOffsetX = -units::math::sin(pitchAngle) * (heightMC + lenRear * units::math::tan(pitchAngle));
    }
    else  // pitching foreward, and mcOffsetX will be positive
    {
      mcOffsetX = units::math::sin(-pitchAngle) * (heightMC + lenFront * units::math::tan(-pitchAngle));
    }

    if (rollAngle >= 0_rad)  // rolling towards right, and mcOffsetY will be negative
    {
      mcOffsetY[0] = -units::math::sin(rollAngle) * (heightMC + lenRight[0] * units::math::tan(rollAngle));
      mcOffsetY[1] = -units::math::sin(rollAngle) * (heightMC + lenRight[1] * units::math::tan(rollAngle));
    }
    else  // rolling towards left, and mcOffsetY will be positive
    {
      mcOffsetY[0] = units::math::sin(-rollAngle) * (heightMC + lenLeft[0] * units::math::tan(-rollAngle));
      mcOffsetY[1] = units::math::sin(-rollAngle) * (heightMC + lenLeft[1] * units::math::tan(-rollAngle));
    }

    CalculateRatio();
    CalculateWheelMass();
  }

  /**
   * @brief Get the Wheel Mass object
   *
   * @param i  The number of the wheel
   * @return wheelMass
   */
  units::mass::kilogram_t GetWheelMass(int i) { return wheelMass[i]; }

private:
  units::length::meter_t mcOffsetX{0.0};
  units::length::meter_t mcOffsetY[2] = {(units::length::meter_t)0.0, (units::length::meter_t)0.0};

  units::mass::kilogram_t wheelMass[NUMBER_WHEELS];
};

#endif  //VEHICLEBASICS_H
