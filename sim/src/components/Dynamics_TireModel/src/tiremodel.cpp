/********************************************************************************
 * Copyright (c) 2023 Volkswagen AG
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * @ingroup Tire_Model
 * @defgroup init_tire Initialization
 */

/**
 * @ingroup Tire_Model
 * @defgroup step_tire Process
 */
/**
 * @ingroup Tire_Model
 * @defgroup output_tire Output
 */

#include "tiremodel.h"

#include <memory>

#include <QString>
#include <qglobal.h>

#include "common/commonTools.h"
#include "components/common/vehicleProperties.h"

DynamicsTireModel::DynamicsTireModel(std::string componentName,
                                     bool isInit,
                                     int priority,
                                     int offsetTime,
                                     int responseTime,
                                     int cycleTime,
                                     StochasticsInterface *stochastics,
                                     WorldInterface *world,
                                     const ParameterInterface *parameters,
                                     PublisherInterface *const publisher,
                                     const CallbackInterface *callbacks,
                                     AgentInterface *agent)
    : UnrestrictedModelInterface(componentName,
                                 isInit,
                                 priority,
                                 offsetTime,
                                 responseTime,
                                 cycleTime,
                                 stochastics,
                                 world,
                                 parameters,
                                 publisher,
                                 callbacks,
                                 agent)
{
  LOGINFO(QString().sprintf("Constructing Dynamics_TireModel for agent %d...", agent->GetId()).toStdString());

  mCycle = cycleTime;

  /** @addtogroup init_tire
   *
   * tire initialization:
   *  - Read Position center of gravity
   *  - Get friction coefficient
   *  - Set initial vertical tire forces and external parameters
   *  - Init TMEasy TireModel
   */

  std::shared_ptr<const mantle_api::VehicleProperties> vehicleProperties
      = std::dynamic_pointer_cast<const mantle_api::VehicleProperties>(GetAgent()->GetVehicleModelParameters());

  try
  {
    PositionCOG.x = std::stod(helper::map::query(vehicleProperties->properties, "XPositionCOG").value()) * 1_m;
  }
  catch (...)
  {
    PositionCOG.x = GetAgent()->GetWheelbase() / 2;
  }
  try
  {
    PositionCOG.y = std::stod(helper::map::query(vehicleProperties->properties, "YPositionCOG").value()) * 1_m;
  }
  catch (...)
  {
    PositionCOG.y = 0_m;
  }

  auto frictionCoeff = helper::map::query(vehicleProperties->properties, vehicle::properties::FrictionCoefficient);
  THROWIFFALSE(frictionCoeff.has_value(), "FrictionCoefficient was not defined in VehicleCatalog");

  auto weight = GetAgent()->GetVehicleModelParameters()->mass;

  std::vector<double> forceWheelVerticalInit;

  std::vector<mantle_api::Axle> axles;
  axles.resize(2);
  axles[0] = vehicleProperties->front_axle;
  axles[1] = vehicleProperties->rear_axle;

  units::mass::kilogram_t massFront = weight
                                    * (PositionCOG.x
                                       / (vehicleProperties->front_axle.bb_center_to_axle_center.x
                                          - vehicleProperties->rear_axle.bb_center_to_axle_center.x));

  units::mass::kilogram_t massRear = weight - massFront;

  units::length::meter_t bb_center = units::math::abs(vehicleProperties->bounding_box.geometric_center.x);

  // Calculate vertical force at timestep 0
  forceWheelVerticalInit.resize(axles.size() * 2);
  forceWheelVerticalInit[0] = 9.81 * massFront.value() * (axles[0].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[0].track_width.value();
  forceWheelVerticalInit[1] = 9.81 * massFront.value()
                            * std::fabs(-axles[0].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[0].track_width.value();
  forceWheelVerticalInit[2] = 9.81 * massRear.value() * (axles[1].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[1].track_width.value();
  forceWheelVerticalInit[3] = 9.81 * massRear.value()
                            * std::fabs(-axles[1].track_width.value() / 2 - PositionCOG.y.value())
                            / axles[1].track_width.value();

  forceWheelVertical.SetDefaultValue(forceWheelVerticalInit);

  // read external parameters
  ParseParameters(parameters);

  Common::Vector2d velocity = agent->GetVelocity();
  velocity.Rotate(-agent->GetYaw());

  // Initialization tires
  InitTires(velocity,
            muTireMaxXFRef,
            muTireMaxYFRef,
            muTireSlideXFRef,
            muTireSlideYFRef,
            slipTireMaxXFRef,
            slipTireMaxYFRef,
            muTireMaxX2FRef,
            muTireMaxY2FRef,
            muTireSlideX2FRef,
            muTireSlideY2FRef,
            slipTireMaxX2FRef,
            slipTireMaxY2FRef,
            FRef,
            FRefNormalized,
            std::stod(frictionCoeff.value()),
            slipTireSlideXFRef,
            slipTireSlideYFRef,
            F0pXFRef,
            F0pYFRef,
            slipTireSlideX2FRef,
            slipTireSlideY2FRef,
            F0pX2FRef,
            F0pY2FRef,
            axles,
            inertia,
            pneumaticTrail,
            forceWheelVerticalInit,
            bb_center);

  LOGINFO("Constructing Dynamics_TireModel successful");
}

void DynamicsTireModel::ParseParameters(const ParameterInterface *parameters)
{
  // Read external parameters
  muTireMaxXFRef = parameters->GetParametersDoubleVector().count("MuTireMaxXFRef") == 1
                     ? parameters->GetParametersDoubleVector().at("MuTireMaxXFRef")
                     : parameters->GetParametersDoubleVector().at("1");
  muTireMaxX2FRef = parameters->GetParametersDoubleVector().count("MuTireMaxX2FRef") == 1
                      ? parameters->GetParametersDoubleVector().at("MuTireMaxX2FRef")
                      : parameters->GetParametersDoubleVector().at("2");

  muTireSlideXFRef = parameters->GetParametersDoubleVector().count("MuTireSlideXFRef") == 1
                       ? parameters->GetParametersDoubleVector().at("MuTireSlideXFRef")
                       : parameters->GetParametersDoubleVector().at("3");
  muTireSlideX2FRef = parameters->GetParametersDoubleVector().count("MuTireSlideX2FRef") == 1
                        ? parameters->GetParametersDoubleVector().at("MuTireSlideX2FRef")
                        : parameters->GetParametersDoubleVector().at("4");

  slipTireMaxXFRef = parameters->GetParametersDoubleVector().count("SlipTireMaxXFRef") == 1
                       ? parameters->GetParametersDoubleVector().at("SlipTireMaxXFRef")
                       : parameters->GetParametersDoubleVector().at("5");
  slipTireMaxX2FRef = parameters->GetParametersDoubleVector().count("SlipTireMaxX2FRef") == 1
                        ? parameters->GetParametersDoubleVector().at("SlipTireMaxX2FRef")
                        : parameters->GetParametersDoubleVector().at("6");

  slipTireSlideXFRef = parameters->GetParametersDoubleVector().count("SlipTireSlideXFRef") == 1
                         ? parameters->GetParametersDoubleVector().at("SlipTireSlideXFRef")
                         : parameters->GetParametersDoubleVector().at("7");
  slipTireSlideX2FRef = parameters->GetParametersDoubleVector().count("SlipTireSlideX2FRef") == 1
                          ? parameters->GetParametersDoubleVector().at("SlipTireSlideX2FRef")
                          : parameters->GetParametersDoubleVector().at("8");

  F0pXFRef = parameters->GetParametersDoubleVector().count("F0pXFRef") == 1
               ? parameters->GetParametersDoubleVector().at("F0pXFRef")
               : parameters->GetParametersDoubleVector().at("9");
  F0pX2FRef = parameters->GetParametersDoubleVector().count("F0pX2FRef") == 1
                ? parameters->GetParametersDoubleVector().at("F0pX2FRef")
                : parameters->GetParametersDoubleVector().at("10");

  muTireMaxYFRef = parameters->GetParametersDoubleVector().count("MuTireMaxYFRef") == 1
                     ? parameters->GetParametersDoubleVector().at("MuTireMaxYFRef")
                     : parameters->GetParametersDoubleVector().at("11");
  muTireMaxY2FRef = parameters->GetParametersDoubleVector().count("MuTireMaxY2FRef") == 1
                      ? parameters->GetParametersDoubleVector().at("MuTireMaxY2FRef")
                      : parameters->GetParametersDoubleVector().at("12");

  muTireSlideYFRef = parameters->GetParametersDoubleVector().count("MuTireSlideYFRef") == 1
                       ? parameters->GetParametersDoubleVector().at("MuTireSlideYFRef")
                       : parameters->GetParametersDoubleVector().at("13");
  muTireSlideY2FRef = parameters->GetParametersDoubleVector().count("MuTireSlideY2FRef") == 1
                        ? parameters->GetParametersDoubleVector().at("MuTireSlideY2FRef")
                        : parameters->GetParametersDoubleVector().at("14");

  slipTireMaxYFRef = parameters->GetParametersDoubleVector().count("SlipTireMaxYFRef") == 1
                       ? parameters->GetParametersDoubleVector().at("SlipTireMaxYFRef")
                       : parameters->GetParametersDoubleVector().at("15");
  slipTireMaxY2FRef = parameters->GetParametersDoubleVector().count("SlipTireMaxY2FRef") == 1
                        ? parameters->GetParametersDoubleVector().at("SlipTireMaxY2FRef")
                        : parameters->GetParametersDoubleVector().at("16");

  slipTireSlideYFRef = parameters->GetParametersDoubleVector().count("SlipTireSlideYFRef") == 1
                         ? parameters->GetParametersDoubleVector().at("SlipTireSlideYFRef")
                         : parameters->GetParametersDoubleVector().at("17");
  slipTireSlideY2FRef = parameters->GetParametersDoubleVector().count("SlipTireSlideY2FRef") == 1
                          ? parameters->GetParametersDoubleVector().at("SlipTireSlideY2FRef")
                          : parameters->GetParametersDoubleVector().at("18");

  F0pYFRef = parameters->GetParametersDoubleVector().count("F0pYFRef") == 1
               ? parameters->GetParametersDoubleVector().at("F0pYFRef")
               : parameters->GetParametersDoubleVector().at("19");
  F0pY2FRef = parameters->GetParametersDoubleVector().count("F0pY2FRef") == 1
                ? parameters->GetParametersDoubleVector().at("F0pY2FRef")
                : parameters->GetParametersDoubleVector().at("20");

  FRef = parameters->GetParametersDoubleVector().count("FRef") == 1 ? parameters->GetParametersDoubleVector().at("FRef")
                                                                    : parameters->GetParametersDoubleVector().at("21");
  FRefNormalized = parameters->GetParametersBoolVector().count("FRefNormalized") == 1
                     ? parameters->GetParametersBoolVector().at("FRefNormalized")
                     : parameters->GetParametersBoolVector().at("22");

  inertia = parameters->GetParametersDoubleVector().count("Inertia") == 1
              ? parameters->GetParametersDoubleVector().at("Inertia")
              : parameters->GetParametersDoubleVector().at("23");
  pneumaticTrail = parameters->GetParametersDoubleVector().count("PneumaticTrail") == 1
                     ? parameters->GetParametersDoubleVector().at("PneumaticTrail")
                     : parameters->GetParametersDoubleVector().at("24");
}

DynamicsTireModel::~DynamicsTireModel()
{
  for (int i = 0; i < tires.size(); ++i)
  {
    delete tires[i];
  }
}

void DynamicsTireModel::UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateInput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = inputPorts.at(localLinkId)->SetSignalValue(data);

  if (success)
  {
    log << COMPONENTNAME << " UpdateInput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateInput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void DynamicsTireModel::UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time)
{
  Q_UNUSED(time);

  std::stringstream log;
  log << COMPONENTNAME << " UpdateOutput";
  LOG(CbkLogLevel::Debug, log.str());
  log.str(std::string());

  bool success = outputPorts.at(localLinkId)->GetSignalValue(data);

  if (success)
  {
    log << COMPONENTNAME << " UpdateOutput successful";
    LOG(CbkLogLevel::Debug, log.str());
  }
  else
  {
    log << COMPONENTNAME << " UpdateOutput failed";
    LOG(CbkLogLevel::Error, log.str());
  }
}

void DynamicsTireModel::Trigger(int time)
{
  Q_UNUSED(time);

  /** @addtogroup step_tire
   * Read previous vehicle's state:
   *  - global position (cartesian coordinates)
   *  - direction of vehicle's longitudinal axes (angle in polar coordinates)
   *  - vehicle's longitudinal and lateral velocity in vehicle's CS
   *  - vehicle's rotational velociy
   *  - vehicle's longitudinal and lateral acceleration in vehicle's CS
   *  - vehicle's rotational acceleration
   *
   */

  ReadPreviousState();

  /** @addtogroup step_tire
   * Apply tire forces at the tire/road interface:
   *  - calculate lateral tire slips and forces
   *  - calculate friction forces
   */
  TireForce();
}

void DynamicsTireModel::ReadPreviousState()
{
  yawVelocity = GetAgent()->GetYawRate();
  yawAngle = GetAgent()->GetYaw();
  velocityCar.x = GetAgent()->GetVelocity().x - yawVelocity.value() * (PositionCOG.y.value()) * 1_mps;  // vehicle COG
  velocityCar.y = GetAgent()->GetVelocity().y + yawVelocity.value() * (PositionCOG.x.value()) * 1_mps;  // vehicle COG

  velocityCar.Rotate(-yawAngle);  // vehicle CS

  LOGDEBUG(QString()
               .sprintf("Prev Velocity for Dynamics_TireModel for agent %d: %f, %f, %f",
                        GetAgent()->GetId(),
                        velocityCar.x,
                        velocityCar.y,
                        yawVelocity)
               .toStdString());
}

void DynamicsTireModel::InitTires(Common::Vector2d<units::velocity::meters_per_second_t> velocityCar,
                                  std::vector<double> muTireMaxX,
                                  std::vector<double> muTireMaxY,
                                  std::vector<double> muTireSlideX,
                                  std::vector<double> muTireSlideY,
                                  std::vector<double> sMaxX,
                                  std::vector<double> sMaxY,
                                  std::vector<double> muTireMaxX2,
                                  std::vector<double> muTireMaxY2,
                                  std::vector<double> muTireSlideX2,
                                  std::vector<double> muTireSlideY2,
                                  std::vector<double> sMaxX2,
                                  std::vector<double> sMaxY2,
                                  std::vector<double> F_ref,
                                  std::vector<bool> F_ref_norm,
                                  double mu_scale,
                                  std::vector<double> sSlideX,
                                  std::vector<double> sSlideY,
                                  std::vector<double> F0pX,
                                  std::vector<double> F0pY,
                                  std::vector<double> sSlideX2,
                                  std::vector<double> sSlideY2,
                                  std::vector<double> F0pX2,
                                  std::vector<double> F0pY2,
                                  std::vector<mantle_api::Axle> axles,
                                  std::vector<double> Inertia,
                                  std::vector<double> PneumaticTrail,
                                  std::vector<double> forceWheelVertical,
                                  units::length::meter_t bb_center)
{
  tires.resize(axles.size() * 2);

  for (int i = 0; i < axles.size(); ++i)
  {
    units::length::meter_t r_tire = axles[i].wheel_diameter / 2;

    Common::Vector2d<units::length::meter_t> positionTire;
    // Calculate tire position from axle data
    positionTire.x = axles[i].bb_center_to_axle_center.x + bb_center - PositionCOG.x;
    positionTire.y = axles[i].track_width / 2 - PositionCOG.y;
    // Init rotational velocity
    units::frequency::hertz_t rotationVelocityTire = velocityCar.x / r_tire;
    // init parameters
    Common::Vector2d<double> muTireMax, muTireSlide, sMax, sSlide, muTireMax2, muTireSlide2, sMax2, sSlide2;

    Common::Vector2d<units::force::newton_t> F0p, F0p2;

    muTireMax = {muTireMaxX[i], muTireMaxY[i]};

    muTireSlide = {muTireSlideX[i], muTireSlideY[i]};
    sMax = {sMaxX[i], sMaxY[i]};
    sSlide = {sSlideX[i], sSlideY[i]};
    F0p = {F0pX[i] * 1_N, F0pY[i] * 1_N};

    muTireMax2 = {muTireMaxX2[i], muTireMaxY2[i]};

    muTireSlide2 = {muTireSlideX2[i], muTireSlideY2[i]};
    sMax2 = {sMaxX2[i], sMaxY2[i]};
    sSlide2 = {sSlideX2[i], sSlideY2[i]};
    F0p2 = {F0pX2[i] * 1_N, F0pY2[i] * 1_N};

    // if reference vertical force is normalized, then multiply with init vertical tire force
    if (F_ref_norm[i])
    {
      F0p.x *= forceWheelVertical[2 * i];
      F0p.y *= forceWheelVertical[2 * i];

      F0p2.x *= 2 * forceWheelVertical[2 * i];
      F0p2.y *= 2 * forceWheelVertical[2 * i];

      F_ref[i] = forceWheelVertical[2 * i];
    }

    tires[2 * i] = new Tire(F_ref[i] * 1_N,
                            {muTireMaxX[i], muTireMaxY[i]},
                            muTireSlide,
                            sMax,
                            muTireMax2,
                            muTireSlide2,
                            sMax2,
                            r_tire,
                            mu_scale,
                            sSlide,
                            sSlide2,
                            F0p,
                            F0p2,
                            positionTire,
                            rotationVelocityTire,
                            Inertia[i],
                            PneumaticTrail[i] * 1_m);

    tires[2 * i]->CalcVelTire(0_rad_per_s, velocityCar);
    // update y position of second tire on axle
    positionTire.y = -axles[i].track_width / 2 - PositionCOG.y;

    F0p = {F0pX[i] * 1_N, F0pY[i] * 1_N};
    F0p2 = {F0pX2[i] * 1_N, F0pY2[i] * 1_N};
    if (F_ref_norm[i])
    {
      F_ref[i] /= forceWheelVertical[2 * i + 1];

      F0p.x *= forceWheelVertical[2 * i + 1];
      F0p.y *= forceWheelVertical[2 * i + 1];

      F0p2.x *= 2 * forceWheelVertical[2 * i + 1];
      F0p2.y *= 2 * forceWheelVertical[2 * i + 1];

      F_ref[i] = forceWheelVertical[2 * i + 1];
    }

    tires[2 * i + 1] = new Tire(F_ref[i] * 1_N,
                                {muTireMaxX[i], muTireMaxY[i]},
                                muTireSlide,
                                sMax,
                                muTireMax2,
                                muTireSlide2,
                                sMax2,
                                r_tire,
                                mu_scale,
                                sSlide,
                                sSlide2,
                                F0p,
                                F0p2,
                                positionTire,
                                rotationVelocityTire,
                                Inertia[i],
                                PneumaticTrail[i] * 1_m);

    tires[2 * i + 1]->CalcVelTire(0_rad_per_s, velocityCar);
  }
}

void DynamicsTireModel::TireForce()
{
  std::vector<double> steering_angle = wheelAngle.GetValue();

  std::vector<double> lonTireForce, latTireForce, torque, wheelRotationRate;

  lonTireForce.resize(tires.size());
  latTireForce.resize(tires.size());
  torque.resize(tires.size());
  wheelRotationRate.resize(tires.size());

  for (unsigned int idx = 0; idx < tires.size(); ++idx)
  {
    tires[idx]->SetTireAngle(steering_angle[idx] * 1_rad);
    tires[idx]->Rescale(std::fabs(forceWheelVertical.GetValue()[idx]) * 1_N);
    // calculate tire velocity in tire CS
    tires[idx]->CalcVelTire(yawVelocity, velocityCar);
    // add tire torque from brake and powertrain
    units::torque::newton_meter_t torqueTireSum = 0.0_Nm;

    if (tires[idx]->GetVelocityTire().x < 0_mps)
    {
      torqueTireSum -= brakeTorque.GetValue()[idx] * 1_Nm;
    }
    else
    {
      torqueTireSum = brakeTorque.GetValue()[idx] * 1_Nm;
    }

    torqueTireSum += driveTorque.GetValue()[idx] * 1_Nm;
    tires[idx]->SetTorque(torqueTireSum);

    ///////////////////////////////////////////////////
    // Calculate Tire Force (classic Runge-Kutta method)
    units::frequency::hertz_t rotVelocity = tires[idx]->GetRotationVelocity();
    double mDT = mCycle / (1000.0);
    tires[idx]->CalcRotAcc();
    double k1 = tires[idx]->GetRotAcceleration();
    double k2, k3, k4;
    tires[idx]->SetRotationVelocity(rotVelocity + 0.5_Hz * mDT * k1);
    tires[idx]->CalcTireForce();
    tires[idx]->CalcRotAcc();
    k2 = tires[idx]->GetRotAcceleration();
    tires[idx]->SetRotationVelocity(rotVelocity + 0.5_Hz * mDT * k2);
    tires[idx]->CalcTireForce();
    tires[idx]->CalcRotAcc();
    k3 = tires[idx]->GetRotAcceleration();
    tires[idx]->SetRotationVelocity(rotVelocity + mDT * k3 * 1_Hz);
    tires[idx]->CalcTireForce();
    tires[idx]->CalcRotAcc();
    k4 = tires[idx]->GetRotAcceleration();
    tires[idx]->SetRotationVelocity(rotVelocity + (1.0_Hz / 6.0) * mDT * (k1 + 2 * k2 + 2 * k3 + k4));
    tires[idx]->CalcTireForce();
    ///////////////////////////////////////////////////

    // Calculate tire self aligning torque
    tires[idx]->CalcSelfAligningTorque();

    // Update wheel rotation rate and tire force output
    lonTireForce[idx] = tires[idx]->GetLongitudinalForce().value() - tires[idx]->GetRollFriction().value();
    latTireForce[idx] = tires[idx]->GetLateralForce().value();
    torque[idx] = tires[idx]->GetSelfAligningTorque().value();
    wheelRotationRate[idx] = tires[idx]->GetRotationVelocity().value();
    GetAgent()->SetWheelRotationRate(std::floor(idx / 2.0), idx % 2, wheelRotationRate[idx]);
  }

  // Set Output
  out_longitudinalTireForce.SetValue(lonTireForce);
  out_lateralTireForce.SetValue(latTireForce);
  out_selfAligningTorque.SetValue(torque);
  out_wheelRotationRate.SetValue(wheelRotationRate);
}
