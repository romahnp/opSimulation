################################################################################
# Copyright (c) 2023 Volkswagen AG
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
set(COMPONENT_NAME Action_BrakeSystem)

add_compile_definitions(ACTION_BRAKESYSTEM_LIBRARY)

add_openpass_target(
  NAME ${COMPONENT_NAME} TYPE library LINKAGE shared COMPONENT module

  HEADERS
    action_brakesystem.h
    src/brakesystem.h

  SOURCES
    action_brakesystem.cpp
    src/brakesystem.cpp

  LIBRARIES
    Qt5::Core
)
