/********************************************************************************
 * Copyright (c) 2022-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "MantleAPI/Common/time_utils.h"
#include "MantleAPI/Execution/i_environment.h"
#include "include/entityRepositoryInterface.h"
#include "include/worldInterface.h"

namespace core
{

//! This class provides the interface for the Environment
class EnvironmentInterface : public mantle_api::IEnvironment
{
public:
  //! @brief Method which retrieves the entity repository
  //!
  //! @return Reference to the entity repository
  virtual EntityRepositoryInterface& GetEntityRepository() = 0;

  //! @brief Method which retrieves the entity repository
  //!
  //! @return Const reference to the entity repository
  virtual const EntityRepositoryInterface& GetEntityRepository() const = 0;

  //! @brief Method which sets the simulation time
  //!
  //! @param[in] simulationTime   Time since start of simulation
  virtual void SetSimulationTime(mantle_api::Time simulationTime) = 0;

  //! @brief Method which updates all entities and sync data of all agents within world
  virtual void SyncWorld() = 0;

  //! @brief Method which updates the status of the ControllStrategyInterface for all entities
  virtual void ResetControlStrategyStatus() = 0;

  //! @brief Method which sets the world
  //!
  //! @param[in] world    The world to be used
  virtual void SetWorld(WorldInterface* world) = 0;

  //! @brief Method which resets the parameters for the next run
  virtual void Reset() = 0;
};
}  //namespace core