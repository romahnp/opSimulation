/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <list>
#include <map>

#include "common/spawnPointLibraryDefinitions.h"
#include "include/agentFactoryInterface.h"
#include "include/configurationContainerInterface.h"

namespace core
{
class SpawnPoint;

/// Interface representing spawnpoint network
class SpawnPointNetworkInterface
{
public:
  SpawnPointNetworkInterface() = default;
  SpawnPointNetworkInterface(const SpawnPointNetworkInterface&) = delete;
  SpawnPointNetworkInterface(SpawnPointNetworkInterface&&) = delete;
  SpawnPointNetworkInterface& operator=(const SpawnPointNetworkInterface&) = delete;
  SpawnPointNetworkInterface& operator=(SpawnPointNetworkInterface&&) = delete;
  virtual ~SpawnPointNetworkInterface() = default;

  //-----------------------------------------------------------------------------
  //! Gets the spawn point instance library either from the already stored libraries
  //! or create a new instance (which is then also stored), then create a new spawn
  //! point using the provided parameters.
  //!
  //! @param[in]  libraryInfos            Information for the SpawnPointLibrary
  //! @param[in]  agentFactory            Factory for the agents
  //! @param[in]  stochastics             StochasticInterface
  //! @param[in]  spawnPointProfiles      SpawnPointProfiles of the ProfilesCatalog
  //! @param[in]  configurationContainer  ConfigurationContainer
  //! @param[in]  environment             Shared pointer to the environment
  //! @param[in]  vehicles                Shared pointer containing the Vehicles of the VehiclesCatalog
  //! @return                             true, if successful
  //-----------------------------------------------------------------------------
  virtual bool Instantiate(const SpawnPointLibraryInfoCollection& libraryInfos,
                           AgentFactoryInterface* agentFactory,
                           StochasticsInterface* stochastics,
                           const std::optional<ProfileGroup>& spawnPointProfiles,
                           ConfigurationContainerInterface* configurationContainer,
                           std::shared_ptr<mantle_api::IEnvironment> environment,
                           std::shared_ptr<Vehicles> vehicles)
      = 0;

  /// Trigger for calculation of pre-run spawning
  ///
  /// Calls the Trigger of each instantiated pre-run SpawnPoint
  ///
  /// @return false on internal runtime exceptions, true otherwise
  virtual bool TriggerPreRunSpawnZones() = 0;

  /// Trigger for calculation of runtime spawning
  ///
  /// Calls the Trigger of each instantiated runtime SpawnPoint
  ///
  /// @param timestamp The current timestamp [ms]
  ///
  /// @return false on internal runtime exceptions, true otherwise
  virtual bool TriggerRuntimeSpawnPoints(const int timestamp) = 0;

  //-----------------------------------------------------------------------------
  //! Clears all spawnpoints.
  //-----------------------------------------------------------------------------
  virtual void Clear() = 0;
};

}  //namespace core
