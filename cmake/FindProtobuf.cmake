################################################################################
# Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# find_package adapter for protobuf
#
# Original protpbuf CMake Config file doesn't provide static targets.
#
# Creates the follwoing imported targets (if available):
# - protobuf::libprotobuf
# - protobuf::libprotobuf_static

set(PROTOBUF_SHARED_NAMES
  protobuf.lib
  libprotobuf.dll.a
  libprotobuf.so
)

set(PROTOBUF_STATIC_NAMES
  protobuf_static.lib
  libprotobuf.a
)

find_library(PROTOBUF_SHARED_LIBRARY NAMES ${PROTOBUF_SHARED_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

find_library(PROTOBUF_STATIC_LIBRARY NAMES ${PROTOBUF_STATIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib
    lib64
)

if(PROTOBUF_SHARED_LIBRARY)
  message(STATUS "Found protobuf (shared): ${PROTOBUF_SHARED_LIBRARY}")
  get_filename_component(PROTOBUF_SHARED_LIBRARY_DIR "${PROTOBUF_SHARED_LIBRARY}" DIRECTORY)
  add_library(protobuf::libprotobuf IMPORTED SHARED)
  set_target_properties(protobuf::libprotobuf
                        PROPERTIES
                          IMPORTED_LOCATION ${PROTOBUF_SHARED_LIBRARY}
                          IMPORTED_IMPLIB ${PROTOBUF_SHARED_LIBRARY}
                          INTERFACE_COMPILE_DEFINITIONS PROTOBUF_USE_DLLS
                          INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF_SHARED_LIBRARY_DIR}/../include
                          INTERFACE_LINK_LIBRARIES pthread)
else()
  message(STATUS "Didn't find protobuf (shared)")
endif()

if(PROTOBUF_STATIC_LIBRARY)
  message(STATUS "Found protobuf (static): ${PROTOBUF_STATIC_LIBRARY}")
  get_filename_component(PROTOBUF_STATIC_LIBRARY_DIR "${PROTOBUF_STATIC_LIBRARY}" DIRECTORY)
  add_library(protobuf::libprotobuf_static IMPORTED STATIC)
  set_target_properties(protobuf::libprotobuf_static
                        PROPERTIES
                          IMPORTED_LOCATION ${PROTOBUF_STATIC_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF_STATIC_LIBRARY_DIR}/../include
                          INTERFACE_LINK_LIBRARIES pthread)
else()
  message(STATUS "Didn't find protobuf (static)")
endif()

unset(PROTOBUF_SHARED_LIBRARY)
unset(PROTOBUF_STATIC_LIBRARY)
