################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building osi with Conan
################################################################################

from conan import ConanFile
from conan.errors import ConanInvalidConfiguration
from conan.tools.files import apply_conandata_patches
from conans import tools, AutoToolsBuildEnvironment, CMake
import os

required_conan_version = ">=1.47.0"

class OpenSimulationInterfaceConan(ConanFile):
    name = "open-simulation-interface"
    license = "2-Clause BSD"
    description = 'Generic interface environmental perception of automated driving functions in virtual scenarios'
    topics = ("asam", "adas", "open-simulation", "automated-driving", "openx")
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }
    generators = "cmake"
    short_paths = True
    _repo_source = None

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    @property
    def _build_subfolder(self):
        return "build_subfolder"

    def requirements(self):
        if self.settings.os == "Linux":
            self.requires("protobuf/3.20.0@")
            self.options["protobuf"].shared = True

    def build_requirements(self):
        if self.settings.os == "Windows":
            self.requires("protobuf/3.20.0@")
            self.options["protobuf"].shared = True
            self.tool_requires("protobuf/3.20.0@")
            self.options["protobuf"].shared = True

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
        
    def source(self): 
        self._repo_source = os.path.join(self.source_folder, self.name)
        tools.get(**self.conan_data["sources"][self.version], strip_root=True, destination=self._repo_source)
        self.run("find . -maxdepth 1 -name '*.proto' -exec sed -i '2i option cc_enable_arenas = true;' {} \;", win_bash=True, cwd=self._repo_source)

    def build(self):
        if self.settings.os == "Windows":
            cmake = CMake(self, generator="MSYS Makefiles")
            cmake.configure(source_folder=self._repo_source,
                defs={"CMAKE_BUILD_TYPE":"Release",
                        "CMAKE_INSTALL_PREFIX":"osi3.5.0"})
        else:
            cmake = CMake(self)
            PROTOBUF_PROTOC_EXECUTABLE = os.path.join(self.deps_cpp_info["protobuf"].bin_paths[0], "protoc")
            PROTOBUF_LIBRARY = os.path.join(self.deps_cpp_info["protobuf"].lib_paths[0], "libprotobuf.so")
            cmake.configure(source_folder=self._repo_source,
                    defs={"CMAKE_GENERATOR_ARG":"Unix Makefiles",
                            "CMAKE_BUILD_TYPE":"Release",
                            "CMAKE_INSTALL_PREFIX":"osi3.5.0",
                            "PROTOBUF_INCLUDE_DIR":self.deps_cpp_info["protobuf"].include_paths[0],
                            "PROTOBUF_PROTOC_EXECUTABLE":PROTOBUF_PROTOC_EXECUTABLE,
                            "PROTOBUF_LIBRARY":PROTOBUF_LIBRARY})

        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", src="osi3.5.0")

        